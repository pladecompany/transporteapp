module.exports = function extraerDigitos(str) {
  /*
   * extraerDigitos('h01a mund0') -> '010'
   */
  return (str.match(/\d/g) || []).join('');
}
