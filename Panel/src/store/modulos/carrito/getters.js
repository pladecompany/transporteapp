export function items (state) {
  return Object.keys(state.items);
}

export function numero_items (state) {
  return Object.keys(state.items).length;
}

export function itemPorId (state) {
  return (idItem) => Object.values(state.items).filter(i => i.id == idItem).pop();
}

export function sync_payload (state) {
  return Object.values(state.items);
}
