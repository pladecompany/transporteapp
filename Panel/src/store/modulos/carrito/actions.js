import { uid } from 'quasar'

export function agregar (ctx, payload) {
  const logueado = ctx.rootGetters['auth/logueado'];
  return logueado ? _agregar_logueado(ctx, payload) : _agregar_no_logueado(ctx, payload);
}

export function cambiar_cantidad (ctx, payload) {
  const logueado = ctx.rootGetters['auth/logueado'];
  return logueado ? _cambiar_cantidad_logueado(ctx, payload) : _cambiar_cantidad_no_logueado(ctx, payload);
}

export function fetch (ctx) {
  /** Recuperar carrito del servidor  **/
  const dummy_items = [
    {
      id:1,
      cantidad:1,
      total:50,
      producto: {
        nom_pro: 'Serrucho de titanio',
        cod_inv: '11111',
        pre_ven_inv: 50,
        dolar: 10,
      },
    },
    {
      id:2,
      cantidad:2,
      total:150,
      producto: {
        nom_pro: 'Juego de alicates',
        cod_inv: '22222',
        pre_ven_inv: 75,
        dolar: 10,
      },
    },
  ];

  dummy_items.forEach(item => ctx.commit('SET_ITEM', [item.id, item]));
}

export function post (ctx) {
  /** Subir carrito local al servidor **/
}


/*
 * FUNCIONES PRIVADAS
 */
/*
 * USUARIO LOGUEADO
 */

function _agregar_logueado(ctx, payload) {
  // body...
}

function _cambiar_cantidad_logueado(ctx, payload) {
  // body...
}

/*
 * USUARIO NO LOGUEADO
 */
function _agregar_no_logueado(ctx, item) {
  return new Promise(function(resolve, reject) {
    const producto = ctx.rootGetters['productos/porId'](item.id);
    const local_id = uid();
    item.subtotal = producto.precio_usd * 100 * item.cantidad / 100;

    ctx.commit('SET_ITEM', [local_id, item]);
    ctx.commit('SET_SUBTOTAL', sumarDinero(ctx.getters.subtotal, item.subtotal))
    ctx.commit('SET_TOTAL', sumarDinero(ctx.getters.total, item.total))
    return resolve({m: 'Agregado al carrito', r: 1, status: 'ok'});
  });
}

function _cambiar_cantidad_no_logueado(ctx, data){
  return new Promise((resolve, reject) => {
    const idItem = data.id
    const cantidad = data.cantidad

    if(!idItem || !cantidad){
      return reject('Datos incompletos');
    }

    const item = ctx.rootGetters['productos/porId'](idItem);
  });
}

function sumarDinero(...valores) {
  return parseFloat(valores.reduce((a, b) => (a * 100 + b * 100) / 100, 0).toFixed(2));
}
