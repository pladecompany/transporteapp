import { LocalStorage } from 'quasar'

export default {
  namespaced: true,

  state: () => ({
    notificaciones: {noti:[]},
  }),


  getters: {
    notificaciones: state => state.notificaciones,
  },


  mutations: {
    GUARDAR_NOTIFICACIONES: (state, notificaciones) => { state.notificaciones = notificaciones || {noti:[]} },
  },


  actions: {
    actualizarNotificaciones({commit}, id){
      if(LocalStorage.has('ID')){
        const userId = LocalStorage.getItem('ID');
        var tipo_usu = 'admin';
        this.$api.get('/notificaciones/?tipo_usuario='+tipo_usu+'&id='+userId)
          .then(data => {
            commit('GUARDAR_NOTIFICACIONES', data.data)
          })
      }
    },
  },
};
