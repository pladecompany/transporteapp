import { LocalStorage } from 'quasar'

function dummyLogs() {
  return [
    {
      name: 'superwifi',
      datetime: new Date('2021-01-01 14:30:00'),
    },
    {
      name: 'public-net',
      datetime: new Date('2021-01-03 07:00:00'),
    },
    {
      name: 'abacantv',
      datetime: new Date('2021-01-08 11:00:00'),
    },
    {
      name: 'abacantv2',
      datetime: new Date('2021-01-08 11:00:00'),
    },
    {
      name: 'abacantv3',
      datetime: new Date('2021-01-08 11:00:00'),
    },
    {
      name: 'abacantv4',
      datetime: new Date('2021-01-08 11:00:00'),
    },
    {
      name: 'abacantv5',
      datetime: new Date('2021-01-08 11:00:00'),
    },
  ];
}

function defaultState() {
  return {
    logs: LocalStorage.getItem('LOGS') || [],
  }
}

const getters = {
  logs: state => state.logs,
  desdeFecha: (state) => (endDate) => state.logs.filter(log => log.datetime <= endDate),
  hastaFecha: (state) => (startDate) => state.logs.filter(log => log.datetime >= startDate),
  entreFechas: (state) => (startDate, endDate) => {
    return state.logs.filter(log => log.datetime <= endDate && log.datetime >= startDate)
  },
}

const mutations = {
  ADD_LOG: (state, productos) => { state.productos = productos },
  RESET_STATE: state => Object.assign(state, defaultState()),
}

const actions = {
  add_log(ctx, log) {
    return new Promise((resolve, reject) => {
      if (!log || !log.name || !log.datetime) return reject('Log expected');

      const logs = LocalStorage.getItem('LOGS') || [];
      logs.push({name:log.name, datetime:log.datetime});

      LocalStorage.set('LOGS', logs);
      resolve(logs);
    });
  },

  reset_state(ctx) {
    LocalStorage.remove('LOGS');
    ctx.commit('RESET_STATE');
  },
}


export default {
  namespaced: true,
  state: defaultState,
  getters,
  mutations,
  actions,
}
