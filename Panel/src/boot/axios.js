import axios from 'axios'
import config from '../config'
import { LocalStorage } from 'quasar'

// Crear instancia de axios.
const axiosAPI = axios.create({
  baseURL: config.API_URL,
})

// Configurar los headers globalmente.
Object.assign(axiosAPI.defaults.headers.common, config.headers)

// Incluir token en cada peticion.
axiosAPI.interceptors.request.use(
  config => {
    const token = LocalStorage.getItem('TOKEN')
    const id_usuario = LocalStorage.getItem('ID')

    if (token) {
      config.headers.token = token
    }
    if (id_usuario) {
      config.headers.id_usuario = id_usuario
    }

    return config
  },
  error => { Promise.reject(error) }
)


// Para que este disponible en los componentes y en store.
export default ({ store, Vue }) => {
  Vue.prototype.$axios = axios
  Vue.prototype.$api = axiosAPI
  store.$axios = axios
  store.$api = axiosAPI

  // Interceptar codigos 401, verificar si el token todavia es valido.
  axiosAPI.interceptors.response.use(
    response => response,
    async error => {
      const online = error.response && error.request;
      const logueado = store.getters['auth/logueado'];

      if (online && logueado && error.response.status === 401 && document.location.hash.toLowerCase() !== '#/login') {
        const code = await store.dispatch('auth/check_token');
        if (code == 0) {
          await store.dispatch('auth/salir');
          store.$router.push('/Login');
        }
      }
      return Promise.reject(error);
    }
  )
}
