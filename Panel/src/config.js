/*
 * Parametros generales.
 */

const config = Object.freeze({
  //API_URL: 'https://inthecompanies.com:7654/',
  API_URL: 'https://enviaxa.com:3000/',
  //API_URL: 'http://localhost:3000/',
  empresa: '1',
  descuento: '0.1',
  headers: {
    // 'Access-Control-Allow-Methods': '*',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': '*',
    'tipo-cliente': 'app',
  },
})

export default config

export const API_URL = config.API_URL
export const loginURL = config.loginURL
export const headers = config.headers
export const empresa = config.empresa
export const DESCUENT = config.descuento
