
const routes = [
  // Redirigir al home.
  {
    path: '/',
    beforeEnter: (to, from, next) => next('/Login')
  },

  /** Rutas con layout y autenticacion **/
  {
    path: '/',
    component: () => import('layouts/Menu.vue'),
    meta: { requiresAuth: true },
    children: [
      { name: 'Home',path: '/Home', component: () => import('pages/Home.vue') },
      { name: 'Perfil',path: '/Perfil', component: () => import('pages/Perfil.vue') },
      // { name: 'Notifications',path: '/Notifications', component: () => import('pages/Notifications.vue') },
      { name: 'Configuracion',path: '/Configuracion', component: () => import('pages/Configuracion.vue') },
      { name: 'Admins',path: '/Admins', component: () => import('pages/Admins.vue') },
      { name: 'Paises',path: '/Paises', component: () => import('pages/Paises.vue') },
      { name: 'Regiones',path: '/Regiones', component: () => import('pages/Regiones.vue') },
      { name: 'Zonas',path: '/Zonas', component: () => import('pages/Zonas.vue') },
      { name: 'Clientes',path: '/Clientes', component: () => import('pages/Clientes.vue') },
      { name: 'Clientes',path: '/Clientes/:id', component: () => import('pages/Clientes.vue') },
      { name: 'Provincias',path: '/Provincias', component: () => import('pages/Provincias.vue') },
      { name: 'Aduana',path: '/Aduana', component: () => import('pages/Aduana.vue') },
      { name: 'Modalidades',path: '/Modalidades', component: () => import('pages/Modalidades.vue') },
      { name: 'Reportes',path: '/Reportes', component: () => import('pages/Reportes.vue') },
      { name: 'Transacciones',path: '/Transacciones', component: () => import('pages/Transacciones.vue') },
      { name: 'Envios',path: '/Envios', component: () => import('pages/Envios.vue') },
      { name: 'Envios',path: '/Envios/:id', component: () => import('pages/Envios.vue') },
      { name: 'Formulario',path: '/Formulario', component: () => import('pages/Formulario.vue') },
      { name: 'Cambiar_contraseña',path: '/Cambiar_contraseña', component: () => import('pages/Cambiar_contraseña.vue') },

    ]
  },

  {
    path: '/Notifications',
    component: () => import('layouts/Menu.vue'),
    children: [
      { name: 'unico',path: '/Notifications', component: () => import('pages/Notifications.vue') },
    ]
  },

  /** Rutas sin layout **/
  {
    path: '/Login',
    component: () => import('layouts/Empty.vue'),
    children: [
      { name: 'Login',path: '/Login/:token?', component: () => import('pages/Login.vue') },
    ]
  },

  /*
  {
    path: '/Formulario',
    component: () => import('layouts/Empty.vue'),
    children: [
      { name: 'Formulario',path: '/Formulario', component: () => import('pages/Formulario.vue') },
    ]
  },
  {
    path: '/Home',
    component: () => import('layouts/Menu.vue'),
    children: [
      { name: 'Home',path: '/Home', component: () => import('pages/Home.vue') },
    ]
  },
  {
    path: '/Cambiar_contraseña',
    component: () => import('layouts/Menu.vue'),
    children: [
      { name: 'Cambiar_contraseña',path: '/Cambiar_contraseña', component: () => import('pages/Cambiar_contraseña.vue') },
    ]
  },
  {
    path: '/Admins',
    component: () => import('layouts/Menu.vue'),
    children: [
      { name: 'Admins',path: '/Admins', component: () => import('pages/Admins.vue') },
    ]
  },
  {
    path: '/Perfil',
    component: () => import('layouts/Menu.vue'),
    children: [
      { name: 'Perfil',path: '/Perfil', component: () => import('pages/Perfil.vue') },
    ]
  },
  */


]

// No encontrado.
routes.push({
  path: '*',
  component: () => import('pages/Error404.vue')
})

export default routes
