<?php
    ini_set ('error_reporting', 0);
    include_once('funciones.php');
    $myfile = fopen("api.txt", "r"); 
    $api = fread($myfile, filesize("api.txt"));
    fclose($myfile);
    $code_pago = $_GET['id'];
    $resultado = consultaapinode($api . "envio/obtener-transaccion", "GET", "code_pago=" . $code_pago, "2");
    $resultado = json_decode($resultado);
    if ($resultado->encontrada === true) {
      $post_field = array( 
          "code_pago" => $code_pago,
          "response_payment" => "000000-1/En proceso",
          "estatus" => "EN PROCESO",
          "accion" => 0
      );
      $request = consultaapinode($api . "envio/actualizar-transaccion", "POST", $post_field, "1");
    }
    header("Content-Type: text/html;charset=utf-8");
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pago aprobado</title>
</head>
<script>
    let go = ""; 
    if (location.origin === "http://localhost") {
      go = "http://localhost:8081/#/Intro";
    } else {
      go = "https://enviaxa.com/envio-online/#/Intro";
    }
    setTimeout(() => {
        location.href = go;
    }, 4500);
</script>
<body style="background-color: #f5f5f5;height:100vh;display:flex;align-items: center; ">

    <!--Copia desde aquí-->
    <table style="max-width: 600px; padding: 10px; margin:0 auto; border-collapse: collapse;">
        <tr>
          <td style="background-color:#3d6ce0;text-align: -webkit-center; padding: 10px;">
      
            <img style="padding: 0; display: block" src="https://i.postimg.cc/mkKY94kS/logo-blanco.png" width="40%">
              
      
          </td>
        </tr>
      
        <tr>
          <td style="padding: 0;background-color:white;text-align: -webkit-center;">
      
            <h4 style="padding: 20px;margin-top: 0px;margin-bottom: 0px;"  >Somos mucho más que transporte, gestora de envíos, mensajería, paquetería, palet, etc. Nacional e internacional </h4>
          </td>
        </tr>
      
        <tr>
          <td style="padding: 0;background-color:white;text-align: -webkit-center;" >
            <div style="text-align: center;color: #3d6ce0;">
              <h1 style="margin:20px;font-weight:bold" >Se le informa que su pago ha sido aprobado</h1>
            
              <h1 style="margin:20px;font-weight:bold" >Los detalles de su envío han sido enviados a su correo </h1>
            </div>
          </td>
        </tr>
      
        <tr>
          <td style="background-color:white" >
            
            
          </td>
        </tr>
      
      </table>
    <!--hasta aquí-->
    
    </body>
</html>