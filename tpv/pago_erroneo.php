<?php
    ini_set ('error_reporting', 0);
    include_once('funciones.php');
    $myfile = fopen("api.txt", "r"); 
    $api = fread($myfile, filesize("api.txt"));
    fclose($myfile);
    $code_pago = $_GET['id'];
    $resultado = consultaapinode($api . "envio/obtener-transaccion", "GET", "code_pago=" . $code_pago, "2");
    $resultado = json_decode($resultado);
    if ($resultado->encontrada === true) {
      $post_field = array( 
          "code_pago" => $code_pago,
          "response_payment" => "000000-2/Rechazado o Cancelado",
          "estatus" => "RECHAZADO/CANCELADO",
          "accion" => 0
      );
      $request = consultaapinode($api . "envio/actualizar-transaccion", "POST", $post_field, "1");
    }
    header("Content-Type: text/html;charset=utf-8");
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pago erróneo</title>
</head>

<body style="background-color: #f5f5f5;height:100vh;display:flex;align-items: center; ">

    <!--Copia desde aquí-->
    <table style="max-width: 600px; padding: 10px; margin:0 auto; border-collapse: collapse;">
        <tr>
          <td style="background-color:#3d6ce0;text-align: -webkit-center; padding: 10px;">
      
            <img style="padding: 0; display: block" src="https://i.postimg.cc/mkKY94kS/logo-blanco.png" width="40%">
              
      
          </td>
        </tr>
      
        <tr>
          <td style="padding: 0;background-color:white;text-align: -webkit-center;">
      
            <h4 style="padding: 20px;margin-top: 0px;margin-bottom: 0px;"  >Somos mucho más que transporte, gestora de envíos, mensajería, paquetería, palet, etc. Nacional e internacional </h4>
          </td>
        </tr>
      
        <tr>
          <td style="padding: 0;background-color:white;text-align: -webkit-center;" >
            <div style="text-align: center;color: #3d6ce0;">
              <h1 style="margin:20px;font-weight:bold" >Se le informa que su pago ha sido rechazado</h1>
            </div>
          </td>
        </tr>
        <tr>
          <td style="padding: 0;background-color:white;text-align: -webkit-center;">
            <button id="againpay" type="button" style="background: #ff9800 !important;width: 40%;border: none;padding: 8px;border-radius: 2%;font-weight: bolder;color: #fff;cursor: pointer;box-shadow: 2px 2px #ccc;"> Reintentar pago</button>
          </td>
        </tr>
        <tr>
          <td style="background-color:white" >
            
            
          </td>
        </tr>
      
      </table>
    <!--hasta aquí-->
    <script>
      let go = ""; 
          if (location.origin === "http://localhost") {
            go = "transporteapp/tpv";
          } else {
            go = "envio-online/tpv";
          }
      const againpay = document.querySelector("button#againpay");
      againpay.onclick = pago_tpv;

      function pago_tpv() {
        againpay.style.display = "none";
        let uri = location.origin.replace(":8081", "");
        let envi_id = "<?php echo $resultado->envio_id ?>";
        if (envi_id) {
            
              let code_pago = String(new Date().getTime()).substr(4) + "T";

              let winName = '_self';
              let winURL = `${uri}/${go}/?op=cobro`;
              let windowoption = `location=yes,zoom=no,menubar=no,scrollbars=yes,toolbar=no,directories=no,resizable=no,top=80,left=30,height=${document.body.clientHeight - 100},width=${document.body.clientWidth - 100}`;
              let params = {
                metodo: `<?php echo $resultado->metodo_pago ?>`,
                metodo_text: `<?php echo $resultado->metodo_pago_text ?>`,
                amount: parseFloat(`<?php echo $resultado->importe ?>`),
                host: `${uri}/${go}`,
                mode: "prod",
                // mode: "dev",
                envio_id: `<?php echo $resultado->envio_id ?>`,
                api: `<?php echo $api ?>`,
                email: `<?php echo $resultado->email ?>`,
                code_pago: code_pago
              };
              let form = document.createElement("form");
              form.setAttribute("method", "post");
              form.setAttribute("action", winURL);
              form.setAttribute("target",winName);
              for (var i in params) {
                if (params.hasOwnProperty(i)) {
                  var input = document.createElement('input');
                  input.type = 'hidden';
                  input.name = i;
                  input.value = params[i];
                  form.appendChild(input);
                }
              }
              document.body.appendChild(form);
              window.open('', winName, windowoption);
              form.target = winName;
              form.submit();
              document.body.removeChild(form);
        }
      }
    </script>
    </body>
</html>