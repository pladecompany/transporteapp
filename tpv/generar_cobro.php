<?php
    ini_set ('error_reporting', 0);
    include_once('funciones.php');
    
	if(empty($_POST) || empty($_POST["metodo"]) || empty($_POST["amount"]) ||  empty($_POST["envio_id"])) {
        $respuesta["r"] = false;
        $respuesta["msj"] = "Error data not found";
        header("Content-type: application/json; charset=utf-8");
        echo json_encode($respuesta);
        return;
    }
    include_once('apiRedsys.php');
    
    $url_site = $_POST["host"];
    $miObj = new RedsysAPI;
    $id = $_POST["code_pago"];
    (FLOAT)$amount = abs($_POST["amount"]);
    $amount_save = number_format($amount, 2, '.', '');
    $amount = number_format($amount, 2, '', '');
    $amount = round($amount, 2);
    (FLOAT)$amount = $amount;
    // enviar al api con curl este id para registrarlo como nueva transaccion, si se registra continua con la operacion
    /* 
    $_POST["api"];
    envio_id, ($_POST["envio_id"])
    code_pago, ($id)
    email,
    proceso, (generar codigo) (accion al procesar el pago) para efectos de programacion, activar algo, generar algo, crear otro proceso etc.
    metodo_pago, $_POST["metodo_text"]
    estatus, (PAGO SIN FINALIZAR)
    response_payment, (por defecto null) al recibir la respuesta de TPV se actualiza
    fecha_registro,
    importe,
    fecha_actualizacion
    */
    $post_field = array(
         "envio_id" => $_POST["envio_id"], 
         "code_pago" => $id,
         "email" => $_POST["email"],
         "proceso" => "generar codigo",
         "metodo_pago" => $_POST["metodo"],
         "metodo_pago_text" => $_POST["metodo_text"],
         "estatus" => "PAGO SIN FINALIZAR",
         "importe" => (FLOAT)$amount_save
    );
    consultaapinode($_POST["api"] . "envio/agregar-transaccion", "POST", $post_field, "1");
    header("Content-Type: text/html;charset=utf-8");
    // SAVE API
      $handled = fopen("api.txt",'w+');
      fwrite($handled, $_POST["api"]);
      fclose($handled);
    
	$urlnoti=$url_site."/process_pay.php?&id=".$id;
	$urlOK=$url_site."/?op=exitoso&id=".$id;
	$urlKO=$url_site."/?op=erroneo&id=".$id;
    $concepto ="Orden numero: ".$id;    
    // if ($_POST["mode"] == "prod") { 
        $comercio_id = "175071505";
    // } else {
        // $comercio_id = "999008881";
    // }
	// Se Rellenan los campos
    $miObj->setParameter("DS_MERCHANT_AMOUNT", "" . $amount . "");
    $miObj->setParameter("DS_MERCHANT_ORDER", strval($id));
    $miObj->setParameter("DS_MERCHANT_MERCHANTCODE", $comercio_id); // codigo de comercio (Fuc) VALOR 1 *
    $miObj->setParameter("DS_MERCHANT_CURRENCY", "978");
    $miObj->setParameter("DS_MERCHANT_TRANSACTIONTYPE", "0");
    $miObj->setParameter("DS_MERCHANT_TERMINAL", "001");
    $miObj->setParameter("DS_MERCHANT_MERCHANTURL", $urlnoti);
    $miObj->setParameter("DS_MERCHANT_URLOK", $urlOK);       
    $miObj->setParameter("DS_MERCHANT_URLKO", $urlKO);
    $miObj->setParameter("DS_MERCHANT_MERCHANTDATA",$concepto);
    /* 
    Realizar al menos una operación Autorizada. Utilice esta tarjeta de prueba:
        Número de tarjeta: 4548812049400004 
        Caducidad: 12/20 
        Código CVV2: 123 
        Código CIP: 123456 
    Realizar al menos una operación Denegada. Utilice esta tarjeta de prueba:
        Número de tarjeta: 1111111111111117 
        Caducidad: 12/20 
    $_POST["metodo"]
    BIZUM	z	Se requiere activación por parte de la entidad.
    PAYPAL	p   Se requiere activación por parte de la entidad.
    Transferencia R   Se requiere activación por parte de la entidad.
    Masterpass	N    Se requiere activación por parte de la entidad.
    Con Tarjeta	C        
     */
    $miObj->setParameter("DS_MERCHANT_PAYMETHODS", $_POST["metodo"]);

   //Datos de configuración
    $version="HMAC_SHA256_V1";
    if ($_POST["mode"] == "prod") {
        $kc = 'ZC4zzjOyFNTu/qTBJDObQzG0N5VVlHwv';
    } else {
        $kc = 'sq7HjrUOBfKmC576ILgskD5srU870gJ7';//Clave recuperada de CANALES VALOR 2 *
    }
    // Se generan los parámetros de la petición
    $params = $miObj->createMerchantParameters();
    $signature = $miObj->createMerchantSignature($kc);
    if ($_POST["mode"] == "prod") {
        $urlgo = "https://sis.redsys.es/sis/realizarPago";
    } else {
        $urlgo = "https://sis-t.redsys.es:25443/sis/realizarPago";
    }
    echo "Redireccionando...";
    /* 
        PARA MAYOR INFO: aca en este link tambien pueden encontrar tarjetas de prueba
        https://pagosonline.redsys.es/entornosPruebas.html

        activar dominios para iframes
        Para ver qué dominios tienes permitidos sólo tienes que acceder al panel de administración de Redsys y, en el menú Comercio, mirar al final del apartado Datos de configuración. Ahí hay una opción llamada Dominios inSite permitidos.
    */
    // $decodec = $miObj->decodeMerchantParameters($params);
    // echo $decodec;
?>
<form id="formid" action="<?php echo $urlgo; ?>"  method="POST">
    <input type="hidden" name="Ds_SignatureVersion" value="<?php echo $version; ?>"/> 
    <input type="hidden" name="Ds_MerchantParameters" value="<?php echo $params; ?>"/> 
    <input type="hidden" name="Ds_Signature" value="<?php echo $signature; ?>"/> 
</form>
<script type="text/javascript">
    document.getElementById("formid").submit();
</script>