<?php		
		
		ini_set ('error_reporting', 0);
		include_once('apiRedsys.php');
        include_once('funciones.php');
	    $miObj = new RedsysAPI;
		// READ API
        $myfile = fopen("api.txt", "r"); 
        $api = fread($myfile, filesize("api.txt"));
        fclose($myfile);

        $version = $_POST["Ds_SignatureVersion"];
        $datos = $_POST["Ds_MerchantParameters"];
        $signatureRecibida = $_POST["Ds_Signature"];
        
        $decodec = $miObj->decodeMerchantParameters($datos);
        // consultaapinode($api . "envio/response-transaccion/?id=" . $code_pago, "POST", array("data" => $decodec), "1");
        // header("Content-Type: text/html;charset=utf-8");

        // DATOS IMPORTANTES 
        if ($api === "http://localhost:3000/") {
            $kc = 'sq7HjrUOBfKmC576ILgskD5srU870gJ7'; // DEV
        } else {
            $kc = 'ZC4zzjOyFNTu/qTBJDObQzG0N5VVlHwv'; // PROD
            // $kc = 'sq7HjrUOBfKmC576ILgskD5srU870gJ7'; // DEV

        }
        // DATOS IMPORTANTES 


        $firma = $miObj->createMerchantSignatureNotif($kc,$datos);	
        $code_pago = $_GET['id'];
        // con curl consultar al api
		// "SELECT * FROM transacciones WHERE code_pago='".$code_pago."' AND estatus='PAGO SIN FINALIZAR';";
		// $resultado que viene de la consulta al api
        $resultado = consultaapinode($api . "envio/obtener-transaccion", "GET", "code_pago=" . $code_pago, "2");
        $resultado = json_decode($resultado);
        // header("Content-Type: text/html;charset=utf-8");
        
		 
        if ($firma === $signatureRecibida && $resultado->encontrada === true){
            
            $codigoRespuesta = $miObj->getParameter("Ds_Response");
            
            // consultaapinode($api . "envio/response-transaccion/?id=" . $code_pago, "POST", array("data" => $codigoRespuesta), "1");
            
            if($codigoRespuesta=="9915") $text_response=$codigoRespuesta."/Denegada";
            else if($codigoRespuesta=="9998") $text_response=$codigoRespuesta."/Sin Finalizar";
            else if($codigoRespuesta=="0104") $text_response=$codigoRespuesta."/Operación no permitida para esa tarjeta o terminal.";	
            else if($codigoRespuesta=="0184") $text_response=$codigoRespuesta."/Error en la autenticación del titular.";	
            else if($codigoRespuesta=="0116") $text_response=$codigoRespuesta."/Disponible Insuficiente.";	
            else if($codigoRespuesta=="0909") $text_response=$codigoRespuesta."/Error de sistema.";	
            
            if($codigoRespuesta < 100) $text_response="".(($miObj->getParameter("Ds_AuthorisationCode")!="")?"".$miObj->getParameter("Ds_AuthorisationCode")."":"".$codigoRespuesta."")."/Autorizada";
            // CONDICIONALES PARA EL FINAL DE CADA TRANSACCION
            if($codigoRespuesta < 100){
                // SI ENTRA EN ESTA OPCION EL PAGO FUE EXITOSO Y EN EL API YA PUEDEN EJECUTAR ACCIONES
                // con curl enviamos un PUT al api solo actualizar (response_payment, fecha_actualizacion)
                // "UPDATE transacciones SET response_payment='".$text_response."' WHERE code_pago='".$code_pago."';";
                $post_field = array( 
                    "code_pago" => $code_pago,
                    "response_payment" => $text_response,
                    "estatus" => "PAGO VERIFICADO",
                    "accion" => 1
                );
                $request = consultaapinode($api . "envio/actualizar-transaccion", "POST", $post_field, "1");
                // header("Content-Type: text/html;charset=utf-8");
            }else{
                // SI ENTRA EN ESTA OPCION EL PAGO FUE RECHAZADO Y EN EL API YA PUEDEN EJECUTAR ACCIONES
                // con curl enviamos un PUT al api actualizar (response_payment, estatus, fecha_actualizacion)
                // "UPDATE transacciones SET estatus='PAGO NO VERIFICADO', response_payment='".$text_response."' WHERE code_pago='".$code_pago."';
                $post_field = array( 
                    "code_pago" => $code_pago,
                    "response_payment" => $text_response,
                    "estatus" => "PAGO NO VERIFICADO",
                    "accion" => 0
                );
                $request = consultaapinode($api . "envio/actualizar-transaccion", "POST", $post_field, "1");
                // header("Content-Type: text/html;charset=utf-8");
            }
            
        }
?>
