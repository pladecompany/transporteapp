import axios from 'axios'
import config from '../config'
import { LocalStorage } from 'quasar'

// Crear instancia de axios.
const axiosAPI = axios.create({
  baseURL: config.API_URL,
})

// Configurar los headers globalmente.
Object.assign(axiosAPI.defaults.headers.common, config.headers)

// Incluir token en cada peticion.
axiosAPI.interceptors.request.use(
  config => {
    const token = LocalStorage.getItem('TOKEN')

    if (token) {
      config.headers.token = token
    }

    return config
  },
  error => { Promise.reject(error) }
)

// Para que este disponible en los componentes y en store.
export default ({ store, Vue }) => {
  Vue.prototype.$axios = axios
  store.$axios = axios

  Vue.prototype.$api = axiosAPI
  store.$api = axiosAPI
}
