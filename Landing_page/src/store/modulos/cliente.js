import { SessionStorage } from 'quasar'

function defaultState() {
  return {
    datos: SessionStorage.getItem('CLIENTE'),
  }
}


const getters = {
  datos: state => state.datos,
  id: state => state.datos ? state.datos.id : null,
  dni: state => state.datos ? state.datos.dni : null,
  cod_telefono: state => state.datos ? state.datos.cod_telefono : null,
  telefono: state => state.datos ? state.datos.telefono : null,
  correo: state => state.datos ? state.datos.correo : null,
  coeficiente_envio: state => state.datos ? state.datos.coeficiente_envio : null,
  porcentaje_des: state => state.datos ? state.datos.porcentaje_des : null,
  nombre: state => state.datos ? state.datos.nombre : null,
  estatus: state => state.datos ? state.datos.estatus : null,
  es_temporal: state => state.datos ? state.datos.estatus == '1' : null,
  es_registrado: state => state.datos ? (state.datos.estatus == '2' || state.datos.estatus == '3') : null,
  es_cliente_uno: state => state.datos ? state.datos.estatus == '3' : null,
  domiciliacion: state => state.datos ? state.datos.domiciliacion_bancaria : null,
  es_empresa: state => state.datos ? state.datos.es_empresa : null,
  token: state => state.datos?.token,
  logueado: state => Boolean(state.datos?.token),
};


const mutations = {
  SET_DATOS: (state, datos) => {
    SessionStorage.set('CLIENTE', datos);
    SessionStorage.set('TOKEN', datos?.token);
    state.datos = datos;
  },

  RESET_STATE: (state) => Object.assign(state, defaultState()),
};


const actions = {
  set_estatus_temporal(ctx) {
    return new Promise((resolve, reject) => {
      const datos = SessionStorage.getItem('CLIENTE') || {};
      datos.estatus = '1';
      ctx.commit('SET_DATOS', datos);
    });
  },

  salir(ctx) {
    return new Promise((resolve) => {
      SessionStorage.remove('CLIENTE');
      SessionStorage.remove('TOKEN');
      ctx.commit('RESET_STATE');
      resolve();
    });
  },

  login(ctx, datos) {
    return new Promise((resolve, reject) => {
      this.$api
        .post('/auth/login-cliente', {dni: datos.dni, pass: datos.pass||datos.contraseña})
        .then(res => {
          ctx.commit('SET_DATOS', res.data);
          resolve(res.data);
        })
        .catch(reject)
    });
  },

  actualizar(ctx, datos) {
    return new Promise((resolve, reject) => {
      this.$api
        .post('/auth/act-cliente', {id: datos.id})
        .then(res => {
          ctx.commit('SET_DATOS', res.data);
          resolve(res.data);
        })
        .catch(reject)
    });
  },
};


export default {
  namespaced: true,
  state: defaultState,
  getters,
  mutations,
  actions,
}
