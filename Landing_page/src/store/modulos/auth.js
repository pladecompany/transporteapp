import { LocalStorage } from 'quasar'

function defaultState() {
  return {
    token: LocalStorage.getItem('TOKEN'),
    datos: LocalStorage.getItem('DATOS'),
    id: LocalStorage.getItem('ID'),
    checking_token: false,
  }
}


const getters = {
  token: state => state.token,
  logueado: state => Boolean(state.token),
  datos: state => state.datos,
  id: state => state.id,
  checking_token: state => state.checking_token
}


const mutations = {
  SET_ID: (state, id) => { state.id=id; LocalStorage.set('ID', id) },
  SET_TOKEN: (state, token) => { state.token=token; LocalStorage.set('TOKEN', token) },
  SET_DATOS: (state, datos) => { state.datos=datos; LocalStorage.set('DATOS', datos) },
  SET_CHECKING_TOKEN: (state, v) => { state.checking_token = Boolean(v) },

  DEL_ID: (state) => { state.id=null; LocalStorage.remove('ID') },
  DEL_TOKEN: (state) => { state.token=null; LocalStorage.remove('TOKEN') },
  DEL_DATOS: (state) => { state.datos=null; LocalStorage.remove('DATOS') },

  RESET_STATE: state => Object.assign(state, defaultState()),
}


const actions = {
  _process_login_data(ctx, datos) {
    return new Promise((resolve, reject) => {
      ctx.commit('SET_ID', datos.id)
      ctx.commit('SET_DATOS', datos)
      ctx.commit('SET_TOKEN', datos.token)
      resolve()
    })
  },

  login(ctx, datos) {
    const correo = datos.email || datos.correo
    const pass = datos.pass || datos.password || datos.clave
    return new Promise((resolve, reject) => {
      this.$api.post('/auth/login', {correo,pass})
        .then(resp => {
          const data = resp.data
          if(!data && !data.token){
            return reject(resp)
          }
          ctx.dispatch('_process_login_data', data).then(resolve, reject)
        })
        .catch(reject)
    })
  },

  salir(ctx) {
    return new Promise((resolve, reject) => {
      ctx.commit('DEL_ID')
      ctx.commit('DEL_DATOS')
      ctx.commit('DEL_TOKEN')
      resolve()
    })
  },

  fetch(ctx) {
    /* Recuperar los datos cuando se esta logueado */
    return new Promise((resolve, reject) => {
      this.$api.get('/admin/'+ctx.getters['id'])
        .then(resp => {
          const data = resp.data
          if (!data) return reject(resp)
          ctx.dispatch('_process_login_data', data)
        })
        .catch(reject);
    });
  },

  check_token(ctx) {
    /*
     * Verificar si el token todavia es valido
     * 1: valido
     * 0: invalido
     * -1: error (no se pudo comprobar)
     */
    return new Promise((resolve, reject) => {
      if (ctx.getters['checking_token']) {
        return resolve(-1);
      }
      ctx.commit('SET_CHECKING_TOKEN', true)

      const token = LocalStorage.getItem('TOKEN')
      this.$api.post('/auth/check-token', { token })
        .then(() => {
          ctx.commit('SET_TOKEN', token)
          resolve(1)
        })
        .catch(r => {
          const online = r.response && r.request;
          if (r.response && r.response.status == 401)
            resolve(0)
          else
            resolve(-1)
        })
        .finally(() => ctx.commit('SET_CHECKING_TOKEN', false))

    });
  },

}


export default {
  namespaced: true,
  state: defaultState,
  getters,
  mutations,
  actions,
}
