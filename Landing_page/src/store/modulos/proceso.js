import { SessionStorage as SS } from 'quasar'

function defaultState() {
  return {
    desde_pais: SS.getItem('DESDEPAIS'),
    desde_provincia: SS.getItem('DESDEPROVINCIA'),
    desde_direccion: SS.getItem('DESDEDIRECCION'),
    desde_codigo_postal: SS.getItem('DESDECP'),
    desde_localidad: SS.getItem('DESDELOCALIDAD'),

    destino_pais: SS.getItem('DESTINOPAIS'),
    destino_provincia: SS.getItem('DESTINOPROVINCIA'),
    destino_direccion: SS.getItem('DESTINODIRECCION'),
    destino_codigo_postal: SS.getItem('DESTINOCP'),
    destino_localidad: SS.getItem('DESTINOLOCALIDAD'),

    seguro: SS.getItem('SEGURO'),
    reembolso: SS.getItem('REEMBOLSO'),
    modalidad: SS.getItem('MODALIDAD'),
    bultos: SS.getItem('BULTOS'),

    fecha_recogida: SS.getItem('FECHA_RECOGIDA'),
    hora_recogida_desde: SS.getItem('HORA_RECOGIDA_DESDE'),
    hora_recogida_hasta: SS.getItem('HORA_RECOGIDA_HASTA'),

    es_destinatario_empresa:  SS.getItem('ES_DESTINATARIO_EMPRESA'),
    es_cliente_empresa: SS.getItem('ES_CLIENTE_EMPRESA'),

    nombre_destinatario: SS.getItem('NOMBRE_DESTINATARIO'),
    nombre_contacto_destinatario: SS.getItem('NOMBRE_CONTACTO_DESTINATARIO'),
    correo_destinatario: SS.getItem('CORREO_DESTINATARIO'),
    telefono_destinatario: SS.getItem('TELEFONO_DESTINATARIO'),
    cod_telefono_destinatario: SS.getItem('COD_TELEFONO_DESTINATARIO'),

    dni_remitente: SS.getItem('DNI_REMITENTE'),
    nombre_remitente: SS.getItem('NOMBRE_REMITENTE'),
    empresa_remitente: SS.getItem('EMPRESA_REMITENTE'),
    correo_remitente: SS.getItem('CORREO_REMITENTE'),
    telefono_remitente: SS.getItem('TELEFONO_REMITENTE'),
    cod_telefono_remitente: SS.getItem('COD_TELEFONO_REMITENTE'),


    envio: SS.getItem('ENVIO'),
  }
}


const getters = {
  desde_pais: state => state.desde_pais,
  desde_provincia: state => state.desde_provincia,
  desde_direccion: state => state.desde_direccion,
  desde_codigo_postal: state => state.desde_codigo_postal,
  desde_localidad: state => state.desde_localidad,

  destino_pais: state => state.destino_pais,
  destino_provincia: state => state.destino_provincia,
  destino_direccion: state => state.destino_direccion,
  destino_codigo_postal: state => state.destino_codigo_postal,
  destino_localidad: state => state.destino_localidad,

  seguro: state => state.seguro,
  reembolso: state => state.reembolso,
  modalidad: state => state.modalidad,
  bultos: state => state.bultos,
  documentacion: state => state.documentacion,

  fecha_recogida: state => state.fecha_recogida,
  hora_recogida_desde: state => state.hora_recogida_desde,
  hora_recogida_hasta: state => state.hora_recogida_hasta,

  es_destinatario_empresa: state => state.es_destinatario_empresa,
  es_cliente_empresa: state => state.es_cliente_empresa,

  nombre_destinatario: state => state.nombre_destinatario,
  nombre_contacto_destinatario: state => state.nombre_contacto_destinatario,
  correo_destinatario: state => state.correo_destinatario,
  telefono_destinatario: state => state.telefono_destinatario,
  cod_telefono_destinatario: state => state.cod_telefono_destinatario,

  dni_remitente: state => state.dni_remitente,
  nombre_remitente: state => state.nombre_remitente,
  empresa_remitente: state => state.empresa_remitente,
  correo_remitente: state => state.correo_remitente,
  telefono_remitente: state => state.telefono_remitente,
  cod_telefono_remitente: state => state.cod_telefono_remitente,

  envio: state => state.envio,

  datos: state => {
    if (!state.desde_pais)
      return null;

    return {
      desde_pais: state.desde_pais,
      desde_provincia: state.desde_provincia,
      desde_direccion: state.desde_direccion,
      desde_codigo_postal: state.desde_codigo_postal,
      desde_localidad: state.desde_localidad,

      destino_pais: state.destino_pais,
      destino_provincia: state.destino_provincia,
      destino_direccion: state.destino_direccion,
      destino_codigo_postal: state.destino_codigo_postal,
      destino_localidad: state.destino_localidad,

      seguro: state.seguro,
      reembolso: state.reembolso,
      modalidad: state.modalidad,
      bultos: state.bultos,
      documentacion: state.documentacion,

      fecha_recogida: state.fecha_recogida,
      hora_recogida_desde: state.hora_recogida_desde,
      hora_recogida_hasta: state.hora_recogida_hasta,

      es_destinatario_empresa: state.es_destinatario_empresa,
      es_cliente_empresa: state.es_cliente_empresa,

      nombre_destinatario: state.nombre_destinatario,
      nombre_contacto_destinatario: state.nombre_contacto_destinatario,
      correo_destinatario: state.correo_destinatario,
      telefono_destinatario: state.telefono_destinatario,
      cod_telefono_destinatario: state.cod_telefono_destinatario,

      nombre_remitente: state.nombre_remitente,
      empresa_remitente: state.empresa_remitente,
      dni_remitente: state.dni_remitente,
      correo_remitente: state.correo_remitente,
      telefono_remitente: state.telefono_remitente,
      cod_telefono_remitente: state.cod_telefono_remitente,

      envio: state.envio,
    };
  },
};


const mutations = {
  DESDE_PAIS: (state, pais) => {
    SS.set('DESDEPAIS',pais)
    state.desde_pais = pais
  },
  DESDE_PROVINCIA: (state, provincia) => {
    SS.set('DESDEPROVINCIA',provincia)
    state.desde_provincia = provincia
  },
  DESDE_DIRECCION: (state, direccion) => {
    SS.set('DESDEDIRECCION',direccion)
    state.desde_direccion = direccion
  },
  DESDE_CODIGO_POSTAL: (state, codigo) => {
    SS.set('DESDECP',codigo)
    state.desde_codigo_postal = codigo
  },
  DESDE_LOCALIDAD: (state, data) => {
    SS.set('DESDELOCALIDAD',data)
    state.desde_localidad = data
  },


  DESTINO_PAIS: (state, pais) => {
    SS.set('DESTINOPAIS',pais)
    state.destino_pais = pais
  },
  DESTINO_PROVINCIA: (state, provincia) => {
    SS.set('DESTINOPROVINCIA',provincia)
    state.destino_provincia = provincia
  },
  DESTINO_DIRECCION: (state, direccion) => {
    SS.set('DESTINODIRECCION',direccion)
    state.destino_direccion = direccion
  },
  DESTINO_CODIGO_POSTAL: (state, codigo) => {
    SS.set('DESTINOCP',codigo)
    state.destino_codigo_postal = codigo
  },
  DESTINO_LOCALIDAD: (state, data) => {
    SS.set('DESTINOLOCALIDAD',data)
    state.destino_localidad = data
  },

  SEGURO: (state, monto) => {
    SS.set('SEGURO',monto)
    state.seguro = monto
  },
  REEMBOLSO: (state, monto) => {
    SS.set('REEMBOLSO',monto)
    state.reembolso = monto
  },
  MODALIDAD: (state, modalidad) => {
    SS.set('MODALIDAD',modalidad)
    state.modalidad = modalidad
  },

  BULTOS: (state, bultos) => {
    SS.set('BULTOS',bultos)
    state.bultos = JSON.parse(JSON.stringify(bultos||null))
  },

  DOCUMENTACION: (state, doc) => {
    SS.set('DOCUMENTACION',doc)
    state.documentacion = Boolean(doc);
  },

  ES_CLIENTE_EMPRESA: (state, v) => {
    SS.set('ES_CLIENTE_EMPRESA',v)
    state.es_cliente_empresa = Boolean(v);
  },

  ES_DESTINATARIO_EMPRESA: (state, v) => {
    SS.set('ES_DESTINATARIO_EMPRESA',v)
    state.es_destinatario_empresa = Boolean(v);
  },

  FECHA_RECOGIDA: (state, f) => {
    SS.set('FECHA_RECOGIDA',f)
    state.fecha_recogida = f;
  },

  HORA_RECOGIDA_DESDE: (state, h) => {
    SS.set('HORA_RECOGIDA_DESDE',h)
    state.hora_recogida_desde = h;
  },

  HORA_RECOGIDA_HASTA: (state, h) => {
    SS.set('HORA_RECOGIDA_HASTA',h)
    state.hora_recogida_hasta = h;
  },

  NOMBRE_DESTINATARIO: (state, v) => {
    SS.set('NOMBRE_DESTINATARIO',v)
    state.nombre_destinatario = v;
  },

  NOMBRE_CONTACTO_DESTINATARIO: (state, v) => {
    SS.set('NOMBRE_CONTACTO_DESTINATARIO',v)
    state.nombre_contacto_destinatario = v;
  },

  TELEFONO_DESTINATARIO: (state, v) => {
    SS.set('TELEFONO_DESTINATARIO',v)
    state.telefono_destinatario = v;
  },

  COD_TELEFONO_DESTINATARIO: (state, v) => {
    SS.set('COD_TELEFONO_DESTINATARIO',v)
    state.cod_telefono_destinatario = v;
  },

  CORREO_DESTINATARIO: (state, v) => {
    SS.set('CORREO_DESTINATARIO',v)
    state.correo_destinatario = v;
  },

  DNI_REMITENTE: (state, v) => {
    SS.set('DNI_REMITENTE',v)
    state.dni_remitente = v;
  },

  NOMBRE_REMITENTE: (state, v) => {
    SS.set('NOMBRE_REMITENTE',v)
    state.nombre_remitente = v;
  },

  EMPRESA_REMITENTE: (state, v) => {
    SS.set('EMPRESA_REMITENTE',v)
    state.empresa_remitente = v;
  },

  TELEFONO_REMITENTE: (state, v) => {
    SS.set('TELEFONO_REMITENTE',v)
    state.telefono_remitente = v;
  },

  COD_TELEFONO_REMITENTE: (state, v) => {
    SS.set('COD_TELEFONO_REMITENTE',v)
    state.cod_telefono_remitente = v;
  },

  CORREO_REMITENTE: (state, v) => {
    SS.set('CORREO_REMITENTE',v)
    state.correo_remitente = v;
  },

  ENVIO: (state, v) => {
    SS.set('ENVIO',v)
    state.envio = v;
  },

  RESET_STATE: (state) => Object.assign(state, defaultState()),
};


const actions = {
  limpiar(ctx) {
    return new Promise((resolve) => {
      SS.remove('DESDEPAIS')
      SS.remove('DESDEPROVINCIA')
      SS.remove('DESDEDIRECCION')
      SS.remove('DESDECP')
      SS.remove('DESDELOCALIDAD')
      SS.remove('DESTINOPAIS')
      SS.remove('DESTINOPROVINCIA')
      SS.remove('DESTINODIRECCION')
      SS.remove('DESTINOCP')
      SS.remove('DESTINOLOCALIDAD')

      SS.remove('SEGURO')
      SS.remove('REEMBOLSO')
      SS.remove('MODALIDAD')
      SS.remove('BULTOS')
      SS.remove('DOCUMENTACION')

      SS.remove('ES_DESTINATARIO_EMPRESA')
      SS.remove('ES_CLIENTE_EMPRESA')

      SS.remove('FECHA_RECOGIDA')
      SS.remove('HORA_RECOGIDA_HASTA')
      SS.remove('HORA_RECOGIDA_DESDE')
      SS.remove('DNI_REMITENTE')
      SS.remove('NOMBRE_DESTINATARIO')
      SS.remove('NOMBRE_CONTACTO_DESTINATARIO')
      SS.remove('CORREO_DESTINATARIO')
      SS.remove('CORREO_REMITENTE')
      SS.remove('NOMBRE_REMITENTE')
      SS.remove('EMPRESA_REMITENTE')
      SS.remove('TELEFONO_REMITENTE')
      SS.remove('COD_TELEFONO_REMITENTE')
      SS.remove('TELEFONO_DESTINATARIO')
      SS.remove('COD_TELEFONO_DESTINATARIO')

      SS.remove('ENVIO')

      ctx.commit('RESET_STATE');

      resolve()
    });
  },

  guardar_datos(ctx, datos) {
    return new Promise((resolve) => {
      // if (datos.desde && datos.hasta) {
      //   ctx.commit('DESDE_PAIS', datos.desde.id_pais);
      //   ctx.commit('DESDE_PROVINCIA', datos.desde.id_pais);
      // }

      ctx.commit('DESDE_PAIS', datos.desde_pais);
      ctx.commit('DESDE_PROVINCIA', datos.desde_provincia);
      ctx.commit('DESDE_DIRECCION', datos.desde_direccion);
      ctx.commit('DESDE_CODIGO_POSTAL', datos.desde_codigo_postal);

      ctx.commit('DESTINO_PAIS', datos.destino_pais);
      ctx.commit('DESTINO_PROVINCIA', datos.destino_provincia);
      ctx.commit('DESTINO_DIRECCION', datos.destino_direccion);
      ctx.commit('DESTINO_CODIGO_POSTAL', datos.destino_codigo_postal);

      ctx.commit('SEGURO', datos.seguro || datos.monto_seguro);
      ctx.commit('REEMBOLSO', datos.reembolso || datos.monto_reembolso);
      ctx.commit('MODALIDAD', datos.modalidad);
      ctx.commit('BULTOS', datos.bultos);
      ctx.commit('DOCUMENTACION', datos.documentacion);
    });
  },

};


export default {
  namespaced: true,
  state: defaultState,
  getters,
  mutations,
  actions,
}
