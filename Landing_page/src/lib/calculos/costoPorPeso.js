import pesoEnRango from './pesoEnRango'
import indicePesoMaximo from './indicePesoMaximo'

export default function costoPorPeso(peso_a_cobrar, _pesos, tarifa_adicional, franja_adicional=null)
{

  if (peso_a_cobrar == 0) {
    return 0;
  }


  // Ordenar por 'hasta' descendente.
  const pesos = Array.from(_pesos);
  pesos.sort((a,b) => a.hasta - b.hasta);

  const peso_a_cobrar_ceil = Math.ceil(peso_a_cobrar);


  if (!pesoEnRango(peso_a_cobrar, pesos, franja_adicional))
  {
    let peso_maximo = pesos[ indicePesoMaximo(pesos) ].hasta;
    let costo_peso_maximo = pesos[ indicePesoMaximo(pesos) ].costo;

    let costo = (peso_a_cobrar_ceil - peso_maximo) * tarifa_adicional + costo_peso_maximo;
    if (franja_adicional)
      costo += tarifa_adicional;

    return costo;
  }


  let i,p;
  for (i = 0; i < pesos.length; i++) {
    p = pesos[i];

    if (p.desde <= peso_a_cobrar && peso_a_cobrar <= p.hasta) {
      if (franja_adicional && pesos[i+1]) {
        return pesos[i+1].costo;
      }
      else {
        return p.costo;
      }
    }
  }



  // Tomar el primer rango que aplique al peso.
  // for (let p of pesos) {
  //   if (p.desde<=peso_a_cobrar && peso_a_cobrar <= p.hasta) return p.costo;
  // }


  // return this.costoDelPesoEnRango(peso_a_cobrar);
}
