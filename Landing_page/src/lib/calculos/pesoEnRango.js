import indicePesoMaximo from './indicePesoMaximo'

export default function pesoEnRango(peso, _pesos, franja_adicional=false) {
  let en_rango = true;

  // Ordenar por 'hasta' ascendente.
  const pesos = Array.from(_pesos);
  pesos.sort((a,b) => a.hasta - b.hasta);


  const i_peso_max = indicePesoMaximo(pesos);
  if (i_peso_max < 0)
    return i_peso_max;

  if (franja_adicional) {
    // const pesos = Array.from(pesos);
    // pesos.sort((a,b) => b.hasta - a.hasta);
    const irango = pesos.findIndex(p => p.desde<=peso && p.hasta>=peso);
    const rango_siguiente = pesos[irango+1];

    if (!rango_siguiente) {
      return false;
    }

  }

  if (pesos[i_peso_max].hasta < peso)
    return false;

  return true;
}
