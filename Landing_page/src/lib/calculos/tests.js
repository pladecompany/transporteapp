import pesoEnRango from './pesoEnRango'
import indicePesoMaximo from './indicePesoMaximo'
import costoPorPeso from './costoPorPeso'

export default function tests() {
  const log = console.log;

  const pesos = [
    { desde:0, hasta:2, costo:6},
    { desde:2.1, hasta:5, costo:7},
    { desde:5.1, hasta:10, costo:8},
  ];
  const tarifa_adicional = 0.5;



  log('----------Probando con pesos:', pesos);

  log('(indicePesoMaximo:pesos)(2):', indicePesoMaximo(pesos) );

  log('(pesoEnRango:1)(true):', pesoEnRango(1, pesos) );
  log('(pesoEnRango:10)(true):', pesoEnRango(10, pesos) );
  log('(pesoEnRango+franja:10)(false):', pesoEnRango(10, pesos, true) );
  log('(pesoEnRango:11)(false):', pesoEnRango(11, pesos) );

  log('(costoPorPeso:46.28)(26.5):', costoPorPeso(46.28, pesos, tarifa_adicional) );
  log('(costoPorPeso+franja:46.28)(27):', costoPorPeso(46.28, pesos, tarifa_adicional, true) );


  log('(pesoEnRango:1.5)(true):', pesoEnRango(1.5, pesos) );
  log('(pesoEnRango+franja:1.5)(true):', pesoEnRango(1.5, pesos, true) );

  log('(costoPorPeso:1.5)(6):', costoPorPeso(1.5, pesos, tarifa_adicional) );
  log('(costoPorPeso+franja:1.5)(7):', costoPorPeso(1.5, pesos, tarifa_adicional, true) );
}

