export default function Quitarcoma(value) {
    if(value) {
        for (var i = 0; i < value.split(".").length+10; i++) {
            value = value.replace(".", "");
        }
        value = value.replace(",", ".");
    }
    if(value.length=="3" || value.length=="2") {
        if(value.split(".").length==0)
            value = "0."+""+value;
        else
        value = "0"+""+value;
    }
    return parseFloat(value);
}
