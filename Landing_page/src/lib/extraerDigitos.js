export default function extraerDigitos(str) {
  /*
   * extraerDigitos('h01a mund0') -> '010'
   */
  str = '' + str;
  return (str.match(/\d/g) || []).join('');
}
