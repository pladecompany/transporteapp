
const routes = [
  // Redirigir al home.
  {
    path: '/',
    beforeEnter: (to, from, next) => next('/Intro')
  },
  {
    path: '/',
    component: () => import('layouts/Menu.vue'),
    children: [
      { name: 'Intro',path: '/Intro', component: () => import('pages/Intro.vue') },
      { name: 'Proceso',path: '/Proceso', props:true, component: () => import('pages/Proceso.vue') },

    ]
  },
  {
    path: '/',
    component: () => import('layouts/Menu.vue'),
    // meta: { requiresAuth: true },
    children: [
      { name: 'Conexionestablecida',path: '/Conexionestablecida', component: () => import('pages/Conexionestablecida.vue') },
      { name: 'Construccion',path: '/Construccion', component: () => import('pages/Construccion.vue') },
      { name: 'Logs', path: '/Logs', component: () => import('pages/Logs.vue') },
    ]
  },
  // {
  //   path: '/Conexionestablecida',
  //   component: () => import('layouts/Menu.vue'),
  //   children: [
  //     { name: 'Conexionestablecida',path: '/Conexionestablecida', component: () => import('pages/Conexionestablecida.vue') },
  //   ]
  // },
  // {
  //   path: '/Construccion',
  //   component: () => import('layouts/Menu.vue'),
  //   children: [
  //     { name: 'Construccion',path: '/Construccion', component: () => import('pages/Construccion.vue') },
  //   ]
  // },
  {
    path: '/Tests',
    component : () =>  import('pages/Tests.vue'),
  },
]

// No encontrado.
routes.push({
  path: '*',
  component: () => import('pages/Error404.vue')
})

export default routes
