const REGEXP_VALIDAR_CORREO = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export default {
  methods: {
    validarCorreo: function (correo) {
      let validez = true;

      if(!correo) validez = 'Por favor escribe tu correo';
      else if(!REGEXP_VALIDAR_CORREO.test(correo)) validez = 'Por favor ingresa un correo válido';

      return validez;
    },

    validarTelefono: function (numero) {
      let validez = true;
      if(!numero){
        validez = 'Por favor ingresa tu número';
      }
      // Extraer digitos.
      numero = (numero.toString().match(/\d/g) || []).join('');
      if (numero.length < 10 || numero.length > 16){
        validez = 'Número inválido';
      }

      return validez;
    },
  }
}
