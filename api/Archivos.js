const multer = require("multer");
const crypto = require("crypto");

var storage = multer.diskStorage({
    destination: async function(req, file, cb) {
        await cb(null, __dirname + "/public/uploads/");
    },
    filename: async function(req, file, cb) {
        console.log("Archivo: "+ file.originalname + " | " + file.mimetype);
        let customFileName = crypto.randomBytes(18).toString("hex"),
            fileExtension = file.originalname.split(".")[file.originalname.split(".").length-1]; // get file extension from original file name
        await cb(null, customFileName + "." + fileExtension);
    }
});

module.exports = multer({
    storage: storage,
    onError: function(err, next) {
        console.log("error", err);
        next(err);
    }
});
