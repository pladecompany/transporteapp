//Importar librerias
const Knex = require("knex");
const morgan = require("morgan");
const express = require("express");
const promiseRouter = require("express-promise-router");
const bodyParser = require("body-parser");

const os =  require("os");
const Server = os.hostname();
const knexConfig = require("./knexfile.js");
const https = require("https");
const fs = require("fs");


if (Server == knexConfig.Hosname) {
  var privateKey  = fs.readFileSync('/etc/ssl/private/inthecompanies.key', 'utf8');
  var certificate = fs.readFileSync('/etc/ssl/inthecompanies_com.crt', 'utf8');
  var ca = fs.readFileSync('/etc/ssl/inthecompanies_com.ca-bundle', 'utf8');
  var credentials = {ca, key: privateKey, cert: certificate};
}


//Importar Config
const { Model } = require("objection");

//Importar Validador de errores
const ValidadorDeErrores = require("./ValidarErrores");
var knex;

if (Server == knexConfig.Hosname) {
  knex = Knex(knexConfig.production);
} else {
    //knex = Knex(knexConfig.production);
    knex = Knex(knexConfig.development);
}

Model.knex(knex);

const router = promiseRouter();

//CORS middleware
const allowCrossDomain = function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "*");
  res.header("Access-Control-Allow-Headers", "*");
  next();
};

//Iniciar Express
const app = express()
  .use(bodyParser.urlencoded({ extended: true }))
  .use(bodyParser.json())
  .use(router)
  //.use(allowCrossDomain)
  .set("json spaces", 2);
  app.all('*', function(req, res,next) {
    var responseSettings = {
      "AccessControlAllowOrigin": req.headers.origin,
      "AccessControlAllowHeaders": "Content-Type,X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5,  Date, X-Api-Version, X-File-Name",
      "AccessControlAllowMethods": "POST, GET, PUT, DELETE, OPTIONS",
      "AccessControlAllowCredentials": true
    };
    res.header("Access-Control-Allow-Credentials", responseSettings.AccessControlAllowCredentials);
    res.header("Access-Control-Allow-Origin",  responseSettings.AccessControlAllowOrigin);
    res.header("Access-Control-Allow-Headers", (req.headers['access-control-request-headers']) ? req.headers['access-control-request-headers'] : "x-requested-with");
    res.header("Access-Control-Allow-Methods", (req.headers['access-control-request-method']) ? req.headers['access-control-request-method'] : responseSettings.AccessControlAllowMethods);

    if ('OPTIONS' == req.method) {
      res.sendStatus(200);
    }
    else {
      next();
    }
});
if (!process.env.TEST) {
  app.use(morgan("dev"));
}

//Definir las rutas en la carpeta "routes"
definirRutas([
  'auth',
  'admin',
  'pais',
  'region',
  'provincia',
  'zona',
  'configuracion',
  'aduana',
  'cliente',
  'modalidad',
  'envio',
  'notificaciones',
  'reporte',
  'transacciones',
]);
//
app.use(ValidadorDeErrores);

// //Definir rutas de archivos
app.get("/public/uploads/:filename", function(req, res) {
  console.log(req.params.filename);
  res.sendFile(__dirname + `/public/uploads/${req.params.filename}`);
});
// app.get("/assets/:filename", function(req, res) {
//   console.log(req.params.filename);
//   res.sendFile(__dirname + `/assets/${req.params.filename}`);
// });

// Iniciar el servidor en un puerto
// Si es un test regresar el objecto app
if (process.env.TEST) {
  module.exports = app;
}
else if (Server == knexConfig.Hosname){
  var httpsServer = https.createServer(credentials, app);
  httpsServer.listen(knexConfig.PORT);
}
else {
  var server = app.listen(knexConfig.PORT, () => {
    console.log(
      "API Corriendo en puerto: %s",
      server.address().port
    );
  });
}

//--------------------------------------------INICIO SOCKET-------------------
const corssocket = {
  cors: {
    origin: "*" ,
    métodos: "*"
  }
};
if (Server == knexConfig.Hosname){
var io = require("socket.io")(httpsServer,corssocket);
} else {
var io = require("socket.io")(server,corssocket);
}

const USUARIOSC = []; // Usuarios conectados en la app
global.USUARIOSC = USUARIOSC; // para usar el arreglo de usuarios conectados en cualquier parte del api...(donde sea)
global.IO = io; // para hacer emits desde cualquier parte del api (de donde sea)


//-----------------------------------------------------------------FIN SOCKET

function definirRutas(rutas) {
  rutas.forEach(ruta => {
    app.use(`/${ruta}`, require(`./routes/${ruta}`));
  });
}


