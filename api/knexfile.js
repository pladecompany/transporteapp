// Update with your config settings.

module.exports = {
  PORT: 3000,

  development: {
    client: 'mysql',
    connection: {
      database: 'transporte_app',
      user:     'root',
      password: '',
      host: 'localhost'
    },
    migrations: {
        directory: "./migrations"
    },
  },


  production: {
    client: 'mysql',
    connection: {
      database: 'transporte_app',
      user:     'root',
      password: ''
    },
    migrations: {
        directory: "./migrations"
    },
  }

};
