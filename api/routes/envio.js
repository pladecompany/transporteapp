const { raw, ref } = require('objection');
const Zona = require('../models/Zona');
const Cliente = require('../models/Cliente');
const Pais = require('../models/Pais');
const Envio = require('../models/Envio');
const EnvioBulto = require('../models/EnvioBulto');
const Aduana = require('../models/Aduana');
const Modalidad = require('../models/Modalidad');
const Provincia = require('../models/Provincia');
const PagoEnvio = require('../models/PagoEnvio');
const Admin = require('../models/Admin');
const Notificaciones = require("../models/Notificacion");
const Transacciones = require("../models/Transacciones");


const validar = require('./sesion2');
const AR = require('../ApiResponser');
const upload = require('multer')();
const router = require('express').Router();

const sanitizeString = require('../functions/sanitizeString');
const checkUrl = require('../functions/checkUrl');

const enviarSeguimiento = require('../mail/senders/seguimiento');
const enviarResumen = require('../mail/senders/pagos');

const costoPorPeso = require('../functions/calculos/costoPorPeso');
const indicePesoMaximo = require('../functions/calculos/indicePesoMaximo');
const pesoEnRango = require('../functions/calculos/pesoEnRango');
const costoDeBultos = require('../functions/calculos/costoDeBultos');
const totalEnvio = require('../functions/calculos/totalEnvio');


const isNum = n => +n===+n;

const PA = require('../data/id_paises');
const PR = require('../data/id_provincias');
const crypto = require("crypto");
const config = require("../config");
const moment = require("moment");
moment.locale("es");

const {
  ID_PROVINCIAS_ESPECIALES,
  ID_PROVINCIAS_MAS_FRANJA_PRECIO,
  ENVIOS_NO_REALIZABLES,
} = require('../data/casos_envio');



const
  HTTP_OK = 200,
  HTTP_BAD_REQUEST = 400,
  HTTP_NOT_FOUND = 404,
  HTTP_UNAUTHORIZED = 401,
  HTTP_SERVER_ERROR = 500


// Listar envios.
router.get('/', validar('admin'), async(req, res) => {
  const params = req.query;

  const Query = Envio.query()
    .select(
      'envio.*',
      'cliente.dni AS dni_cliente',
      'cliente.nombre AS nombre_cliente',
      'cliente.cod_telefono AS cod_telefono_cliente',
      'cliente.telefono AS telefono_cliente',
      'cliente.correo AS correo_cliente',
      'cliente.empresa AS empresa_cliente',
      'modalidad.nombre AS modalidad',
      'pago_envio.tipo_pago',
      'transacciones.metodo_pago_text',
      'transacciones.estatus AS estatus_transaccion',
      'transacciones.code_pago AS code_pago',
      b => b.select('nombre').from('pais').where({ id: ref('id_pais_origen') }).as('pais_origen'),
      b => b.select('nombre').from('pais').where({ id: ref('id_pais_destino') }).as('pais_destino'),
      b => b.select('nombre').from('provincia').where({ id: ref('id_provincia_origen') }).as('provincia_origen'),
      b => b.select('nombre').from('provincia').where({ id: ref('id_provincia_destino') }).as('provincia_destino'),
    )
    .leftJoin('cliente', 'cliente.id', 'envio.id_cliente')
    .leftJoin('modalidad', 'modalidad.id', 'envio.id_modalidad')
    .leftJoin('pago_envio', 'pago_envio.id_envio', 'envio.id')
    .leftJoin('transacciones', 'transacciones.envio_id', 'envio.id')

  if (+params.page === +params.page)
    Query.page(params.page, +params.pagesize ? params.pagesize : undefined)

  if (params.filter && params.filter.length > 1){
    Query.having('dni_cliente', 'LIKE', '%'+sanitizeString(params.filter)+'%');
    Query.orHaving('nombre_cliente', 'LIKE', '%'+sanitizeString(params.filter)+'%');
    Query.orHaving('nombre_destinatario', 'LIKE', '%'+sanitizeString(params.filter)+'%');
    Query.orHaving('codigo_generado', 'LIKE', '%'+sanitizeString(params.filter)+'%');
  }

  Query.where(b => b.where('transacciones.estatus', '<>', 'RECHAZADO/CANCELADO').orWhereNull('transacciones.estatus'));
  Query.where('envio.estatus',1);
  Query.orderBy('fecha_envio', "DESC");

  await Query.withGraphFetched('bultos')
    .then(envios => AR.enviarDatos(envios, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

router.get('/cliente/:id(\\d+)', /*validar('admin'),*/ async(req, res) => {
  const params = req.query;

  const Query = Envio.query()
    .select(
      'envio.*',
      'cliente.dni AS dni_cliente',
      'cliente.nombre AS nombre_cliente',
      'cliente.cod_telefono AS cod_telefono_cliente',
      'cliente.telefono AS telefono_cliente',
      'cliente.correo AS correo_cliente',
      'cliente.empresa AS empresa_cliente',
      'modalidad.nombre AS modalidad',
      raw('(SELECT tipo_pago FROM pago_envio WHERE id_envio=envio.id order By id desc LIMIT 1)').as('modalidad_pago_db'),
      raw('(SELECT metodo_pago_text FROM transacciones WHERE envio_id=envio.id AND estatus="PAGO VERIFICADO" order By id desc LIMIT 1)').as('modalidad_pago'),
      raw('(SELECT code_pago FROM transacciones WHERE envio_id=envio.id AND estatus="PAGO VERIFICADO" order By id desc LIMIT 1)').as('referencia_pago'),
      raw('(SELECT importe FROM transacciones WHERE envio_id=envio.id AND estatus="PAGO VERIFICADO" order By id desc LIMIT 1)').as('importe_pago'),
      b => b.select('nombre').from('pais').where({ id: ref('id_pais_origen') }).as('pais_origen'),
      b => b.select('nombre').from('pais').where({ id: ref('id_pais_destino') }).as('pais_destino'),
      b => b.select('nombre').from('provincia').where({ id: ref('id_provincia_origen') }).as('provincia_origen'),
      b => b.select('nombre').from('provincia').where({ id: ref('id_provincia_destino') }).as('provincia_destino'),
    )
    .leftJoin('cliente', 'cliente.id', 'envio.id_cliente')
    .leftJoin('modalidad', 'modalidad.id', 'envio.id_modalidad')

  if (+params.page === +params.page)
    Query.page(params.page, +params.pagesize ? params.pagesize : undefined)

  if (params.filter && params.filter.length > 1){
    Query.having('dni_cliente', 'LIKE', '%'+sanitizeString(params.filter)+'%');
    Query.orHaving('nombre_cliente', 'LIKE', '%'+sanitizeString(params.filter)+'%');
    Query.orHaving('nombre_destinatario', 'LIKE', '%'+sanitizeString(params.filter)+'%');
  }

  Query.where('envio.estatus',1)
  Query.where('envio.id_cliente',req.params.id)
  Query.orderBy('id','DESC')
  await Query.withGraphFetched('bultos')
    .then(envios => AR.enviarDatos(envios, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

// Registrar envio.
router.post('/', upload.none(), async(req, res) => {
  const body = req.body;
  const actualizar_datos_cliente = body.actualizar_datos_cliente;

  const PORCENTAJE_SEGURO = 3; //%
  const PORCENTAJE_REEMBOLSO = 3; //%

  const datos = {
    id_pais_origen: body.pais_origen || body.id_pais_origen,
    id_pais_destino: body.pais_destino || body.id_pais_destino,
    id_provincia_origen: body.provincia_origen || body.id_provincia_origen,
    id_provincia_destino: body.provincia_destino || body.id_provincia_destino,

    id_cliente: body.cliente || body.id_cliente,
    id_modalidad: body.modalidad || body.id_modalidad,

    seguro: body.seguro || body.monto_seguro,
    reembolso: body.reembolso || body.monto_reembolso,

    fecha_recogida: body.fecha_recogida,
    hora_recogida_desde: body.hora_recogida_desde,
    hora_recogida_hasta: body.hora_recogida_hasta,

    nombre_destinatario: body.nombre_destinatario,
    correo_destinatario: body.correo_destinatario,
    telefono_destinatario: body.telefono_destinatario,
    cod_telefono_destinatario: body.cod_telefono_destinatario,

    dni_remitente: body.dni_remitente,
    nombre_remitente: body.nombre_remitente,
    empresa_remitente: body.empresa_remitente,
    nombre_contacto_destinatario: body.nombre_contacto_destinatario,
    correo_remitente: body.correo_remitente,
    telefono_remitente: body.telefono_remitente,
    cod_telefono_remitente: body.cod_telefono_remitente,

    direccion_origen: body.direccion_origen,
    direccion_destino: body.direccion_destino,

    codigo_postal_origen: body.codigo_postal_origen,
    codigo_postal_destino: body.codigo_postal_destino,

    localidad_origen: body.localidad_origen,
    localidad_destino: body.localidad_destino,

    es_documentacion: Boolean(body.es_documentacion),
    es_cliente_empresa: Boolean(body.es_cliente_empresa || body.es_remitente_empresa),
    es_destinatario_empresa: Boolean(body.es_destinatario_empresa),
  };

  const bultos = body.bultos;



  /* PRIMERA VALIDACION: DATOS */

  let err = false;

  if (!datos.id_pais_origen && !datos.id_pais_destino)
    err = 'Falta un país';

  else if (!datos.id_provincia_origen && !datos.id_provincia_destino)
    err = 'Falta al menos una provincia';

  // else if (!datos.id_cliente)
  //   err = 'Falta id del cliente';

  else if (!datos.id_modalidad)
    err = 'Falta la modalidad de envío';

  else if (!datos.direccion_origen || !datos.direccion_destino)
    err = 'Falta al menos una dirección';

  else if (!datos.nombre_destinatario || !datos.correo_destinatario || !datos.telefono_destinatario || !datos.cod_telefono_destinatario)
    err = 'Falta información sobre el destinatario';

  else if (!datos.codigo_postal_origen || !datos.codigo_postal_destino)
    err = 'Falta al menos un código postal';

  else if (!datos.localidad_origen || !datos.localidad_destino)
    err = 'Falta al menos una localidad';

  // else if (!datos.fecha_recogida || !datos.hora_recogida_hasta || !datos.hora_recogida_desde)
  //   err = 'Falta información sobre el tiempo de recogida';

  else if (!datos.fecha_recogida)
    err = 'Falta información sobre el tiempo de recogida';

  else if (!bultos || bultos.length === 0)
    err = 'No se registraron bultos';

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);


  /* SEGUNDA VALIDACION: ENVIO */
  const tipo_envio = await Envio.obtenerTipoDeEnvio(
    datos.id_pais_origen,
    datos.id_pais_destino,
    datos.id_provincia_origen,
    datos.id_provincia_destino
    );

  if (!tipo_envio.zona)
    err = 'No se puede realizar este envío';

  if (!datos.id_cliente && (!datos.telefono_remitente && !datos.dni_remitente && !datos.correo_remitente && !datos.nombre_remitente && !datos.cod_telefono_remitente))
    err = 'Faltan datos del remitente';

  else if (tipo_envio.zona.tipo && (tipo_envio.zona.tipo.toUpperCase().includes('INTERNACIONAL') && bultos.length > 1))
  {
    err = 'Los envíos internacionales solo pueden tener un bulto';
  }

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);


  /* TERCERA VALIDACION: BULTOS */
  for (let bulto of bultos) {
    if (!bulto || !bulto.fondo || !bulto.alto || !bulto.ancho || !bulto.peso) {
      return AR.enviarError('Uno de los bultos no es correcto', res, HTTP_BAD_REQUEST);
    }
  }

  /* FIN VALIDACIONES */

  let cli, fetch_cli=false;
  if (datos.id_cliente) {
    cli = await Cliente.query().findById(datos.id_cliente).catch(console.error);
    if (!cli)
      return AR.enviarDatos('El cliente no existe', res, HTTP_BAD_REQUEST);
  }
  else if (datos.dni_remitente) {
    cli = await Cliente.query().findOne({dni: datos.dni_remitente}).catch(console.error);
  }
  else if (datos.nombre_remitente) {
    cli = await Cliente.query().findOne({nombre: datos.nombre_remitente}).catch(console.error);
  }

  if (cli) {
    datos.id_cliente = cli.id;

    if (actualizar_datos_cliente) {
      cli = await Cliente.query().patchAndFetchById(cli.id, {
        telefono: datos.telefono_remitente,
        cod_telefono: datos.cod_telefono_remitente,
        nombre: datos.nombre_remitente,
        correo: datos.correo_remitente,
        es_empresa: !!datos.es_cliente_empresa,
        empresa: datos.empresa_remitente,
      }).catch(console.error);
    }
  }
  else {
    cli = await Cliente.query()
      .insert({
        telefono: datos.telefono_remitente,
        cod_telefono: datos.cod_telefono_remitente,
        dni: datos.dni_remitente,
        nombre: datos.nombre_remitente,
        correo: datos.correo_remitente,
        estatus: '1',
        es_empresa: datos.es_cliente_empresa,
        empresa: datos.empresa_remitente,
      }).catch(console.error);
    datos.id_cliente = cli.id;
  }

  let modalidad;
  if (datos.id_modalidad) {
    modalidad = await Modalidad.query().findById(datos.id_modalidad).catch(console.error);
  }


  const costo_bultos = costoDeBultos(
    bultos,
    tipo_envio.zona.pesos,
    tipo_envio.zona.tarifa_adicional,
    (cli.estatus == 1) ?
        tipo_envio.zona.coeficiente_no_registrado
        : (cli.estatus==2 ? tipo_envio.zona.coeficiente_registrado : tipo_envio.zona.coeficiente_registrado_lvl_1),
    tipo_envio.params ? tipo_envio.params.franja_adicional : 0
  );

  const subtotal = totalEnvio({
    bultos: costo_bultos,
    modalidad: modalidad ? modalidad.monto : 0,
    seguro: datos.seguro,
    porcentaje_seguro: PORCENTAJE_SEGURO,//%
    reembolso: datos.reembolso,
    porcentaje_reembolso: PORCENTAJE_REEMBOLSO,//%
    dua: tipo_envio.aduana ? tipo_envio.aduana.dua : 0,
    importacion: tipo_envio.aduana ? tipo_envio.aduana.importacion : 0,
    porcentaje_descuento: cli.porcentaje_des || 0,
  })

  const total = subtotal;

  const envio = await Envio.query()
    .insertGraph({
        id_pais_origen: datos.id_pais_origen,
        id_pais_destino: datos.id_pais_destino,
        id_provincia_origen: datos.id_provincia_origen,
        id_provincia_destino: datos.id_provincia_destino,

        id_cliente: datos.id_cliente,
        id_modalidad: datos.id_modalidad,
        id_zona: tipo_envio && tipo_envio.zona ? tipo_envio.zona.id : null,

        // seguro: +datos.seguro || 0,
        // reembolso: +datos.reembolso || 0,

        seguro: +datos.seguro ? Math.max(3, PORCENTAJE_SEGURO/100 * datos.seguro) : 0,
        reembolso: +datos.reembolso ? Math.max(3, PORCENTAJE_REEMBOLSO/100 * datos.reembolso) : 0,

        fecha_recogida: datos.fecha_recogida,
        hora_recogida_desde: '09:00:00',
        hora_recogida_hasta: '20:00:00',

        // hora_recogida_desde: datos.hora_recogida_desde,
        // hora_recogida_hasta: datos.hora_recogida_hasta,

        nombre_destinatario: datos.nombre_destinatario,
        nombre_contacto_destinatario: datos.nombre_contacto_destinatario,
        correo_destinatario: datos.correo_destinatario,
        telefono_destinatario: datos.telefono_destinatario,
        cod_telefono_destinatario: datos.cod_telefono_destinatario,

        direccion_origen: datos.direccion_origen,
        direccion_destino: datos.direccion_destino,

        codigo_postal_origen: datos.codigo_postal_origen,
        codigo_postal_destino: datos.codigo_postal_destino,

        localidad_origen: datos.localidad_origen,
        localidad_destino: datos.localidad_destino,

        es_documentacion: datos.es_documentacion,
        es_cliente_empresa: datos.es_cliente_empresa,
        es_destinatario_empresa: datos.es_destinatario_empresa,
        estatus: 2,
        monto: total,

        'bultos': bultos,
    })
    .catch(console.error);

  envio.cliente = cli;

  AR.enviarDatos(envio, res);
});

router.post('/agregar-transaccion', upload.none(), async (req, res) => {
  let data = req.body;
  data.fecha_registro = moment().format("YYYY-MM-DD HH:mm:ss");
  data.fecha_actualizacion = moment().format("YYYY-MM-DD HH:mm:ss");
  data.response_payment = null;
  // console.log(data);
  await Transacciones.query().insert(data).catch(console.error);
  return AR.enviarDatos(data, res);
});

router.post('/response-transaccion', upload.none(), async (req, res) => {
  let data = req.body;
  console.log("REDSYS WEBHOOCK");
  console.log(data);
  return AR.enviarDatos({msj:true}, res);
});

router.post('/actualizar-transaccion', upload.none(), async (req, res) => {
  let data = req.body;
  AR.enviarDatos(data, res);

  let accion = data.accion;
  delete data["accion"];

    let find = await Transacciones.query().findOne({ code_pago: data.code_pago }).catch(console.error);
    if (find) {
      if (find.proceso == "generar codigo" && accion == 1 && find.estatus != "PAGO VERIFICADO") {
        let codgen = moment().format('YYYYMMDDHHmm')+'-'+ find.envio_id;
        await Envio.query().update({codigo_generado:codgen, estatus:1}).where('id',find.envio_id).catch(err => {console.error(err);});
        // aca en esta parte enviar algun correo o algo asi
        let pago = {
          fecha_pago:  moment().format("YYYY-MM-DD HH:mm"),
          referencia_pago: data.code_pago,
          tipo_pago: find.metodo_pago_text
        };
        await activarEnvio(find.envio_id, pago);
        PagoEnvio.query().insert({id_envio: find.envio_id, tipo_pago: find.metodo_pago_text}).then().catch(console.error);
      }
      data.fecha_actualizacion = moment().format("YYYY-MM-DD HH:mm:ss");
      await Transacciones.query().update(data).where({code_pago: data.code_pago}).catch(console.error);
    }

  return;
});

router.get('/obtener-transaccion', upload.none(), async (req, res) => {

  let data = await Transacciones.query()
  .whereRaw(`code_pago = '${req.query.code_pago}' AND (estatus = 'RECHAZADO/CANCELADO' OR estatus = 'PAGO NO VERIFICADO' OR estatus = 'PAGO SIN FINALIZAR' OR estatus = 'EN PROCESO')`)
  .catch(console.error);
  if (data) {
    if (data[0]) {
      data = data[0];
    } else {
      data = undefined;
    }
  } else {
    data = undefined;
  }

  if (data) {
    data.encontrada = true;
  } else {
    data = { encontrada: false };
  }
  return AR.enviarDatos(data, res);
});

router.get('/obtener-transaccion-proceso/:code_pago', upload.none(), async (req, res) => {

  let data = await Transacciones.query()
  .whereRaw(`code_pago = '${req.params.code_pago}' AND (estatus = 'RECHAZADO/CANCELADO' OR estatus = 'EN PROCESO' OR estatus = 'PAGO VERIFICADO')`)
  .catch(console.error);
  if (data) {
    if (data[0]) {
      data = data[0];
    } else {
      data = undefined;
    }
  } else {
    data = undefined;
  }
  if (data) {
    if (data.estatus === "RECHAZADO/CANCELADO") {
        data.accion_proceso = 1;
    } else if (data.estatus === "EN PROCESO") {
        data.accion_proceso = 2;
    } else if (data.estatus === "PAGO VERIFICADO") {
      data.accion_proceso = 3;
    }
    data.encontrada = true;
  } else {
    data = { encontrada: false };
  }
  return AR.enviarDatos(data, res);
});

// Registrar pago envio
router.post('/pago_bancario', upload.none(), async(req, res) => {
  const id_envio = req.body.idp;
  const refe = req.body.referencia_bancaria;
  const fec = req.body.fecha_bancaria;
  const img = req.body.img_bancaria;
  const tipo = req.body.tipo;
  const sts = req.body.sts;
  if(!req.body.fecha_bancaria)
    fec = moment().format('YYYY-MM-DD');
  /* Verificacion de errores */
  let err = null;

  if (!id_envio) {
    err = 'Especificar el id del envio';
  }
  /*
  else if (!refe) {
    err = 'Especificar la referencia';
  }
  else if (!fec) {
    err = 'Especificar la fecha del pago';
  }
  */

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);

  let codgen = moment().format('YYYYMMDDHHmm')+'-'+id_envio;
  const datos = {
    id_envio: id_envio,
    tipo_pago: tipo,
    fecha_pago: fec,
    referencia_pago: refe,
    estatus_pago: sts
  };
  /*
  if(img){
    var file = "img_pago__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
    await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(img.split(",")[1], 'base64'), 'base64', function(err) {
      console.log(err);
    });
    datos.img_pago = config.rutaArchivo(file);
  }
  */
  await PagoEnvio.query().insert(datos)
  .then(async resp => {
    await Envio.query().update({codigo_generado:codgen, estatus:1}).where('id',id_envio).catch(err => {console.error(err);});
    resp.cod = codgen;
    resp.r = true;
    //resp.fecha_pago = moment(resp.fecha_pago).format('DD/MM/YYYY');
    await activarEnvio(id_envio, resp);
    resp.act_noti = true;
    AR.enviarDatos(resp, res);
  }).catch(err => {
    console.error(err);
    res.sendStatus(HTTP_SERVER_ERROR);
  })
});
async function activarEnvio(id_envio, resp) {
  const DatosC = await Envio.query()
    .select(
      'envio.*',
      'cliente.dni AS dni_cliente',
      'cliente.nombre AS nombre_cliente',
      'cliente.cod_telefono AS cod_telefono_cliente',
      'cliente.telefono AS telefono_cliente',
      'cliente.correo AS correo_cliente',
      'cliente.empresa AS empresa_cliente',
      'modalidad.nombre AS modalidad',
      raw('(SELECT SUM(peso) FROM envio_bulto WHERE id_envio=envio.id)').as('cantidad_peso'),
      raw('(SELECT COUNT(*) FROM envio_bulto WHERE id_envio=envio.id)').as('nr_bultos'),
      b => b.select('nombre').from('pais').where({ id: ref('id_pais_origen') }).as('pais_origen'),
      b => b.select('nombre').from('pais').where({ id: ref('id_pais_destino') }).as('pais_destino'),
      b => b.select('nombre').from('provincia').where({ id: ref('id_provincia_origen') }).as('provincia_origen'),
      b => b.select('nombre').from('provincia').where({ id: ref('id_provincia_destino') }).as('provincia_destino'),
    )
    .leftJoin('cliente', 'cliente.id', 'envio.id_cliente')
    .leftJoin('modalidad', 'modalidad.id', 'envio.id_modalidad')
    .where('envio.id',id_envio);
    // console.log(resp.fecha_pago);
    let fec_sola = moment(resp.fecha_pago).format('YYYY-MM-DD');
    DatosC[0].hora_recogida_desde=moment(fec_sola+' '+DatosC[0].hora_recogida_desde).format('HH');
    DatosC[0].hora_recogida_hasta=moment(fec_sola+' '+DatosC[0].hora_recogida_hasta).format('HH');
    DatosC[0].fecha_recogida = moment(DatosC[0].fecha_recogida).format('DD/MM/YYYY');
    resp.fecha_pago = moment(resp.fecha_pago).format('DD/MM/YYYY');
    enviarResumen(DatosC[0].correo_cliente, DatosC[0].nombre_cliente, DatosC[0], resp)
    .then(re => {
      console.error(re);
    })
    .catch(err => {
      console.error(err);
    });
    const ADMIN = await Admin.query();
    for (var i = 0; i < ADMIN.length; i++) {
      await Notificaciones.query()
        .insert({
          titulo: 'Nuevo paquete',
          contenido: 'Se ha enviado un nuevo paquete',
          fecha: new Date(),
          estatus: 0,
          tipo_noti: 2,
          url: 'Envios',
          id_usable: id_envio,
          id_receptor: ADMIN[i].id,
          id_emisor: DatosC[0].id_cliente,
          tabla_usuario_r: 'admin',
          tabla_usuario_e: 'cliente'
        }).catch(async(err)=>{console.log(err);});
    }
    resp.act_noti = true;
    IO.sockets.emit("RecibirNotificacionPanel", {resp});
    return true;
}
// Determinar el tipo de envio
router.post('/obtener-tipo', upload.none(), async(req, res) => {
  const id_pais1 = req.body.pais1;
  const id_provincia1 = req.body.provincia1;

  const id_pais2 = req.body.pais2;
  const id_provincia2 = req.body.provincia2;


  /* Verificacion de errores */
  let err = null;

  if (!isNum(id_pais1)) {
    err = 'Especificar pais de procedencia';
  }
  else if (!isNum(id_pais2)) {
    err = 'Especificar pais de destino';
  }
  else if (id_pais1 == PA.ESPAÑA && !isNum(id_provincia1)) {
    err = 'Especificar provincia de procedencia';
  }
  else if (id_pais2 == PA.ESPAÑA && !isNum(id_provincia2)) {
    err = 'Especificar provincia de destino';
  }

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);



  const datos = await Envio.obtenerTipoDeEnvio(id_pais1, id_pais2, id_provincia1, id_provincia2);

  AR.enviarDatos(datos, res);
});


// Enviar correo de seguimiento al cliente.
router.post('/seguimiento', upload.none(), async(req, res) => {
  const url = req.body.url;
  const observacion = req.body.observacion;
  const id_cliente = req.body.cliente || req.body.id_cliente;
  const id_envio = req.body.envio || req.body.id_envio;

  let err = false;
  if (!observacion)
    err = 'Ingrese la observación';
  else if (!url || !checkUrl(url))
    err = 'Ingrese una URL válida';
  else if (!id_cliente)
    err = 'Cliente no especificado';
  else if (!id_envio)
    err = 'Envío no especificado';

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);

  const cli = await Cliente.query()
    .findById(id_cliente)
    .select('cliente.*', 'pais.nombre AS pais', 'provincia.nombre AS provincia')
    .leftJoin('pais', 'pais.id', 'cliente.id_pais')
    .leftJoin('provincia', 'provincia.id', 'cliente.id_provincia')
    .catch(console.error);
  if (!cli) {
    return AR.enviarError('Ocurrió un error', res, HTTP_BAD_REQUEST);
  }

  const envio = await Envio.query()
    .findById(id_envio)
    .patchAndFetchById(id_envio, {
      url_seguimiento: url,
      observacion_seguimiento: observacion,
    })
    .catch(console.log);

  const datos = {
    ...cli,
    nombre: cli.nombre,
    correo: cli.correo,
    observacion: observacion,
    url: url,
    codigo_generado: envio.codigo_generado,
    localidad_origen: envio.localidad_origen,
  };

  // enviarSeguimiento(cli.nombre, cli.correo, observacion, url)
  enviarSeguimiento(datos)
    .then(() => res.sendStatus(HTTP_OK))
    .catch(err => {
      console.error(err);

      Envio.query()
        .findById(id_envio)
        .patchAndFetchById(id_envio, {
          url_seguimiento: null,
          observacion_seguimiento: null,
        })
        .then().catch(console.log);

      AR.enviarError('Ocurrió un error, intente de nuevo', res, HTTP_SERVER_ERROR);
    });
});

router.get('/:id(\\d+)', /* validar('admin'), */ async(req, res) => {
  const params = req.query;

  const Query = Envio.query()
    .select(
      'envio.*',
      'cliente.dni AS dni_cliente',
      'cliente.nombre AS nombre_cliente',
      'cliente.cod_telefono AS cod_telefono_cliente',
      'cliente.telefono AS telefono_cliente',
      'cliente.correo AS correo_cliente',
      'modalidad.nombre AS modalidad',
      'pago_envio.tipo_pago',
      'transacciones.metodo_pago_text',
      'transacciones.estatus AS estatus_transaccion',
      'transacciones.code_pago AS code_pago',
      b => b.select('nombre').from('pais').where({ id: ref('id_pais_origen') }).as('pais_origen'),
      b => b.select('nombre').from('pais').where({ id: ref('id_pais_destino') }).as('pais_destino'),
      b => b.select('nombre').from('provincia').where({ id: ref('id_provincia_origen') }).as('provincia_origen'),
      b => b.select('nombre').from('provincia').where({ id: ref('id_provincia_destino') }).as('provincia_destino'),
    )
    .leftJoin('cliente', 'cliente.id', 'envio.id_cliente')
    .leftJoin('modalidad', 'modalidad.id', 'envio.id_modalidad')
    .leftJoin('pago_envio', 'pago_envio.id_envio', 'envio.id')
    .leftJoin('transacciones', 'transacciones.envio_id', 'envio.id')


  Query.where(b => b.where('transacciones.estatus', '<>', 'RECHAZADO/CANCELADO').orWhereNull('transacciones.estatus'));
  Query.where('envio.estatus',1)
  Query.where('envio.id',req.params.id)

  await Query.withGraphFetched('bultos')
    .then(envios => AR.enviarDatos(envios, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

router.get('/metodos_pago', async(req, res) => {
  const tipos = await PagoEnvio.query()
    .select('tipo_pago')
    .groupBy('tipo_pago')
    .catch(console.log);



  AR.enviarDatos(tipos.map(r => r.tipo_pago), res);
});

module.exports = router;
