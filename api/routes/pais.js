const Pais = require('../models/Pais');

const validar = require('./sesion2');
const sanitizeString = require('../functions/sanitizeString');
const AR = require('../ApiResponser');
const upload = require('multer')();
const router = require('express').Router();

const
  HTTP_OK = 200,
  HTTP_BAD_REQUEST = 400,
  HTTP_NOT_FOUND = 404,
  HTTP_UNAUTHORIZED = 401,
  HTTP_SERVER_ERROR = 500

// Listar paises.
router.get('/', /*validar('admin'),*/ async(req, res) => {
  const params = req.query;

  const Query = Pais.query()
    .select('pais.*', 'zona.envio')
    .leftJoin('zona', 'zona.id', 'pais.id_zona');


  if (params.envio) {
    params.envio = params.envio.toLowerCase();

    if (params.envio == 'envio' || params.envio == 'envío' || params.envio == 'e')
      Query.whereRaw('zona.envio IN (?) OR zona.envio IS NULL', [['Envío', 'Ambos']]);
      // Query.havingIn('envio', ['Envío', 'Ambos', null]);

    if (params.envio == 'destino' || params.envio == 'd')
      Query.whereRaw('zona.envio IN (?) OR zona.envio IS NULL', [['Destino', 'Ambos']]);
      // Query.havingIn('envio', ['Destino', 'Ambos']);

    if (params.envio == 'ambos' || params.envio == 'a')
      Query.whereRaw('zona.envio IN (?) OR zona.envio IS NULL', [['Destino', 'Ambos', 'Envío']]);
      // Query.havingIn('envio', ['Envío', 'Ambos', 'Destino']);
  }

  if (+params.page === +params.page)
    Query.page(params.page, +params.pagesize ? params.pagesize : undefined)

  if (params.filter && params.filter.length > 1){
    Query.having('nombre', 'LIKE', '%'+sanitizeString(params.filter)+'%');
  }
  Query.orderBy('nombre');
  await Query
    .then(paises => AR.enviarDatos(paises, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Recuperar pais sin zona.
router.get('/sin_zona', validar('admin'), async(req, res) => {
  await Pais.query().where('id_zona',null).where('region',null).orderBy('nombre')
    .then(pais => {
      if (!pais)
        return res.sendStatus(HTTP_NOT_FOUND);
      AR.enviarDatos(pais, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Recuperar pais.
router.get('/:id(\\d+)',/* validar('admin'),*/ async(req, res) => {
  await Pais.query().findById(req.params.id)
    .then(pais => {
      if (!pais)
        return res.sendStatus(HTTP_NOT_FOUND);
      AR.enviarDatos(pais, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Crear pais.
router.post('/', upload.none(), validar('admin'), async(req, res) => {
  const datos = {
    nombre: req.body.nombre,
    region: req.body.region,
    id_zona: req.body.zona || req.body.id_zona,
    extracomunitario: req.body.extracomunitario,
    codigo_tlf: req.body.cod,
    importacion: req.body.importacion
  };

  let err;
  if (!datos.nombre)
    err = 'Ingrese el nombre del pais';
  else if (!datos.codigo_tlf)
    err = 'Ingrese el Código del país';

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);

  await Pais.query()
    .insert(datos)
    .then(pais => AR.enviarDatos(pais, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

// Editar pais.
router.put('/:id(\\d+)', upload.none(), validar('admin'), async(req, res) => {
  const datos = {
    nombre: req.body.nombre,
    region: req.body.region,
    id_zona: req.body.zona || req.body.id_zona,
    extracomunitario: req.body.extracomunitario,
    codigo_tlf: req.body.cod,
    importacion: req.body.importacion
  };

  await Pais.query()
    .patchAndFetchById(req.params.id, datos)
    .then(pais => {
      if (!pais)
        return res.sendStatus(HTTP_NOT_FOUND);
      AR.enviarDatos(pais, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Eliminar pais.
router.delete('/:id(\\d+)', validar('admin'), async(req, res) => {
  await Pais.query()
    .findById(req.params.id)
    .delete()
    .then(() => res.sendStatus(HTTP_OK))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});


router.get('/esp', async(req, res) => {
  await Pais.query()
    .where('nombre', 'LIKE', '%espa%')
    .first()
    .then(e => AR.enviarDatos(e || {r:false}, res))
    .catch(err => { console.error(err); res.sendStatus(HTTP_SERVER_ERROR) });
});



module.exports = router;
