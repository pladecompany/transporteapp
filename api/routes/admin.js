const Admin = require('../models/Admin');
const Cliente = require('../models/Cliente');
const Envio = require('../models/Envio');

const validar = require('./sesion2');
const AR = require('../ApiResponser');
const hashPass = require('../functions/hashPass');
const sanitizeString = require('../functions/sanitizeString');
const upload = require('multer')();
const router = require('express').Router();

const
  HTTP_OK = 200,
  HTTP_BAD_REQUEST = 400,
  HTTP_NOT_FOUND = 404,
  HTTP_UNAUTHORIZED = 401,
  HTTP_SERVER_ERROR = 500

// Listar admins.
router.get('/', validar('admin'), async(req, res) => {
  const params = req.query;

  const Query = Admin.query();

  if (+params.page === +params.page)
    Query.page(params.page, +params.pagesize ? params.pagesize : undefined)

  if (params.filter && params.filter.length > 1){
    Query.having('nombre', 'LIKE', '%'+sanitizeString(params.filter)+'%');
    Query.orHaving('correo', 'LIKE', '%'+sanitizeString(params.filter)+'%');
  }

  await Query
    .then(admins => AR.enviarDatos(admins, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Crear admin.
router.post('/', upload.none(), validar('admin'), async(req, res) => {
  const body = req.body;
  const datos = {
    nombre: body.nombre,
    correo: body.correo || body.email,
    pass: body.pass || body.password || body.clave,
  }

  let err;
  if (!datos.nombre)
    err = 'Ingrese el nombre';
  else if (!datos.correo)
    err = 'Ingrese el correo';
  else if (!datos.pass)
    err = 'Ingrese una contraseña';
  else if (datos.pass.length < 8)
    err = 'La contraseña debe tener un mínimo de 8 caracteres';

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);


  const existe = await Admin.query().findOne({correo: datos.correo}).catch(console.error);
  if (existe)
    return AR.enviarError('El correo ya existe', res, HTTP_BAD_REQUEST);


  datos.pass = await hashPass(datos.pass);
  await Admin.query()
    .insert(datos)
    .then(admin => {
      if (admin && admin.pass)
        delete admin.pass;
      AR.enviarDatos(admin, res)
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR)
    });
});

// Recuperar admin.
router.get('/:id(\\d+)', validar('admin'), async(req, res) => {
  const id_admin = req.params.id;

  await Admin.query()
    .findById(id_admin)
    .then(admin => {
      if (!admin) return res.sendStatus(HTTP_NOT_FOUND);

      delete admin.pass;
      AR.enviarDatos(admin, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

// Editar admin.
router.put('/:id(\\d+)', upload.none(), validar('admin'), async(req, res) => {
  const id_admin = req.params.id;
  const body = req.body;

  const datos = {};
  const pass = body.pass || body.password || body.clave;

  if (body.nombre)
    datos.nombre = body.nombre;

  if (body.correo || body.email)
    datos.correo = body.correo || body.email;

  if (pass && pass.length >= 8) {
    datos.pass = await hashPass(pass);
  }
  else if (pass && pass.length < 8) {
    return AR.enviarError('La contraseña debe tener un mínimo de 8 caracteres', res, HTTP_BAD_REQUEST);
  }

  if (datos.correo) {
    const existe = await Admin.query()
        .findOne({ correo: datos.correo })
        .where('id', '<>', id_admin)
        .catch(console.error);

    if (existe)
      return AR.enviarError('El correo ya existe', res, HTTP_BAD_REQUEST);
  }

  await Admin.query()
    .patchAndFetchById(id_admin, datos)
    .then(admin => {
      if (!admin) return res.sendStatus(404);

      delete admin.pass;
      delete admin.token;
      AR.enviarDatos(admin, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

// Eliminar admin.
router.delete('/:id(\\d+)', validar('admin'), async(req, res) => {
  await Admin.query()
    .findById(req.params.id)
    .delete()
    .then(() => res.sendStatus(HTTP_OK))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});


router.get('/contadores', validar('admin'), async(req, res) => {
  const admin = await Admin.query().count('* AS conteo').first().catch(console.error);
  const cliente = await Cliente.query().count('* AS conteo').where('estatus','<>',1).first().catch(console.error);
  const envio = await Envio.query().where('estatus',1).count('* AS conteo').first().catch(console.error);

  const conteos = {
    conteo_admin: admin ? admin.conteo : null,
    conteo_cliente: cliente ? cliente.conteo : null,
    conteo_envio: envio ? envio.conteo : null,
  };

  AR.enviarDatos(conteos, res);
});


module.exports = router;
