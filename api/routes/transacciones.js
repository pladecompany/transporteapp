const Model = require('../models/Transacciones');


const validar = require('./sesion2');
const sanitizeString = require('../functions/sanitizeString');
const AR = require('../ApiResponser');
const Transacciones = require('../models/Transacciones');
const upload = require('multer')();
const router = require('express').Router();

const
  HTTP_OK = 200,
  HTTP_BAD_REQUEST = 400,
  HTTP_NOT_FOUND = 404,
  HTTP_UNAUTHORIZED = 401,
  HTTP_SERVER_ERROR = 500


router.get('/', /*validar('admin'),*/ async(req, res) => {
    const params = req.query;
  
    const Query = Model.query()
      .select('transacciones.*', 'envio.codigo_generado', 'cliente.dni', 'cliente.nombre', Model.raw(`CONCAT(cliente.cod_telefono, " ", cliente.telefono) as telefono`))
      .leftJoin('envio', 'envio.id', 'transacciones.envio_id')
      .leftJoin('cliente', 'cliente.correo', 'transacciones.email');
  
    if (+params.page === +params.page)
      Query.page(params.page, +params.pagesize ? params.pagesize : undefined)
  
    if (params.filter && params.filter.length > 1){
      Query.having('email', 'LIKE', '%'+sanitizeString(params.filter)+'%');
      Query.orHaving('code_pago', 'LIKE', '%'+sanitizeString(params.filter)+'%');
      Query.orHaving('metodo_pago_text', 'LIKE', '%'+sanitizeString(params.filter)+'%');
      Query.orHaving('estatus', 'LIKE', '%'+sanitizeString(params.filter)+'%');
      Query.orHaving('importe', 'LIKE', '%'+sanitizeString(params.filter)+'%');
      Query.orHaving('fecha_actualizacion', 'LIKE', '%'+sanitizeString(params.filter)+'%');
      Query.orHaving('codigo_generado', 'LIKE', '%'+sanitizeString(params.filter)+'%');
      Query.orHaving('dni', 'LIKE', '%'+sanitizeString(params.filter)+'%');
      Query.orHaving('nombre', 'LIKE', '%'+sanitizeString(params.filter)+'%');
      Query.orHaving('telefono', 'LIKE', '%'+sanitizeString(params.filter)+'%');
    }
    Query.orderBy('fecha_actualizacion', "DESC");
    await Query
      .then(transacciones => AR.enviarDatos(transacciones, res))
      .catch(err => {
        console.error(err);
        res.sendStatus(HTTP_SERVER_ERROR);
      })
  });

module.exports = router;
