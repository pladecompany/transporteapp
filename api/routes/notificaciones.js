const Notificaciones = require("../models/Notificacion");
//const Firebase = require("../models/Firebase");
//var FirebaseB = require("../routes/firebase");
//FirebaseB = new FirebaseB();

//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const sanitizeString = require('../functions/sanitizeString');

//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

const
  HTTP_OK = 200,
  HTTP_BAD_REQUEST = 400,
  HTTP_NOT_FOUND = 404,
  HTTP_UNAUTHORIZED = 401,
  HTTP_SERVER_ERROR = 500

var router = express.Router();

router.get('/',upload.none(),async (req, res, next) => {
    var data = req.query;

    const notificaciones = await Notificaciones.query()
    .where("id_receptor",data.id)
    .where("estatus",0)
    .where("tabla_usuario_r",data.tipo_usuario)
    .orderBy('fecha','DESC')
    .limit(10)
    .catch(err => {console.log(err);});

    const notificaciones2 = await Notificaciones.query()
    .where("id_receptor",data.id)
    .where("estatus",0)
    .where("tabla_usuario_r",data.tipo_usuario)
    .catch(err => {console.log(err);});
    //var notificaciones = await Notificaciones.raw('select *,(SELECT CONCAT("#",a.id," ",a.nombre) FROM cliente a WHERE a.id=id_emisor and tabla_usuario_e="cliente") as nombre_c from notificacion where id_receptor='+data.id+' and tabla_usuario_r="'+data.tipo_usuario+'" and estatus=0 '+string+';')

    //var notificaciones2 = await Notificaciones.raw('select count(*) as count from notificacion where id_receptor='+data.id+' and tabla_usuario_r="'+data.tipo_usuario+'" and  estatus=0;')
    AR.enviarDatos({noti : notificaciones || [], count: notificaciones2.length || 0}, res);
});

router.get('/tabla/:tipo/:id', async(req, res) => {
  const params = req.query;

  const Query = Notificaciones.query();

  if (+params.page === +params.page)
    Query.page(params.page, +params.pagesize ? params.pagesize : undefined)

  if (params.filter && params.filter.length > 1){
    Query.having('titulo', 'LIKE', '%'+sanitizeString(params.filter)+'%');
    Query.orHaving('contenido', 'LIKE', '%'+sanitizeString(params.filter)+'%');
  }

  Query.where('estatus',0);
  Query.where('id_receptor',req.params.id);
  Query.where('tabla_usuario_r',req.params.tipo);
  Query.orderBy('fecha','DESC');
  await Query
    .then(Zonas => AR.enviarDatos(Zonas, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.get('/vistaall',upload.none(),async (req, res, next) => {
    var data = req.query;
    await Notificaciones.query()
    .update({ estatus:1})
    .where("id_receptor",data.id)
    .where("tabla_usuario_r",data.tipo_usuario)
    .catch(err => {
    console.log(err);
        res.send(err);
    });
    datos = {};
    datos.r = true;
    AR.enviarDatos(datos, res);
});

router.post('/vista/:id',upload.none(),async (req, res, next) => {
    var datos = {
        estatus:1
    }
    await Notificaciones.query()
    .updateAndFetchById(req.params.id, datos)
    .then(notificacion => {
        notificacion.r=true;
        notificacion.msj = "Se editó correctamente";
        AR.enviarDatos(notificacion, res);
    })
    .catch(err => {
    console.log(err);
        res.send(err);
    });
});

router.post('/count',upload.none(),async (req, res, next) => {
    data = req.body;
    var notificaciones = await Notificaciones.raw('select count(*) as count from notificacion where id_receptor='+data.id+' and tabla_usuario_r="'+data.tipo_usuario+'" and  estatus=0;')

    AR.enviarDatos(notificaciones[0], res);
});

/*router.post('/agregar-eliminar-firebase', upload.none(), async (req, res) => {
    console.log(req.body);
    var obj = {
        'id_usuario' : req.body.id_usuario,
        'token' : req.body.tokenFirebase,
        'tablausuario' : req.body.tablausuario,
        'device': req.body.device ? req.body.device : 2
        };
        await Firebase.query().delete()
        .where({token: obj.token})
        .catch(err => {
            console.log(err);
            res.send(err);
        });
        Firebase.query()
        .insert(obj)
        .then(fire => {
            AR.enviarDatos(fire, res);
        })
        .catch(async err => {

                        Firebase.query()
                        .patchAndFetchById(
                            obj.token,{
                                id_usuario: obj.id_usuario
                            })
                        .then(firebase => {
                            firebase.msj = "Se edito correctamente!";
                            AR.enviarDatos(firebase, res);
                        })
                        .catch(err => {
                            res.send(err);
                        });
        });
}); */



module.exports = router;
