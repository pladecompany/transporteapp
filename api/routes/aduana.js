const Aduana = require('../models/Aduana');

const validar = require('./sesion2');
const sanitizeString = require('../functions/sanitizeString');
const AR = require('../ApiResponser');
const upload = require('multer')();
const router = require('express').Router();

const
  HTTP_OK = 200,
  HTTP_BAD_REQUEST = 400,
  HTTP_NOT_FOUND = 404,
  HTTP_UNAUTHORIZED = 401,
  HTTP_SERVER_ERROR = 500

// Listar Aduanas.
router.get('/', validar('admin'), async(req, res) => {
  const params = req.query;

  const Query = Aduana.query().join('zona','zona.id','aduana.id_zona');

  if (+params.page === +params.page)
    Query.page(params.page, +params.pagesize ? params.pagesize : undefined)

  if (params.filter && params.filter.length > 1){
    Query.having('zona.nombre', 'LIKE', '%'+sanitizeString(params.filter)+'%');
  }
  Query.select('aduana.*','zona.nombre')
  await Query
    .then(Aduanas => AR.enviarDatos(Aduanas, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Recuperar Aduana.
router.get('/:id(\\d+)', validar('admin'), async(req, res) => {
  await Aduana.query().join('zona','zona.id','aduana.id_zona').findById(req.params.id)
    .then(Aduana => {
      if (!Aduana)
        return res.sendStatus(HTTP_NOT_FOUND);
      AR.enviarDatos(Aduana, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Editar Aduana.
router.put('/:id(\\d+)', upload.none(), validar('admin'), async(req, res) => {
  const datos = {
    importacion: req.body.imp,
    dua: req.body.dua,
    dua_exportacion: req.body.exp,
  };

  await Aduana.query()
    .patchAndFetchById(req.params.id, datos)
    .then(async Aduana => {
      if (!Aduana)
        return res.sendStatus(HTTP_NOT_FOUND);
      AR.enviarDatos(Aduana, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});


module.exports = router;
