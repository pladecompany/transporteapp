const { raw, ref } = require('objection');

const Envio = require('../models/Envio');
const validar = require('./sesion2');
const sanitizeString = require('../functions/sanitizeString');
const precio = require('../functions/precio');
const AR = require('../ApiResponser');

const Excel = require('excel4node');
const csvStringify = require('csv-stringify');
// const PDFMake = require('pdfmake');
const ejs = require('ejs');
const fs = require('fs');
const moment = require('moment');

const upload = require('multer')();
const router = require('express').Router();


const
  HTTP_OK = 200,
  HTTP_NO_CONTENT = 204,
  HTTP_BAD_REQUEST = 400,
  HTTP_NOT_FOUND = 404,
  HTTP_UNAUTHORIZED = 401,
  HTTP_SERVER_ERROR = 500

router.post('/en-excel', upload.none(), validar('admin'), async(req, res) => {
  const Libro = new Excel.Workbook();
  const Hoja = Libro.addWorksheet('Envíos');

  const opciones = {
    desde: req.body.desde,
    hasta: req.body.hasta,
    id_zona: req.body.id_zona,
    id_cliente: req.body.id_cliente,
    metodo: req.body.metodo,
  };

  const envios = await recuperarReporte(opciones);
  // const envios = await recuperarReporte(req.body.desde, req.body.hasta, req.body.id_zona, req.body.id_cliente);
  if (envios && envios.length == 0)
    return res.sendStatus(HTTP_NO_CONTENT);

  const titulos = Object.keys(envios[0]);

  const estiloTitulos = Libro.createStyle({
    font: { bold:true },
    alignment: { horizontal:'center' },
  });
  const estiloDatos = Libro.createStyle({
    alignment: { horizontal:'center' },
  });

  // Titulos.
  for (let i = 0; i < titulos.length; i++)
  {
    Hoja.column(i+1).setWidth(titulos[i].length + 3);

    Hoja.cell(1, i+1)
      .style(estiloTitulos)
      .string(titulos[i]);
  }

  // Datos.
  for (let i = 0; i < envios.length; i++)
  {
    let columnas = Object.values(envios[i]);
    for (let j = 0; j < columnas.length; j++)
    {
      let dataType = typeof (columnas[j]);
      if (dataType == 'object')
      {
        if (columnas[j] instanceof Date)
          dataType = 'date';

        if (columnas[j]===null || columnas[j]===undefined) {
          dataType = 'string';
          columnas[j] = '';
        }
      }

      (Hoja.cell(i+2, j+1)) [dataType](columnas[j]).style(estiloDatos);
    }
  }

  Libro.writeToBuffer().then(buffer => {
    res.setHeader('Content-Length', buffer.byteLength);
    res.setHeader('Content-Type', 'application/vnd.ms-excel');
    res.end(buffer);
  });
});


router.post('/en-csv', upload.none(), validar('admin'), async(req, res) => {
  const { desde, hasta, id_zona, id_cliente, metodo } = req.body;

  const envios = await recuperarReporte({desde, hasta, id_zona, id_cliente, metodo});
  // const envios = await recuperarReporte(desde, hasta, id_zona, id_cliente);
  if (envios && envios.length == 0)
    return res.sendStatus(HTTP_NO_CONTENT);


  csvStringify(envios, {header:true}, (err,data) => {
    if (err) {
      console.log(err);
      return res.sendStatus(HTTP_SERVER_ERROR);
    }

    res.setHeader('Content-Length', Buffer.byteLength(data));
    res.setHeader('Content-Type', 'text/csv; charset=utf-8');
    res.end(data);
  });
});

router.post('/en-html', upload.none(), validar('admin'), async(req, res) => {

  const opciones = {
    desde: req.body.desde,
    hasta: req.body.hasta,
    id_zona: req.body.id_zona,
    id_cliente: req.body.id_cliente,
    metodo: req.body.metodo,
    raw_data: true,
  };

  const envios = await recuperarReporte(opciones);
  // const envios = await recuperarReporte(req.body.desde, req.body.hasta, req.body.id_zona, req.body.id_cliente, true);
  if (envios && envios.length == 0)
    return res.sendStatus(HTTP_NO_CONTENT);

  const data = {
    envios,
    precio,
    fecha: datetime => moment(datetime).format('DD-MM-YYYY'),
  };

  const html = await ejs.renderFile(__dirname + '/../static/Reporte.html', data);

  res.setHeader('Content-Length', Buffer.byteLength(html));
  res.setHeader('Content-Type', 'text/html');
  res.end(html);
});

/*
router.post('/en-pdf', upload.none(), validar('admin'), async(req, res) => {
  const envios = await recuperarReporte(req.body.desde, req.body.hasta, req.body.id_zona, req.body.id_cliente);
  if (envios && envios.length == 0)
    return res.sendStatus(HTTP_NO_CONTENT);

  // const numTitulos = Object.keys(envios[0]).length;
  const titulos = Object.keys(envios[0]);
  const data = envios.map(Object.values);

  const docDefinition = {
    pageSize: 'LETTER',
    pageOrientation: 'landscape',

    content: [
      {
        table: {
          headerRows: 1,
          widths: '*'.repeat(titulos.length).split(''),
          // widths: [ '*', '*', '*', '*' ],

          body: [titulos, data],
          // body: [...titulos, ...data],
        }
      }
    ],
  };


  const printer = new PDFMake({
    Roboto: { normal:__dirname + '/../static/fonts/Roboto-Regular.ttf' }
  });

  const pdfDoc = printer.createPdfKitDocument(docDefinition);


  // res.setHeader('Content-Length', buffer.byteLength);
  res.setHeader('Content-Type', 'application/pdf');

  pdfDoc.pipe(res);
  pdfDoc.end();
});
*/

// async function recuperarReporte(desde, hasta, id_zona, id_cliente, raw_data=false) {
async function recuperarReporte(opciones) {
  const {desde, hasta, id_zona, id_cliente, metodo, raw_data}  = opciones;

  const Query = Envio.query()
    .select(
      'envio.codigo_generado AS código',
      // 'cliente.nombre AS cliente',
      raw('IFNULL(cliente.nombre,"")').as('cliente'),
      raw('IFNULL(cliente.dni,"")').as('cliente_dni'),
      raw('IFNULL(cliente.correo,"")').as('cliente_correo'),
      raw('IFNULL(cliente.cod_telefono,"")').as('cliente_cod_telefono'),
      raw('IFNULL(cliente.telefono,"")').as('cliente_telefono'),
      b => b.select('nombre').from('pais').where({ id: ref('envio.id_pais_origen') }).as('país_de_origen'),
      b => b.select('nombre').from('provincia').where({ id: ref('envio.id_provincia_origen') }).as('provincia_de_origen'),
      b => b.select('nombre').from('pais').where({ id: ref('envio.id_pais_destino') }).as('país_de_destino'),
      b => b.select('nombre').from('provincia').where({ id: ref('envio.id_provincia_destino') }).as('provincia_de_destino'),
      'envio.fecha_envio AS fecha_de_envío',
      'envio.direccion_origen AS dirección_de_orígen',
      'envio.direccion_destino AS dirección_de_destino',
      'envio.localidad_origen AS localidad_de_origen',
      'envio.localidad_destino AS localidad_de_destino',
      'envio.codigo_postal_origen AS código_postal_orígen',
      'envio.codigo_postal_destino AS código_postal_destino',
      'envio.seguro',
      'envio.reembolso',
      'envio.monto',
      'envio.correo_destinatario',
      'envio.fecha_recogida',
      'envio.codigo_generado',
      'envio.cod_telefono_destinatario',
      'envio.telefono_destinatario',
      'pago_envio.tipo_pago',
      raw('IF(envio.es_documentacion,"SI","NO")').as('documentación'),
      b => b.count().from('envio_bulto').where('id_envio', ref('envio.id')).as('bultos'),
      b => b.select('nombre').from('modalidad').where({ id: ref('envio.id_modalidad') }).as('modalidad'),
    )
    .leftJoin('cliente', 'cliente.id', 'envio.id_cliente')
    .leftJoin('pago_envio', 'pago_envio.id_envio', 'envio.id')

  if (desde)
    Query.where('fecha_envio', '>=', desde + ' 00:00:00');

  if (hasta)
    Query.where('fecha_envio', '<=', hasta + ' 23:59:59');

  if (id_zona)
    Query.where('id_zona', id_zona);

  if (id_cliente)
    Query.where('id_cliente', id_cliente);

  if (metodo)
    Query.where('pago_envio.tipo_pago', metodo);


  Query.where('envio.estatus',1) // Envios completados.
  const envios = await Query.catch(console.error);

  if (!envios && envios.length == 0 || raw_data)
    return envios;

  // Limpiar titulos (poner en mayusculas y quitar '_')
  envios.forEach((fila, index, envios) => {
    for (let key of Object.keys(fila)) {
      fila[key.toUpperCase().split('_').join(' ')] = fila[key];
      delete fila[key];
    }
  });

  return envios;
}


module.exports = router;
