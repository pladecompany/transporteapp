const Provincia = require('../models/Provincia');

const validar = require('./sesion2');
const AR = require('../ApiResponser');
const sanitizeString = require('../functions/sanitizeString');
const upload = require('multer')();
const router = require('express').Router();

const
  HTTP_OK = 200,
  HTTP_BAD_REQUEST = 400,
  HTTP_NOT_FOUND = 404,
  HTTP_UNAUTHORIZED = 401,
  HTTP_SERVER_ERROR = 500



// Listar provincias.
router.get('/', /*validar('admin'),*/ async(req, res) => {
  const params = req.query;

  const Query = Provincia.query()
    .select(
      'provincia.*',
      'pais.id AS id_pais',
      'pais.nombre AS pais',
      'region.nombre AS region'
      )
    .join('region', 'region.id', 'provincia.id_region')
    .join('pais', 'pais.id', 'region.id_pais');

  if (+params.page === +params.page)
    Query.page(params.page, +params.pagesize ? params.pagesize : undefined)

  if (params.filter && params.filter.length > 1){
    Query.having('nombre', 'LIKE', '%'+sanitizeString(params.filter)+'%');
    Query.orHaving('pais', 'LIKE', '%'+sanitizeString(params.filter)+'%');
    Query.orHaving('region', 'LIKE', '%'+sanitizeString(params.filter)+'%');
  }
  Query.orderBy('provincia.nombre');
  await Query
    .then(data => AR.enviarDatos(data, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.get('/por-region/:id(\\d+)', async(req, res) => {
  await Provincia.query()
    .select('provincia.*')
    .where('id_region', req.params.id)
    .then(data => AR.enviarDatos(data, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

router.get('/por-pais/:id(\\d+)', async(req, res) => {
  const params = req.query;

  const Query = Provincia.query()
    .select('provincia.*')
    .join('region', 'region.id', 'provincia.id_region')
    .join('pais', 'pais.id', 'region.id_pais')
    .leftJoin('zona', 'zona.id', 'region.id_zona')
    .where('pais.id', req.params.id)
    .orderBy('provincia.nombre')


  /* Filtrar por zona de envio o destino, si tiene */
  if (params.envio) {
    params.envio = params.envio.toLowerCase();

    if (params.envio == 'envio' || params.envio == 'envío' || params.envio == 'e')
      Query.whereRaw('zona.envio IN (?) OR zona.envio IS NULL', [['Envío', 'Ambos']]);

    if (params.envio == 'destino' || params.envio == 'd')
      Query.whereRaw('zona.envio IN (?) OR zona.envio IS NULL', [['Destino', 'Ambos']]);

    if (params.envio == 'ambos' || params.envio == 'a')
      Query.whereRaw('zona.envio IN (?) OR zona.envio IS NULL', [['Destino', 'Ambos', 'Envío']]);
  }


    await Query.then(data => AR.enviarDatos(data, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

router.get('/todas', validar('admin'), async(req, res) => {
  const params = req.query;

  const Query = Provincia.query()
    .join('region', 'region.id', 'provincia.id_region')
    .join('pais', 'pais.id', 'region.id_pais')
    .where('pais.id',1)
    .orderBy('nombre');

  await Query
    .then(data => AR.enviarDatos(data, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.get('/todas-cliente', /*validar('cliente'),*/ async(req, res) => {
  const params = req.query;

  const Query = Provincia.query()
    .join('region', 'region.id', 'provincia.id_region')
    .join('pais', 'pais.id', 'region.id_pais')
    .where('pais.id',1)
    .orderBy('nombre');

  await Query
    .then(data => AR.enviarDatos(data, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Recuperar provincia.
router.get('/:id(\\d+)', /*validar('admin'),*/ async(req, res) => {
  await Provincia.query().findById(req.params.id)
    .then(provincia => {
      if (!provincia)
        return res.sendStatus(HTTP_NOT_FOUND);
      AR.enviarDatos(provincia, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Crear provincia.
router.post('/', upload.none(), validar('admin'), async(req, res) => {
  const datos = {
    nombre: req.body.nombre,
    id_region: req.body.region || req.body.id_region
  };

  let err;
  if (!datos.nombre)
    err = 'Ingrese el nombre de la provincia';

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);

  await Provincia.query()
    .insert(datos)
    .then(provincia => AR.enviarDatos(provincia, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

// Editar provincia.
router.put('/:id(\\d+)', upload.none(), validar('admin'), async(req, res) => {
  const datos = {
    nombre: req.body.nombre,
    id_region: req.body.region || req.body.id_region
  };

  await Provincia.query()
    .patchAndFetchById(req.params.id, datos)
    .then(provincia => {
      if (!provincia)
        return res.sendStatus(HTTP_NOT_FOUND);
      AR.enviarDatos(provincia, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Eliminar provincia.
router.delete('/:id(\\d+)', validar('admin'), async(req, res) => {
  await Provincia.query()
    .findById(req.params.id)
    .delete()
    .then(() => res.sendStatus(HTTP_OK))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});



module.exports = router;
