const validar = require('./sesion2');
const sanitizeString = require('../functions/sanitizeString');
const AR = require('../ApiResponser');
const upload = require('multer')();
const router = require('express').Router();

const
  HTTP_OK = 200,
  HTTP_BAD_REQUEST = 400,
  HTTP_NOT_FOUND = 404,
  HTTP_UNAUTHORIZED = 401,
  HTTP_SERVER_ERROR = 500

const jsonfile = require('jsonfile');
var fs = require('fs');


function Quitarcoma(value) {
  if(value){
    for (var i = 0; i < value.split(".").length+10; i++) {
      value = value.replace(".", "");
    }
    value = value.replace(",", ".");
  }
  if(value.length=="3" || value.length=="2"){
    if(value.split(".").length==0)
      value = "0."+""+value;
    else
    value = "0"+""+value;
  }
  return parseFloat(value);
}

router.get("/", /*validar('admin'),*/ async (req, res) => {
  var file = __dirname+ '/../configuracion.json'
  var files = jsonfile.readFileSync(file);
  AR.enviarDatos(files, res);
});

router.post("/",upload.none(), validar('admin'), async (req, res) => {
    var file = __dirname+ '/../configuracion.json'
    err = false;
    data = req.body;
    /*if (data.cal_r.length <1) {
      err = 'Introduzca el coeficiente para el calculo del peso de USUARIOS REGISTRADOS'
    }
    if (data.cal_nr.length <1) {
      err = 'Introduzca el coeficiente para el calculo del peso de USUARIOS NO REGISTRADOS'
    }*/
    if (data.max_n.length <1) {
      err = 'Introduzca el Maximo Asegurado NACIONAL'
    }
    if (data.max_i.length <1) {
      err = 'Introduzca el Maximo Asegurado INTERNACIONAL'
    }
    if (data.adu_e.length <1) {
      err = 'Introduzca el COSTE ADUANA - IMPORTACIÓN PARA PAÍSES EXTRACOMUNITARIOS'
    }
    if (!data.por_i) {
      err = 'Introduzca el Porcentaje de IVA'
    }

    if (data.por_i >100) {
      err = 'El Porcentaje de IVA no debe ser mayor a 100%'
    }

    if(err){return AR.enviarError(err, res, HTTP_BAD_REQUEST);}

    jsonfile.readFile(file, function(err, obj) {
      var fileObj = obj;

      //fileObj.cal_peso_reg=parseFloat(data.cal_r);
      fileObj.adu_imp_ext=parseFloat(data.adu_e);
      fileObj.max_ase_nac=parseFloat(data.max_n);
      fileObj.max_ase_int=parseFloat(data.max_i);
      fileObj.por_iva=parseFloat(data.por_i);

      jsonfile.writeFile(file, fileObj, function(err,obj) {
        if (err) throw err;

      fileObj.msg = "Configuración actualizada con éxito";
      fileObj.r= true;
      AR.enviarDatos(fileObj, res);
      });
    });

});

module.exports = router;
