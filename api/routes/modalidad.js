const Modalidad = require('../models/Modalidad');

const validar = require('./sesion2');
const sanitizeString = require('../functions/sanitizeString');
const AR = require('../ApiResponser');
const upload = require('multer')();
const router = require('express').Router();

const
  HTTP_OK = 200,
  HTTP_BAD_REQUEST = 400,
  HTTP_NOT_FOUND = 404,
  HTTP_UNAUTHORIZED = 401,
  HTTP_SERVER_ERROR = 500


router.get('/', async(req, res) => {
  const params = req.query;
  const Query = Modalidad.query()
    .select('modalidad.*', 'pais.nombre AS pais', 'zona.nombre AS zona')
    .leftJoin('pais', 'pais.id', 'modalidad.id_pais')
    .leftJoin('zona', 'zona.id', 'modalidad.id_zona');

  if (+params.page === +params.page)
    Query.page(params.page, +params.pagesize ? params.pagesize : undefined)

  if (params.filter && params.filter.length > 1){
    Query.having('nombre', 'LIKE', '%'+sanitizeString(params.filter)+'%');
    Query.orHaving('pais', 'LIKE', '%'+sanitizeString(params.filter)+'%');
    Query.orHaving('zona', 'LIKE', '%'+sanitizeString(params.filter)+'%');
  }

  await Query
    .then(data => AR.enviarDatos(data, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

router.get('/:id(\\d+)', async(req, res) => {
  await Modalidad.query()
    .findById(req.params.id)
    .then(data => AR.enviarDatos(data, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});


router.post('/', upload.none(), validar('admin'), async(req, res) => {
  const datos = {
    nombre: req.body.nombre,
    descripcion: req.body.descripcion,
    horas: req.body.horas,
    id_pais: req.body.id_pais,
    id_zona: req.body.id_zona,
    monto: req.body.monto,
  };

  let err;

  if (!datos.nombre)
    err = 'Introduzca el nombre';

  // else if (!datos.id_pais)
  //   err = 'Introduzca el país';

  else if (!datos.id_zona)
    err = 'Introduzca la zona';

  // else if (!datos.monto)
  //   err = 'Introduzca el monto';

  if (err)
    return AR.enviarDatos({r:false, msg:err})

  await Modalidad.query()
    .insert(datos)
    .then(() => AR.enviarDatos({r:true}, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });

});

router.put('/:id(\\d+)', upload.none(), validar('admin'), async(req, res) => {
  const datos = {
    nombre: req.body.nombre,
    descripcion: req.body.descripcion,
    horas: req.body.horas,
    id_pais: req.body.id_pais,
    id_zona: req.body.id_zona,
    monto: req.body.monto,
  };

  await Modalidad.query()
    .patchAndFetchById(req.params.id, datos)
    .then(() => AR.enviarDatos({r:true}, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

router.delete('/:id(\\d+)', validar('admin'), async(req, res) => {
  await Modalidad.query()
    .findById(req.params.id)
    .delete()
    .then(() => AR.enviarDatos({r:true}, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});


module.exports = router;