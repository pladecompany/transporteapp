const Region = require('../models/Region');

const validar = require('./sesion2');
const AR = require('../ApiResponser');
const sanitizeString = require('../functions/sanitizeString');
const upload = require('multer')();
const router = require('express').Router();

const
  HTTP_OK = 200,
  HTTP_BAD_REQUEST = 400,
  HTTP_NOT_FOUND = 404,
  HTTP_UNAUTHORIZED = 401,
  HTTP_SERVER_ERROR = 500



// Listar regiones.
router.get('/', /*validar('admin'),*/ async(req, res) => {
  const params = req.query;

  const Query = Region.query()
    .select('region.*', 'pais.nombre AS pais')
    .join('pais', 'pais.id', 'region.id_pais');

  if (+params.page === +params.page)
    Query.page(params.page, +params.pagesize ? params.pagesize : undefined)

  if (params.filter && params.filter.length > 1){
    Query.having('nombre', 'LIKE', '%'+sanitizeString(params.filter)+'%');
    Query.orHaving('pais', 'LIKE', '%'+sanitizeString(params.filter)+'%');
  }
  Query.orderBy('nombre');
  await Query
    .then(data => AR.enviarDatos(data, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Recuperar region sin zona.
router.get('/sin_zona', validar('admin'), async(req, res) => {
  await Region.query().where('id_zona',null).orderBy('nombre')
    .then(region => {
      if (!region)
        return res.sendStatus(HTTP_NOT_FOUND);
      AR.enviarDatos(region, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Recuperar region sin zona.
router.get('/sin_zona/:id(\\d+)', validar('admin'), async(req, res) => {
  await Region.query().where('id_zona',null).where('id_pais',req.params.id)
    .then(region => {
      if (!region)
        return res.sendStatus(HTTP_NOT_FOUND);
      AR.enviarDatos(region, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Recuperar region.
router.get('/:id(\\d+)', validar('admin'), async(req, res) => {
  await Region.query().findById(req.params.id)
    .then(region => {
      if (!region)
        return res.sendStatus(HTTP_NOT_FOUND);
      AR.enviarDatos(region, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Crear region.
router.post('/', upload.none(), validar('admin'), async(req, res) => {
  const datos = {
    nombre: req.body.nombre,
    zona: req.body.zona || req.body.id_zona,
    id_pais: req.body.pais || req.body.id_pais
  };

  let err;
  if (!datos.nombre)
    err = 'Ingrese el nombre de la región';

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);

  await Region.query()
    .insert(datos)
    .then(region => AR.enviarDatos(region, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

// Editar region.
router.put('/:id(\\d+)', upload.none(), validar('admin'), async(req, res) => {
  const datos = {
    nombre: req.body.nombre,
    zona: req.body.zona || req.body.id_zona,
    id_pais: req.body.pais || req.body.id_pais
  };

  await Region.query()
    .patchAndFetchById(req.params.id, datos)
    .then(region => {
      if (!region)
        return res.sendStatus(HTTP_NOT_FOUND);
      AR.enviarDatos(region, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Eliminar region.
router.delete('/:id(\\d+)', validar('admin'), async(req, res) => {
  await Region.query()
    .findById(req.params.id)
    .delete()
    .then(() => res.sendStatus(HTTP_OK))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Regiones por pais.
router.get('/por-pais/:id_pais(\\d+)', /*validar('admin'),*/ async(req, res) => {
  await Region.query()
    .where('id_pais', req.params.id_pais)
    .then(data => AR.enviarDatos(data, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});


module.exports = router;
