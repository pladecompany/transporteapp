const Cliente = require('../models/Cliente');
const Admin = require('../models/Admin');
const Notificaciones = require("../models/Notificacion");

const validar = require('./sesion2');
const AR = require('../ApiResponser');
const hashPass = require('../functions/hashPass');
const checkEmail = require('../functions/checkEmail');
const sanitizeString = require('../functions/sanitizeString');
const upload = require('multer')();
const router = require('express').Router();

// const enviarClienteRechazado = require('../mail/senders/cliente_rechazado');
// const enviarClienteAprobado = require('../mail/senders/cliente_aprobado');
const enviarRegistroExitoso = require('../mail/senders/registro_cliente');

const
  HTTP_OK = 200,
  HTTP_BAD_REQUEST = 400,
  HTTP_NOT_FOUND = 404,
  HTTP_UNAUTHORIZED = 401,
  HTTP_SERVER_ERROR = 500

// Listar Clientes.
router.get('/', validar('admin'), async(req, res) => {
  const params = req.query;
  const Query = Cliente.query();

  if (+params.page === +params.page)
    Query.page(params.page, +params.pagesize ? params.pagesize : undefined)

  if (params.filter && params.filter.length > 1){
    Query.having('nombre', 'LIKE', '%'+sanitizeString(params.filter)+'%');
    Query.orHaving('correo', 'LIKE', '%'+sanitizeString(params.filter)+'%');
    Query.orHaving('dni', 'LIKE', '%'+sanitizeString(params.filter)+'%');
  }
  //Query.where('estatus','<>',1)

  if(params.domiciliazacion=='1')
    Query.where('domiciliacion_bancaria',1)
  else if(params.domiciliazacion=='2')
    Query.where('domiciliacion_bancaria','<>',1)

  if(params.tiponivel)
    Query.where('estatus',params.tiponivel)
  else
    Query.where('estatus','<>',1)

  await Query
    .then(Clientes => {
      if (Clientes.results && Clientes.results.length) {
        for (var i = 0; i < Clientes.results.length; i++) {
          Clientes.results[i].dom_ban = false;
          if(Clientes.results[i].domiciliacion_bancaria)
            Clientes.results[i].dom_ban = true;
          Clientes.results[i].nivel_1 = false;
          if(Clientes.results[i].estatus==3)
            Clientes.results[i].nivel_1 = true;
        }
      }
      AR.enviarDatos(Clientes, res)
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Por DNI
router.get('/por-dni/:dni', async(req, res) => {
  const solo_registrados = req.query.registrados;

  const Query = Cliente.query()
    .findOne('dni', req.params.dni)

  if (solo_registrados)
    Query.where('estatus', '<>', '1');

  await Query.then(cli => {
      if (!cli)
        return res.sendStatus(HTTP_NOT_FOUND);

      delete cli.token;
      AR.enviarDatos(cli, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })

});

// Crear Cliente.
router.post('/', upload.none(), validar('admin'), async(req, res) => {
  const body = req.body;
  //console.log(body);
  const datos = {
    nombre: body.nombre,
    correo: body.correo ,
    dni: body.dni,
    cod_telefono: body.cod,
    telefono: body.tlf,
    direccion: body.direccion,
    localidad: body.localidad,
    codigo_postal: body.cp,
    empresa: body.empresa,
    domiciliacion_bancaria: body.domiciliacion,
    id_pais: body.pais,
    coeficiente_envio: body.coeficiente,
    porcentaje_des: body.descuento,
    //id_region: body.region,
    id_provincia: body.provincia,
    pass: body.pass,
    localidad: body.localidad
  }
  if(body.nivel)
    datos.estatus = 3;
  else
    datos.estatus = 2;

  const isNumber = n => +n === +n;

  let err;
  if (!datos.nombre)
    err = 'Ingrese el nombre';
  else if (!datos.correo)
    err = 'Ingrese el correo';
  else if (!datos.dni)
    err = 'Ingrese el DNI/Pasaporte';
  else if (datos.dni.length<9 || datos.dni.length>12)
    err = 'El DNI/Pasaporte debe tener de 9 a 12 caracteres';
  else if (!datos.cod_telefono)
    err = 'Selecciona el código del país';
  else if (!datos.telefono)
    err = 'Ingrese el Teléfono';
  else if (datos.telefono.length<9 || datos.telefono.length>12)
    err = 'El Teléfono debe tener de 9 a 12 caracteres';
  else if (!datos.direccion)
    err = 'Ingrese el Dirección';
  else if (!body.localidad)
    err = 'Ingresa la localidad';
  else if (!datos.id_pais)
    err = 'Selecciona el pais';
  else if (!datos.id_provincia && datos.id_pais==1)
    err = 'Selecciona la provincia';
  else if (!datos.codigo_postal)
    err = 'Ingresa el Código postal';
  else if (!datos.pass)
    err = 'Ingresa la contraseña';
  else if (!body.pass2)
    err = 'Ingresa el confirmar contraseña';
  else if (body.pass2!=datos.pass)
    err = 'Las contraseñas no coinciden';
  else if (!isNumber(datos.porcentaje_des))
    err = 'Ingresa el porcentaje de descuento';
  else if (datos.porcentaje_des >100)
    err = 'El porcentaje de descuento no debe ser mayor a 100%';

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);

  const existe = await Cliente.query().findOne({correo: datos.correo}).where('estatus','<>',1).catch(console.error);
  if (existe)
    return AR.enviarError('El correo ya existe', res, HTTP_BAD_REQUEST);

  if(datos.id_pais>1)
    datos.id_provincia = null;
  datos.pass = await hashPass(datos.pass);
  await Cliente.query()
    .insert(datos)
    .then(Cliente => {
      AR.enviarDatos(Cliente, res)
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR)
    });
});

// Registro cliente temporal.
router.post('/temporal', upload.none(), validar('admin'), async(req, res) => {
  const datos = {
    dni: req.body.dni || req.body.identificacion,
    correo: req.body.correo,
    cod_telefono: req.body.cod || req.body.cod_telefono,
    telefono: req.body.cod_telefono,
    es_empresa: req.body.es_empresa,
  };


  let err;
  if (!datos.correo)
    err = 'Ingrese el correo';
  else if (!datos.dni)
    err = 'Ingrese el DNI/Pasaporte';
  else if (+datos.dni !== +datos.dni)
    err = 'Ingrese el DNI/Pasaporte';
  else if (datos.dni.length<9 || datos.dni.length>12)
    err = 'El DNI/Pasaporte debe tener de 9 a 12 caracteres';
  else if (!datos.telefono)
    err = 'Ingrese el Teléfono';
  else if (datos.telefono.length<9 || datos.telefono.length>12)
    err = 'El Teléfono debe tener de 9 a 12 caracteres';
  else if (!checkEmail(datos.correo))
    err = 'Ingrese un correo válido';


  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);


  let existe = await Cliente.query().findOne({correo: datos.correo}).catch(console.error);
  if (existe)
    err = 'El correo ya existe';

  existe = await Cliente.query().findOne({dni: datos.dni}).catch(console.error);
  if (existe)
    err = 'El DNI/pasaporte ya existe';

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);

  // Es temporal.
  datos.estatus = '1';

  await Cliente.query()
    .insert(datos)
    .then(Cliente => {
      // enviarRegistroExitoso(Cliente.nombre, Cliente.correo);
      AR.enviarDatos(Cliente, res)
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR)
    });
});

// Registro desde landing
router.post('/landing', upload.none(), async(req, res) => {
  const body = req.body;
  const completando = req.body.completando_registro;

  const datos = {
    nombre: body.nombre,
    correo: body.correo ,
    dni: body.dni,
    cod_telefono: body.cod,
    telefono: body.tlf,
    direccion: body.direccion,
    localidad: body.localidad,
    codigo_postal: body.cp,
    empresa: body.empresa,
    id_pais: body.pais,
    id_provincia: body.provincia,
    pass: body.pass || body['contraseña'],
    estatus: '2',
  }

  let err;
  if (!datos.nombre)
    err = 'Ingrese el nombre';
  else if (!datos.correo)
    err = 'Ingrese el correo';
  else if (!datos.dni)
    err = 'Ingrese el DNI/Pasaporte';
  else if (datos.dni.length<9 || datos.dni.length>12)
    err = 'El DNI/Pasaporte debe tener de 9 a 12 caracteres';
  else if (!datos.telefono)
    err = 'Ingrese el Teléfono';
  else if (datos.telefono.length<9 || datos.telefono.length>12)
    err = 'El Teléfono debe tener de 9 a 12 caracteres';
  else if (!datos.direccion)
    err = 'Ingrese la dirección';
  else if (!datos.localidad)
    err = 'Ingrese la localidad';
  else if (!datos.id_pais)
    err = 'Selecciona el pais';
  else if (!datos.id_provincia && datos.id_pais==1)
    err = 'Selecciona la provincia';
  else if (!datos.codigo_postal)
    err = 'Ingresa el Código postal';
  else if (!checkEmail(datos.correo))
    err = 'Ingrese un correo válido';
  else if (!datos.pass || datos.pass < 8)
    err = 'La contraseña debe tener un mínimo de 8 caracteres';


  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);

  let cli = await Cliente.query().findOne({correo: datos.correo}).catch(console.error);
  if (cli && !completando)
    err = 'El correo ya existe';

  cli = await Cliente.query().findOne({dni: datos.dni}).catch(console.error);
  if (cli && cli.estatus != 1 && !completando) {
    err = 'El DNI/pasaporte ya existe';
  }


  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);


  if(datos.id_pais>1)
    datos.id_provincia = null;

  const Query = Cliente.query();

  // Actualizar los datos si era cliente temporal.
  if (cli && cli.estatus == 1) {
    // datos.estatus = '2';

    if (completando) {
      datos.pass = await hashPass(datos.pass);
    }
    else {
      delete datos.pass;
    }
    Query.patchAndFetchById(cli.id, datos);
  }
  else {
    datos.pass = await hashPass(datos.pass);
    Query.insert(datos);
  }


  await Query.then(async Cliente => {
    enviarRegistroExitoso(Cliente.nombre, Cliente.correo);

    const ADMIN = await Admin.query();
    for (var i = 0; i < ADMIN.length; i++) {
      await Notificaciones.query()
        .insert({
          titulo: 'Nuevo cliente',
          contenido: 'Se ha registrado un nuevo cliente desde el landing',
          fecha: new Date(),
          estatus: 0,
          tipo_noti: 2,
          url: 'Clientes',
          id_usable: Cliente.id,
          id_receptor: ADMIN[i].id,
          id_emisor: Cliente.id,
          tabla_usuario_r: 'admin',
          tabla_usuario_e: 'cliente'
        }).catch(async(err)=>{console.log(err);});
    }
    const resp = {act_noti:true};
    IO.sockets.emit("RecibirNotificacionPanel", {resp});
    AR.enviarDatos(Cliente, res)
  })
  .catch(err => {
    console.error(err);
    res.sendStatus(HTTP_SERVER_ERROR)
  });
});

// Aprobar cliente registrado desde el landing.
router.put('/aprobar/:id(\\d+)', upload.none(), validar('admin'), async(req, res) => {
  const datos = {
    coeficiente_envio: req.body.coeficiente,
    porcentaje_des: req.body.descuento,
    domiciliacion_bancaria: req.body.domiciliacion,
    // estatus: 'aprobado',
  };

  const isNumber = n => +n === +n;

  // if (!datos.coeficiente_envio || !datos.descuento)
  //   return res.sendStatus(HTTP_BAD_REQUEST);

  if (!isNumber(datos.coeficiente_envio) || !isNumber(datos.porcentaje_des))
    return AR.enviarError('Ingrese los datos', res, HTTP_BAD_REQUEST);


  await Cliente.query()
    .patchAndFetchById(req.params.id, datos)
    .then(cli => {
      enviarClienteAprobado(cli.nombre, cli.correo);
      AR.enviarDatos(cli, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR)
    });
});

// Aprobar cliente registrado desde el landing.
router.put('/editar_campo/:id(\\d+)', upload.none(), validar('admin'), async(req, res) => {
  const datos = {};
  if (!req.body.tipo_columna || req.body.valor.length == 0 )
    return AR.enviarError('Ingrese los datos', res, HTTP_BAD_REQUEST);

  datos[req.body.tipo_columna]=req.body.valor;
  await Cliente.query()
    .patchAndFetchById(req.params.id, datos)
    .then(cli => {
      cli.r = true;
      AR.enviarDatos(cli, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR)
    });
});

// Rechazar cliente registrado desde el landing.
// router.put('/rechazar/:id(\\d+)', upload.none(), validar('admin'), async(req, res) => {
//   const motivo = req.body.motivo || req.body.razon;

//   if (!motivo)
//     return AR.enviarError('Ingrese el motivo', res, HTTP_BAD_REQUEST);

//   await Cliente.query()
//     .patchAndFetchById(req.params.id, { motivo, estatus:'rechazado' })
//     .then(cli => {
//       enviarClienteRechazado(cli.nombre, cli.correo, cli.motivo);
//       // console.log('RECHAZADO: ', cli.nombre)
//       AR.enviarDatos({r:true}, res);
//     })
//     .catch(err => {
//       console.error(err);
//       res.sendStatus(HTTP_SERVER_ERROR)
//     });
// });

// Recuperar Cliente.
router.get('/:id(\\d+)', /*validar('admin'),*/ async(req, res) => {
  const id_Cliente = req.params.id;

  await Cliente.query()
    .findById(id_Cliente)
    .then(Cliente => {
      if (!Cliente) return res.sendStatus(HTTP_NOT_FOUND);

      delete Cliente.pass;
      AR.enviarDatos(Cliente, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

// Editar perfil.
router.put('/perfil/:id(\\d+)', upload.none(), /*validar('cliente'),*/ async(req, res) => {
  const id_Cliente = req.params.id;
  const body = req.body;
  const datos = {
    nombre: body.nombre,
    correo: body.correo ,
    dni: body.dni,
    cod_telefono: body.cod,
    telefono: body.tlf,
    direccion: body.direccion,
    codigo_postal: body.cp,
    empresa: body.empresa,
    id_pais: body.pais,
    id_provincia: body.provincia,
    localidad: body.localidad
  }

  let err;
  if (!datos.nombre)
    err = 'Ingrese el nombre';
  else if (!datos.correo)
    err = 'Ingrese el correo';
  else if (!datos.dni)
    err = 'Ingrese el DNI/Pasaporte';
  else if (datos.dni.length<9 || datos.dni.length>12)
    err = 'El DNI/Pasaporte debe tener de 9 a 12 caracteres';
  else if (!datos.cod_telefono)
    err = 'Selecciona el código del país';
  else if (!datos.telefono)
    err = 'Ingrese el Teléfono';
  else if (datos.telefono.length<9 || datos.telefono.length>12)
    err = 'El Teléfono debe tener de 9 a 12 caracteres';
  else if (!datos.direccion)
    err = 'Ingrese el Dirección';
  else if (!datos.id_pais)
    err = 'Selecciona el pais';
  else if (!datos.localidad)
    err = 'Ingresa la localidad';
  else if (!datos.id_provincia && datos.id_pais==1)
    err = 'Selecciona la provincia';
  else if (!datos.codigo_postal)
    err = 'Ingresa el Código postal';

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);

  if (datos.correo) {
    const existe = await Cliente.query()
        .findOne({ correo: datos.correo })
        .where('estatus','<>',1)
        .where('id', '<>', id_Cliente)
        .catch(console.error);

    if (existe)
      return AR.enviarError('El correo ya existe', res, HTTP_BAD_REQUEST);
  }

  if(datos.id_pais>1)
    datos.id_provincia = null;

  await Cliente.query()
    .patchAndFetchById(id_Cliente, datos)
    .then(Cliente => {
      if (!Cliente) return res.sendStatus(404);

      AR.enviarDatos(Cliente, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

// Editar Cliente.
router.put('/:id(\\d+)', upload.none(), validar('admin'), async(req, res) => {
  const id_Cliente = req.params.id;
  const body = req.body;

  const datos = {
    nombre: body.nombre,
    correo: body.correo ,
    dni: body.dni,
    cod_telefono: body.cod,
    telefono: body.tlf,
    direccion: body.direccion,
    codigo_postal: body.cp,
    empresa: body.empresa,
    domiciliacion_bancaria: body.domiciliacion,
    id_pais: body.pais,
    coeficiente_envio: body.coeficiente,
    porcentaje_des: body.descuento,
    //id_region: body.region,
    id_provincia: body.provincia,
    localidad: body.localidad
  }

  if(body.nivel)
    datos.estatus = 3;
  else
    datos.estatus = 2;

  const isNumber = n => +n === +n;

  let err;
  if (!datos.nombre)
    err = 'Ingrese el nombre';
  else if (!datos.correo)
    err = 'Ingrese el correo';
  else if (!datos.dni)
    err = 'Ingrese el DNI/Pasaporte';
  else if (datos.dni.length<9 || datos.dni.length>12)
    err = 'El DNI/Pasaporte debe tener de 9 a 12 caracteres';
  else if (!datos.cod_telefono)
    err = 'Selecciona el código del país';
  else if (!datos.telefono)
    err = 'Ingrese el Teléfono';
  else if (datos.telefono.length<9 || datos.telefono.length>12)
    err = 'El Teléfono debe tener de 9 a 12 caracteres';
  else if (!datos.direccion)
    err = 'Ingrese el Dirección';
  else if (!body.localidad)
    err = 'Ingresa la localidad';
  else if (!datos.id_pais)
    err = 'Selecciona el pais';
  else if (!datos.id_provincia && datos.id_pais==1)
    err = 'Selecciona la provincia';
  else if (!datos.codigo_postal)
    err = 'Ingresa el Código postal';
  else if (!body.pass && body.pass2)
    err = 'Ingresa la contraseña';
  else if (!body.pass2 && body.pass)
    err = 'Ingresa el confirmar contraseña';
  else if (datos.pass && body.pass2!=body.pass)
    err = 'Las contraseñas no coinciden';
  else if (!isNumber(datos.porcentaje_des))
    err = 'Ingresa el porcentaje de descuento';
  else if (datos.porcentaje_des >100)
    err = 'El porcentaje de descuento no debe ser mayor a 100%';

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);

  if (datos.correo) {
    const existe = await Cliente.query()
        .findOne({ correo: datos.correo })
        .where('estatus','<>',1)
        .where('id', '<>', id_Cliente)
        .catch(console.error);

    if (existe)
      return AR.enviarError('El correo ya existe', res, HTTP_BAD_REQUEST);
  }

  if(datos.id_pais>1)
    datos.id_provincia = null;
  if(body.pass)
    datos.pass = await hashPass(body.pass);
  await Cliente.query()
    .patchAndFetchById(id_Cliente, datos)
    .then(Cliente => {
      if (!Cliente) return res.sendStatus(404);

      AR.enviarDatos(Cliente, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

// Eliminar Cliente.
router.delete('/:id(\\d+)', validar('admin'), async(req, res) => {
  await Cliente.query()
    .findById(req.params.id)
    .delete()
    .then(() => res.sendStatus(HTTP_OK))
    .catch(err => {
      console.error(err);

      if (err && err.name && err.name.startsWith('ForeignKeyViolation')) {
        AR.enviarError('No se puede eliminar', res, HTTP_SERVER_ERROR);
      }
      else {
        res.sendStatus(HTTP_SERVER_ERROR);
      }
    });
});


module.exports = router;
