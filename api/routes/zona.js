const Zona = require('../models/Zona');
const PesosZona = require('../models/PesosZona');
const Region = require('../models/Region');
const Pais = require('../models/Pais');
const Aduana = require('../models/Aduana');
const Modalidad = require('../models/Modalidad');
const Provincia = require('../models/Provincia');

const validar = require('./sesion2');
const sanitizeString = require('../functions/sanitizeString');
const AR = require('../ApiResponser');
const upload = require('multer')();
const router = require('express').Router();


const ID_ESPAÑA = 1;

const ID_PROVINCIAS_ESPECIALES = [
  12, // LAS PALMAS
  13, // SANTA CRUZ DE TENERIFE
  45, // BALEARES RESTO DE ISLAS
  51, // BALEARES MALLORCA
  52, // CANARIAS RESTO DE ISLAS
  53, // CEUTA
  54, // MELILLA 
];



const
  HTTP_OK = 200,
  HTTP_BAD_REQUEST = 400,
  HTTP_NOT_FOUND = 404,
  HTTP_UNAUTHORIZED = 401,
  HTTP_SERVER_ERROR = 500

// Listar Zonas.
router.get('/', validar('admin'), async(req, res) => {
  const params = req.query;

  const Query = Zona.query();

  if (+params.page === +params.page)
    Query.page(params.page, +params.pagesize ? params.pagesize : undefined)

  if (params.filter && params.filter.length > 1){
    Query.having('nombre', 'LIKE', '%'+sanitizeString(params.filter)+'%');
  }

  if (params.envio)
    Query.having('envio', '=', params.envio)

  await Query
    .then(Zonas => AR.enviarDatos(Zonas, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Recuperar Zona.
router.get('/:id(\\d+)', validar('admin'), async(req, res) => {
  await Zona.query().findById(req.params.id)
    .then(Zona => {
      if (!Zona)
        return res.sendStatus(HTTP_NOT_FOUND);
      AR.enviarDatos(Zona, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Crear Zona.
router.post('/', upload.none(), validar('admin'), async(req, res) => {
  //console.log(req.body);
  var paises = req.body.pais;
  var regiones = req.body.region;
  var pesos = req.body.pesos;
  var tiempo = req.body.tiempo;
  const datos = {
    nombre: req.body.nombre,
    tipo: req.body.tipo,
    envio: req.body.tipoe,
    //extracomunitario: req.body.extracomunitario,
    coeficiente_registrado:req.body.coe_usu,
    coeficiente_registrado_lvl_1:req.body.coe_usu_1,
    coeficiente_no_registrado:req.body.coe_no_usu,
  };
  if(req.body.precio)
    datos.tarifa_adicional = req.body.precio;

  let err;
  if (!datos.nombre)
    err = 'Ingrese el nombre de la Zona';
  if (!datos.tipo)
    err = 'Ingrese el tipo de la Zona';
  if (!datos.envio)
    err = 'Ingrese el tipo de envio de la Zona';

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);

  await Zona.query()
    .insert(datos)
    .then(async Zona => {
      if(Zona.tipo=='Nacional' || Zona.tipo=='Internacional/Provincia'){
        for (var i = 0; i < regiones.length; i++) {
          await Region.query().patchAndFetchById(regiones[i].id, {id_zona:Zona.id}).catch(err => {console.error(err);});
        }
      }else if(Zona.tipo=='Internacional'){
        for (var i = 0; i < paises.length; i++) {
          await Pais.query().patchAndFetchById(paises[i].id, {id_zona:Zona.id}).catch(err => {console.error(err);});
        }
      }

      for (var i = 0; i < pesos.length; i++) {
        var datos_pesos = {desde:pesos[i].Desde,hasta:pesos[i].Hasta,costo:pesos[i].Costo,id_zona:Zona.id}
        if(tiempo)
          datos_pesos.tiempo = pesos[i].Tiempo;
        await PesosZona.query().insert(datos_pesos).catch(err => {console.error(err);});
      }
      await Aduana.query().insert({importacion:0,dua:0,dua_exportacion:0,id_zona:Zona.id}).catch(err => {console.error(err);});
      AR.enviarDatos(Zona, res)
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

// Editar Zona.
router.put('/:id(\\d+)', upload.none(), validar('admin'), async(req, res) => {
  //console.log(req.body);
  var paises = req.body.pais;
  var regiones = req.body.region;
  var pesos = req.body.pesos;
  var tiempo = req.body.tiempo;
  const datos = {
    nombre: req.body.nombre,
    tipo: req.body.tipo,
    envio: req.body.tipoe,
    //extracomunitario: req.body.extracomunitario,
    coeficiente_registrado:req.body.coe_usu,
    coeficiente_registrado_lvl_1:req.body.coe_usu_1,
    coeficiente_no_registrado:req.body.coe_no_usu,
  };
  if(req.body.precio)
    datos.tarifa_adicional = req.body.precio;

  let err;
  if (!datos.nombre)
    err = 'Ingrese el nombre de la Zona';
  if (!datos.tipo)
    err = 'Ingrese el tipo de la Zona';
  if (!datos.envio)
    err = 'Ingrese el tipo de envio de la Zona';

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);

  await Zona.query()
    .patchAndFetchById(req.params.id, datos)
    .then(async Zona => {
      if (!Zona)
        return res.sendStatus(HTTP_NOT_FOUND);

      await Region.query().update({id_zona:null}).where('id_zona',req.params.id).catch(err => {console.error(err);});
      await Pais.query().update({id_zona:null}).where('id_zona',req.params.id).catch(err => {console.error(err);});
      await PesosZona.query().delete().where('id_zona',req.params.id).catch(err => {console.error(err);});

      if(Zona.tipo=='Nacional' || Zona.tipo=='Internacional/Provincia'){
        for (var i = 0; i < regiones.length; i++) {
          await Region.query().patchAndFetchById(regiones[i].id, {id_zona:Zona.id}).catch(err => {console.error(err);});
        }
      }else if(Zona.tipo=='Internacional'){
        for (var i = 0; i < paises.length; i++) {
          await Pais.query().patchAndFetchById(paises[i].id, {id_zona:Zona.id}).catch(err => {console.error(err);});
        }
      }

      for (var i = 0; i < pesos.length; i++) {
        var datos_pesos = {desde:pesos[i].Desde,hasta:pesos[i].Hasta,costo:pesos[i].Costo,id_zona:Zona.id}
        if(tiempo)
          datos_pesos.tiempo = pesos[i].Tiempo;
        await PesosZona.query().insert(datos_pesos).catch(err => {console.error(err);});
      }
      AR.enviarDatos(Zona, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Eliminar Zona.
router.delete('/:id(\\d+)', validar('admin'), async(req, res) => {
  const existe = await Modalidad.query().where('id_zona',req.params.id).catch(console.error);
  if (existe.length>0)
    return AR.enviarError('Esta zona no se puede eliminar, información usada.', res, HTTP_BAD_REQUEST);

  const existe_a = await Aduana.query().where('id_zona',req.params.id).whereRaw('(importacion>0 OR dua>0 OR dua_exportacion>0)').catch(console.error);
  if (existe_a.length>0)
    return AR.enviarError('Esta zona no se puede eliminar, información usada.', res, HTTP_BAD_REQUEST);


  await Zona.query()
    .findById(req.params.id)
    .delete()
    .then(() => {
      Region.query().update({id_zona:null}).where('id_zona',req.params.id).catch(err => {console.error(err);});
      Pais.query().update({id_zona:null}).where('id_zona',req.params.id).catch(err => {console.error(err);});
      res.sendStatus(HTTP_OK);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});



/* Condicionales para determinar el tipo de envio */
router.post('/por-tipo-envio', upload.none(), async(req, res) => {
  const id_pais1 = req.body.pais1;
  const id_provincia1 = req.body.provincia1;

  const id_pais2 = req.body.pais2;
  const id_provincia2 = req.body.provincia2;

  const españa = await Pais.query()
    .where('nombre', 'LIKE', '%espa%')
    .first()
    .catch(console.error);

  const isNum = n => +n===+n;


  /* Verificacion de errores */
  let err = null;

  if (!isNum(id_pais1)) {
    err = 'Especificar pais de procedencia';
  }
  else if (!isNum(id_pais2)) {
    err = 'Especificar pais de destino';
  }
  else if (id_pais1 == españa.id && !isNum(id_provincia1)) {
    err = 'Especificar provincia de procedencia';
  }
  else if (id_pais2 == españa.id && !isNum(id_provincia2)) {
    err = 'Especificar provincia de destino';
  }

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);


  let pais1, provincia1, pais2, provincia2;

  pais1 = await Pais.query()
    .findById(id_pais1)
    .catch(console.error);

  if (isNum(id_provincia1)) {
    provincia1 = await Provincia.query()
      .select('provincia.*', 'zona.nombre AS zona', 'region.id_zona AS id_zona')
      .findById(id_provincia1)
      .leftJoin('region', 'region.id', 'provincia.id_region')
      .leftJoin('zona', 'zona.id', 'region.id_zona')
      .catch(console.error);
  }
  if (!provincia1)
    provincia1 = {};

  if (isNum(id_provincia2)) {
    provincia2 = await Provincia.query()
      .select('provincia.*', 'zona.nombre AS zona', 'region.id_zona AS id_zona')
      .findById(id_provincia2)
      .leftJoin('region', 'region.id', 'provincia.id_region')
      .leftJoin('zona', 'zona.id', 'region.id_zona')
      .catch(console.error);
  }
  if (!provincia2)
    provincia2 = {};


  if (+id_pais2 === +id_pais1) {
    pais2 = pais1;
  }
  else {
    pais2 = await Pais.query().findById(id_pais2).catch(console.error);
  }



  /** CONDICIONALES DE LA ZONA **/
  let id_zona_envio = null;
  const parametros = {
    dua: true,
    importacion: false,
  };

  /* DENTRO DE ESPAÑA */
  if (id_pais1 == ID_ESPAÑA && id_pais1 == id_pais2)
  {
    if (provincia1.id == provincia2.id) {
      id_zona_envio = 1; //PROVINCIAL
    } else if (provincia1.id_region == provincia2.id_region) {
      id_zona_envio = 2; //REGIONAL
    } else {
      id_zona_envio = 3; //PENINSULAR
    }

    if (ID_PROVINCIAS_ESPECIALES.includes(+provincia2.id)) {
      id_zona_envio = null;
    }
  }

  /* ESPAÑA -> INTERNACIONAL */
  else if (id_pais2 != ID_ESPAÑA)
  {
    id_zona_envio = pais2.id_zona;
  }

  /* INTERNACIONAL -> ESPAÑA */
  else if (id_pais1 != ID_ESPAÑA) {
    parametros.importacion = true;
    parametros.dua = false;
  }


  let zona_destino;
  if (id_zona_envio) {
    zona_destino = await Zona.query().findById(id_zona_envio).catch(console.error);
  }
  else if (provincia2 && provincia2.id) {
    zona_destino = await Zona.query().findById(provincia2.id_zona).catch(console.error);
  }
  else {
    zona_destino = await Zona.query().findById(pais2.id_zona).catch(console.error);
  }

  let modalidades_envio;
  if (zona_destino)
    modalidades_envio = await Modalidad.query().where('id_zona', zona_destino.id).catch(console.error);



  const datos = {
    parametros,
    zona: zona_destino,
    modalidades: modalidades_envio,
  };

  AR.enviarDatos(datos, res);


});




module.exports = router;
