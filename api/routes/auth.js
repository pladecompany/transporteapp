const Admin = require('../models/Admin');
const Cliente = require('../models/Cliente');
const TokenRecuperacion = require('../models/TokenRecuperacion');

// const enviarRecuperacion = require('../mail/enviarRecuperacion');
const enviarRecuperacion = require('../mail/senders/recuperacion');
const enviarRecuperacionCli = require('../mail/senders/recuperacion_cliente');
const AR = require('../ApiResponser');
const comparePass = require('../functions/comparePass');
const hashPass = require('../functions/hashPass');
const genToken = require('../functions/genToken');
const checkEmail = require('../functions/checkEmail');
const upload = require('multer')();
const router = require('express').Router();
const validar = require('./sesion2');

const 
  HTTP_OK = 200,
  HTTP_BAD_REQUEST = 400,
  HTTP_UNAUTHORIZED = 401,
  HTTP_FORBIDDEN = 403,
  HTTP_NOT_FOUND = 404,
  HTTP_SERVER_ERROR = 500



router.post('/login', upload.none(), async(req, res) => {
  const correo = req.body.correo || req.body.email;
  const pass = req.body.pass || req.body.password || req.body.clave;

  if (!correo || !pass) {
    return res.sendStatus(HTTP_BAD_REQUEST);
  }

  const admin = await Admin.query()
    .findOne('correo', correo)
    .catch(console.error);

  if (!admin) {
    return res.sendStatus(HTTP_UNAUTHORIZED);
  }

  const access = await comparePass(pass, admin.pass);
  if (!access){
    return res.sendStatus(HTTP_UNAUTHORIZED);
  }

  admin.token = await genToken(64);
  delete admin.pass;

  await Admin.query()
    .findById(admin.id)
    .patch({ token: admin.token })
    .catch(console.error);

  AR.enviarDatos(admin, res);
});


router.post('/login-cliente', upload.none(), async(req, res) => {
  const dni = req.body.dni || req.body.email;
  const pass = req.body.pass || req.body.password || req.body.clave;

  if (!dni || !pass) {
    return res.sendStatus(HTTP_BAD_REQUEST);
  }

  const cli = await Cliente.query()
    .findOne('dni', dni)
    .catch(console.error);

  if (!cli) {
    return res.sendStatus(HTTP_UNAUTHORIZED);
  }
  if (cli.estatus == 1 && !cli.pass) {
    return res.status(412).send(cli); // Precondition Failed (registro incompleto)
  }

  const access = await comparePass(pass, cli.pass);
  if (!access){
    return res.sendStatus(HTTP_UNAUTHORIZED);
  }

  cli.token = await genToken(64);
  delete cli.pass;

  await Cliente.query()
    .findById(cli.id)
    .patch({ token: cli.token })
    .catch(console.error);

  AR.enviarDatos(cli, res);
});

router.post('/act-cliente', upload.none(), async(req, res) => {
  const idc = req.body.id;
  
  if (!idc) {
    return res.sendStatus(HTTP_BAD_REQUEST);
  }

  const cli = await Cliente.query()
    .findOne('id', idc)
    .catch(console.error);

  if (!cli) {
    return res.sendStatus(HTTP_UNAUTHORIZED);
  }

  AR.enviarDatos(cli, res);
});

router.put('/cambiar-pass-cli', upload.none(), async(req, res) => {
  const id_usuario = req.body.id_cliente;
  const new_pass = req.body.new_pass || req.body.new_password;
  const old_pass = req.body.old_pass || req.body.old_password;

  if (!new_pass || !old_pass || !id_usuario)
    return res.sendStatus(HTTP_BAD_REQUEST);

  const cli = await Cliente.query().findById(id_usuario).catch(console.error);
  if (!cli)
    return res.sendStatus(HTTP_UNAUTHORIZED);

  const access = await comparePass(old_pass, cli.pass);
  if (!access)
    return res.sendStatus(HTTP_UNAUTHORIZED);

  cli.pass = await hashPass(new_pass);
  await Cliente.query().findById(id_usuario).patch({pass: cli.pass}).catch(console.error);

  res.sendStatus(HTTP_OK);
});


// Cambiar clave desde panel.
router.post('/change-password', upload.none(), validar('cliente'), async(req, res) => {
  const id_usuario = req.body.id_usuario || req.headers.id_usuario;
  const new_pass = req.body.new_pass || req.body.new_password;
  const old_pass = req.body.old_pass || req.body.old_password;

  if (!new_pass || !old_pass || !id_usuario)
    return res.sendStatus(HTTP_BAD_REQUEST);

  const admin = await Admin.query().findById(id_usuario).catch(console.error);
  if (!admin)
    return res.sendStatus(HTTP_UNAUTHORIZED);

  const access = await comparePass(old_pass, admin.pass);
  if (!access)
    return res.sendStatus(HTTP_UNAUTHORIZED);

  admin.pass = await hashPass(new_pass);
  await Admin.query().findById(id_usuario).patch({pass: admin.pass}).catch(console.error);

  res.sendStatus(HTTP_OK);
});

// Verificar que el token sea valido.
router.post("/check-token", upload.none(), async(req, res)=>{
  const id_usuario = req.headers.id_usuario;
  const token = req.body.token || req.query.token || req.headers.token;

  if(!token || !id_usuario)
    return res.sendStatus(HTTP_UNAUTHORIZED);

  await Admin.query()
    .findById(id_usuario)
    .where({ token })
    .then(admin => {
      if(admin)
        return res.sendStatus(HTTP_OK); // Exito, nada se devuelve.
      else
        return res.sendStatus(HTTP_UNAUTHORIZED);
    })
    .catch(err=>{
        return res.sendStatus(HTTP_UNAUTHORIZED);
    });
});

router.post('/recuperar-clave-cli', upload.none(), async(req, res) => {
  const correo = req.body.correo;

  if (!correo || !checkEmail(correo))
    return AR.enviarError('Ingrese el correo', res, HTTP_BAD_REQUEST);

  const cli = await Cliente.query().findOne({correo}).catch(console.error);
  if (!cli)
    return AR.enviarError('Correo no encontrado', res, HTTP_BAD_REQUEST);

  const new_pass = await genToken(8);
  cli.pass = await hashPass(new_pass);

  await Cliente.query().patchAndFetchById(cli.id, {pass: cli.pass}).catch(console.error);

  enviarRecuperacionCli(cli.nombre, cli.correo, new_pass)
    .then(() => res.sendStatus(HTTP_OK))
    .catch(err => {
      console.error(err);
      AR.enviarError('Ocurrió un error, intente de nuevo');
    });

  res.sendStatus(HTTP_OK);
});


/* RECUPERACION DE CLAVE */

// Paso 1, enviar correo con enlace de recuperacion.
router.post('/recuperar-clave', upload.none(), async(req, res)  => {
  const correo = req.body.correo || req.body.email;
  let urlCambio = req.body.url_cambio;

  const admin = await Admin.query()
    .findOne('correo', correo)
    .catch(console.error);

  if (!admin || !urlCambio)
    return res.sendStatus(HTTP_UNAUTHORIZED);

  const token = await genToken(100);
  const tr = await TokenRecuperacion.query().findOne('id_admin', admin.id).catch(console.error);

  if (tr)
    await TokenRecuperacion.query().findById(tr.id).patch({ token }).catch(console.error);
  else
    await TokenRecuperacion.query().insert({ token, id_admin:admin.id }).catch(console.error);

  urlCambio = (urlCambio.endsWith('/') ? urlCambio : urlCambio+'/') + token;

  enviarRecuperacion(admin.nombre, admin.correo, urlCambio)
    .then(() => res.sendStatus(HTTP_OK))
    .catch(err => {
      console.error(err);
      AR.enviarError('Ocurrió un error, intente de nuevo');
    });

});

// Paso 2, cambiar clave verificando token.
router.post('/change-password/:token', upload.none(), async(req, res) => {
  const token = req.params.token;
  const new_pass = req.body.new_pass;

  if (!new_pass || new_pass.length < 8)
    return res.sendStatus(HTTP_BAD_REQUEST);

  const tr = await TokenRecuperacion.query()
    .findOne({ token })
    .catch(console.error);

  if (!tr)
    return res.sendStatus(HTTP_NOT_FOUND);

  const pass = await hashPass(new_pass);
  await Admin.query()
    .findById(tr.id_admin)
    .patch({ pass })
    .then(async() => {
      await TokenRecuperacion.query().findById(tr.id).delete().catch(console.error);
      res.sendStatus(HTTP_OK);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});


module.exports = router;