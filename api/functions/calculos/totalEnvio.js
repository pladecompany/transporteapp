const jsonfile = require('jsonfile');
const config = jsonfile.readFileSync( __dirname + '/../../configuracion.json' );

function parseOptions(op) {
  return {
    bultos: parseFloat(op.costoBultos || op.montoBultos || op.bultos) || 0,
    modalidad: parseFloat(op.modalidad || op.montoModalidad || op.monto_modalidad) || 0,
    seguro: parseFloat(op.seguro || op.monto_seguro) || 0,
    reembolso: parseFloat(op.reembolso || op.monto_reembolso) || 0,
    dua: parseFloat(op.dua) || 0,
    importacion: parseFloat(op.importacion) || 0,
    descuento: parseFloat(op.porcentaje_des || op.porcentaje_descuento || op.descuento) || 0,
    es_nacional: parseFloat(op.es_nacional || op.nacional) || 0,
    porcentaje_seguro: parseFloat(op.porcentaje_seguro) || 0,
    porcentaje_reembolso: parseFloat(op.porcentaje_reembolso) || 0,
    iva: parseFloat(op.iva || op.por_iva || config.por_iva),
  };
}


module.exports = function totalEnvio(parametros) {
  const op = parseOptions(parametros);
  let subtotal = 0;

  subtotal += op.bultos;
  // subtotal += op.modalidad;

  if (op.seguro) {
    // subtotal += (op.porcentaje_seguro/100 * op.seguro) + op.seguro;
    subtotal += Math.max(3, op.porcentaje_seguro/100 * op.seguro);
  }

  if (op.reembolso) {
    // subtotal += (op.porcentaje_reembolso/100 * op.reembolso) + op.reembolso;
    subtotal += Math.max(3, op.porcentaje_reembolso/100 * op.reembolso);
  }

  if (op.dua) {
    subtotal += op.dua;
  }

  if (op.importacion) {
    subtotal += op.importacion;
  }

  if (op.descuento) {
    subtotal = subtotal - (op.descuento / 100 * subtotal);
  }


  if (op.iva) {
    let IVA = op.iva / 100;
    subtotal = subtotal * IVA + subtotal;
  }

  // Redondear a dos decimales.
  return Math.round(subtotal * 100) / 100;
}
