module.exports = function indiceDePesoMaximo(pesos) {
  const max = pesos.reduce((a,b) => Math.max(b.hasta,a), 0);
  return pesos.findIndex(p => p.hasta == max);
}
