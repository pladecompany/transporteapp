const costoPorPeso = require('./costoPorPeso');

module.exports =  function costoDeBultos(bultos, pesos, tarifa_kilo_adicional, coeficiente, franja_adicional=false) {
  if (!pesos)
    return 0;

  let costo = 0;
  let i, bulto, peso_a_cobrar;

  for ([i, bulto] of Object.entries(bultos)) {
    // El peso a cobrar es el mayor entre el calculo y el peso ingresado.
    peso_a_cobrar = Math.max(bulto.peso,  bulto.fondo * bulto.ancho * bulto.alto / coeficiente)
    costo += costoPorPeso(peso_a_cobrar, pesos, tarifa_kilo_adicional, franja_adicional);
  }

  return costo;
}
