module.exports = function checkUrl(url) {
  try {
    new URL(url);
    return true;
  }
  catch (e) {
    return false;
  }
}
