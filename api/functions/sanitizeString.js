module.exports = function sanitizeString(str){
  /* Limpiar input */
  str = str.replace(/[^a-z0-9áéíóúñü \.,_-]/gim, "");
  return str.trim();
}
