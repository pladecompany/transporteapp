const nodemailer = require('nodemailer');
const { mailConfig } = require('../config');

const options = {
  // from: 'Enviaxa <noreply@enviaxa.com>',
  from: mailConfig.auth.user,
};


const testmailConfig = {
    // pool: true,
    host: "0.0.0.0",
    port: 1025,
    auth: {
      user: "no-responder@enviaxa.com",
      pass: "ndLXhQgb+t^U"
    },
    tls: {
      rejectUnauthorized: false
    }
}


function send(mailOptions) {
  return nodemailer
    .createTransport(mailConfig)
    // .createTransport(testmailConfig)
    .sendMail({...options, ...mailOptions});
};


module.exports = send;
