const fs = require('fs');
const sendMail = require('../../send');
const render = require('../../../functions/renderHTML');
const config = require('../../../config');
// const mailConfig = require('./static/recuperar_clave/config');

const tmpl = fs.readFileSync(__dirname+'/index.html').toString()


const mailConfig = {
  /*attachments: [
    {
      filename: 'logo.png',
      path: __dirname + '/../logo.png',

      // Uso: <img src="cid:imagencorreo" />
      cid: 'imagencorreo',
    }
  ],*/
};


function enviarCorreo(correo, nombre, datos, pago) {
  if(pago.img_pago){
    mailConfig.attachments= [
        {
          filename: 'adjunto.jpg',
          path: pago.img_pago,
          cid: 'imagencorreo',
        }
      ];
  }
  const multiple = config.mailConfig.correo_multiple || [];
  const data = {
    bcc: [correo, multiple],
    subject: 'Resumen del envío | Enviaxa',
    html: render(tmpl, {nombre,datos,pago})
  };

  if (mailConfig.attachments)
    data.attachments = mailConfig.attachments;
  //console.log(data);
  return sendMail(data);
}


module.exports = enviarCorreo;
