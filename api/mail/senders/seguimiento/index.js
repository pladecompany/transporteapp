const fs = require('fs');
const sendMail = require('../../send');
const render = require('../../../functions/renderHTML');
// const mailConfig = require('./static/recuperar_clave/config');

const tmpl = fs.readFileSync(__dirname+'/index.html').toString()


const mailConfig = {
  attachments: [
    {
      filename: 'logo.png',
      path: __dirname + '/../logo.png',

      // Uso: <img src="cid:imagencorreo" />
      cid: 'imagencorreo',
    }
  ],
};


// function enviarCorreo(nombre, correo, observacion, url) {
function enviarCorreo(datos) {
  return new Promise((resolve, reject) => {
    const data = {
      to: datos.correo,
      subject: 'Seguimiento del envío | Enviaxa',
      html: render(tmpl, {datos})
    };

    if (mailConfig.attachments)
      data.attachments = mailConfig.attachments;

    return resolve(sendMail(data));
  });
}


module.exports = enviarCorreo;
