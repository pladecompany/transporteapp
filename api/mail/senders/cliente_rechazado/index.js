const fs = require('fs');
const sendMail = require('../../send');
const render = require('../../../functions/renderHTML');

const tmpl = fs.readFileSync(__dirname+'/index.html').toString()


const mailConfig = {
  /*
  attachments: [
    {
      filename: 'imagen.png',
      path: __dirname + '/imagen.png',
      cid: 'imagen',
    }
  ],
*/
};


function enviarRechazoCliente(nombre, correo, motivo) {
  const data = {
    to: correo,
    subject: 'Registro rechazado | Enviaxa',
    html: render(tmpl, {nombre, motivo})
  };

  if (mailConfig.attachments)
    data.attachments = mailConfig.attachments;

  return sendMail(data);
}


module.exports = enviarRechazoCliente;
