const fs = require('fs');
const sendMail = require('../../send');
const render = require('../../../functions/renderHTML');
// const mailConfig = require('./static/recuperar_clave/config');

const tmpl = fs.readFileSync(__dirname+'/index.html').toString()


const mailConfig = {
  attachments: [
    {
      filename: 'logo.png',
      path: __dirname + '/../logo.png',

      // Uso: <img src="cid:imagencorreo" />
      cid: 'imagencorreo',
    }
  ],
};


function enviarRecuperacion(nombre, correo, pass) {
  const data = {
    to: correo,
    subject: 'Recuperación de contraseña | Enviaxa',
    html: render(tmpl, {nombre, pass})
  };

  if (mailConfig.attachments)
    data.attachments = mailConfig.attachments;

  return sendMail(data);
}


module.exports = enviarRecuperacion;
