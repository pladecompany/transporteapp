const fs = require('fs');
const sendMail = require('../../send');
const render = require('../../../functions/renderHTML');
// const mailConfig = require('./static/recuperar_clave/config');

const tmpl = fs.readFileSync(__dirname+'/index.html').toString()


const mailConfig = {
  attachments: [
    {
      filename: 'logo.png',
      path: __dirname + '/../logo.png',

      // Uso: <img src="cid:imagencorreo" />
      cid: 'imagencorreo',
    }
  ],
};


function enviarCorreo(nombre, correo) {
  const data = {
    to: correo,
    subject: 'Registro exitoso | Enviaxa',
    html: render(tmpl, {nombre})
  };

  if (mailConfig.attachments)
    data.attachments = mailConfig.attachments;

  return sendMail(data);
}


module.exports = enviarCorreo;
