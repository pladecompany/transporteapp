const fs = require('fs');
const sendMail = require('../../send');
const render = require('../../../functions/renderHTML');

const tmpl = fs.readFileSync(__dirname+'/index.html').toString()


const mailConfig = {
  /*
  attachments: [
    {
      filename: 'imagen.png',
      path: __dirname + '/imagen.png',
      cid: 'imagen',
    }
  ],
*/
};


function enviarAprobacionCliente(nombre, correo) {
  const data = {
    to: correo,
    subject: 'Registro aprobado | Enviaxa',
    html: render(tmpl, {nombre})
  };

  if (mailConfig.attachments)
    data.attachments = mailConfig.attachments;

  return sendMail(data);
}


module.exports = enviarAprobacionCliente;
