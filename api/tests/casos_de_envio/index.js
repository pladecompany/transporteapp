process.env.TEST = true;


const normal_nacional = require('./casos/normales/nacional');
const normal_españa_internacional = require('./casos/normales/españa-internacional');
const normal_internacional_españa = require('./casos/normales/internacional-españa');

const especial_nacional = require('./casos/especiales/nacional');
const especial_españa_internacional = require('./casos/especiales/españa-internacional');
// const normal_internacional_españa = require('./casos/normales/internacional-españa');

const no_nacional = require('./casos/irrealizables/nacional');
// const no_españa_internacional = require('./casos/irrealizables/españa-internacional');
const no_internacional_españa = require('./casos/irrealizables/internacional-españa');


describe('CASOS NORMALES', function() {
  describe('ESPAÑA -> ESPAÑA', normal_nacional.bind(this));
  describe('ESPAÑA -> INTERNACIONAL', normal_españa_internacional.bind(this));
  describe('INTERNACIONAL -> ESPAÑA', normal_internacional_españa.bind(this));
});


describe('CASOS ESPECIALES', function() {
  describe('ESPAÑA -> ESPAÑA', especial_nacional.bind(this));
  describe('ESPAÑA -> INTERNACIONAL', especial_españa_internacional.bind(this));
  // describe('INTERNACIONAL -> ESPAÑA', especial_internacional_españa.bind(this));
});


describe('ENVIOS NO REALIZABLES', function() {
  describe('ESPAÑA -> ESPAÑA', no_nacional.bind(this));
  // describe('ESPAÑA -> INTERNACIONAL', no_españa_internacional.bind(this));
  describe('INTERNACIONAL -> ESPAÑA', no_internacional_españa.bind(this));
});