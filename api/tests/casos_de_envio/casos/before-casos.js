/*
 * Preparar entorno para los tests
 */
process.env.TEST = true;
const app = require('../../../app');
const request = require('supertest');
const chai = require('chai');
const assert = chai.assert;


before(function(){
  this.app = app;
  this.chai = chai;
  this.assert = chai.assert;


  this.post = function post(ruta, body, expected_code=200) {
    return request(app)
    .post(ruta)
    .send(body)
    .expect(expected_code);
  }

});