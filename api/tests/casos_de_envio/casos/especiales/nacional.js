const PR = require('../../../../data/id_provincias');
const PA = require('../../../../data/id_paises');


function suite()
{


  it('PENÍNSULA -> BALEARES MALLORCA', function(listo) {
    this.post('/envio/obtener-tipo', {
      pais1: PA.ESPAÑA,
      pais2: PA.ESPAÑA,
      provincia1: PR.TERUEL,
      provincia2: PR.BALEARES_MALLORCA
    })
    .then(res => {
      const data = res.body;
      this.assert.typeOf(data.zona, 'object', 'Deberia regresar la zona');
      this.assert.typeOf(data.zona.nombre, 'string', 'Deberia tener el nombre de zona');
      this.assert.include(data.zona.nombre.toUpperCase(), 'BALEARES', 'Deberia ser Baleares Mallorca');
      this.assert.include(data.zona.nombre.toUpperCase(), 'MALLORCA', 'Deberia ser Baleares Mallorca');
      listo();
    })
    .catch(listo);
  });

  it('PENÍNSULA -> BALEARES RESTO DE ISLAS', function(listo) {
    this.post('/envio/obtener-tipo', {
      pais1: PA.ESPAÑA,
      pais2: PA.ESPAÑA,
      provincia1: PR.LUGO,
      provincia2: PR.BALEARES_RESTO_DE_ISLAS
    })
    .then(res => {
      const data = res.body;
      this.assert.typeOf(data.zona, 'object', 'Deberia regresar la zona');
      this.assert.typeOf(data.zona.nombre, 'string', 'Deberia tener el nombre de zona');
      this.assert.include(data.zona.nombre.toUpperCase(), 'BALEARES', 'Deberia ser Baleares Resto de Islas');
      this.assert.include(data.zona.nombre.toUpperCase(), 'RESTO', 'Deberia ser Baleares Resto de Islas');
      this.assert.include(data.zona.nombre.toUpperCase(), 'ISLA', 'Deberia ser Baleares Resto de Islas');
      listo();
    })
    .catch(listo);
  });


  it('PENÍNSULA -> GRAN CANARIA (LAS PALMAS)', function(listo) {
    this.post('/envio/obtener-tipo', {
      pais1: PA.ESPAÑA,
      pais2: PA.ESPAÑA,
      provincia1: PR.LUGO,
      provincia2: PR.LAS_PALMAS
    })
    .then(res => {
      const data = res.body;
      this.assert.typeOf(data.zona, 'object', 'Deberia regresar la zona');
      this.assert.typeOf(data.zona.nombre, 'string', 'Deberia tener el nombre de zona');
      this.assert.include(data.zona.nombre.toUpperCase(), 'CANARIA', 'Deberia ser Canarias');
      listo();
    })
    .catch(listo);
  });

  it('PENÍNSULA -> GRAN CANARIA (SANTA CRUZ DE TENERIFE)', function(listo) {
    this.post('/envio/obtener-tipo', {
      pais1: PA.ESPAÑA,
      pais2: PA.ESPAÑA,
      provincia1: PR.LUGO,
      provincia2: PR.SANTA_CRUZ_DE_TENERIFE
    })
    .then(res => {
      const data = res.body;
      this.assert.typeOf(data.zona, 'object', 'Deberia regresar la zona');
      this.assert.typeOf(data.zona.nombre, 'string', 'Deberia tener el nombre de zona');
      this.assert.include(data.zona.nombre.toUpperCase(), 'CANARIA', 'Deberia ser Canarias');
      listo();
    })
    .catch(listo);
  });


  it('PENÍNSULA -> CEUTA', function(listo) {
    this.post('/envio/obtener-tipo', {
      pais1: PA.ESPAÑA,
      pais2: PA.ESPAÑA,
      provincia1: PR.LUGO,
      provincia2: PR.CEUTA
    })
    .then(res => {
      const data = res.body;
      this.assert.typeOf(data.zona, 'object', 'Deberia regresar la zona');
      this.assert.typeOf(data.zona.nombre, 'string', 'Deberia tener el nombre de zona');
      this.assert.include(data.zona.nombre.toUpperCase(), 'CEUTA', 'Deberia ser Ceuta');
      listo();
    })
    .catch(listo);
  });

  it('PENÍNSULA -> MELILLA', function(listo) {
    this.post('/envio/obtener-tipo', {
      pais1: PA.ESPAÑA,
      pais2: PA.ESPAÑA,
      provincia1: PR.LUGO,
      provincia2: PR.CEUTA
    })
    .then(res => {
      const data = res.body;
      this.assert.typeOf(data.zona, 'object', 'Deberia regresar la zona');
      this.assert.typeOf(data.zona.nombre, 'string', 'Deberia tener el nombre de zona');
      this.assert.include(data.zona.nombre.toUpperCase(), 'MELILLA', 'Deberia ser Melilla');
      listo();
    })
    .catch(listo);
  });

  it('BALEARES MALLORCA -> PENÍNSULA', function(listo) {
    this.post('/envio/obtener-tipo', {
      pais1: PA.ESPAÑA,
      pais2: PA.ESPAÑA,
      provincia1: PR.BALEARES_MALLORCA,
      provincia2: PR.LUGO
    })
    .then(res => {
      const data = res.body;
      this.assert.typeOf(data.params, 'object', 'Deberia contener objeto con parametros');
      this.assert.equal(data.params.franja_adicional, 1, 'Deberia aumentar una franja en los precios');
      // this.assert.fail('¿QUE ZONA DEBERIA SER?');
      // this.assert.typeOf(data.zona, 'object', 'Deberia regresar la zona');
      // this.assert.typeOf(data.zona.nombre, 'string', 'Deberia tener el nombre de zona');
      // this.assert.include(data.zona.nombre.toUpperCase(), 'MELILLA', 'Deberia ser Melilla');
      listo();
    })
    .catch(listo);
  });

  it('BALEARES RESTO DE ISLAS -> PENÍNSULA', function(listo) {
    this.post('/envio/obtener-tipo', {
      pais1: PA.ESPAÑA,
      pais2: PA.ESPAÑA,
      provincia1: PR.BALEARES_RESTO_DE_ISLAS,
      provincia2: PR.LUGO
    })
    .then(res => {
      const data = res.body;
      this.assert.typeOf(data.params, 'object', 'Deberia contener objeto con parametros');
      this.assert.equal(data.params.franja_adicional, 1, 'Deberia aumentar una franja en los precios');
      // this.assert.fail('¿QUE ZONA DEBERIA SER?');
      // this.assert.typeOf(data.zona, 'object', 'Deberia regresar la zona');
      // this.assert.typeOf(data.zona.nombre, 'string', 'Deberia tener el nombre de zona');
      // this.assert.include(data.zona.nombre.toUpperCase(), 'MELILLA', 'Deberia ser Melilla');
      listo();
    })
    .catch(listo);
  });




}

module.exports = suite;
