const PR = require('../../../../data/id_provincias');
const PA = require('../../../../data/id_paises');


function suite()
{

  it('PORTUGAL PENÍNSULA', function(listo) {
    this.post('/envio/obtener-tipo', {
      pais1: PA.ESPAÑA,
      pais2: PA.PORTUGAL_PENINSULA,
      provincia1: PR.LUGO,
    })
    .then(res => {
      const data = res.body;
      this.assert.typeOf(data.zona, 'object', 'Deberia regresar la zona');
      this.assert.typeOf(data.zona.nombre, 'string', 'Deberia tener el nombre de zona');
      this.assert.include(data.zona.nombre.toUpperCase(), 'PORTUGAL', 'Deberia ser Portugal');
      listo();
    })
    .catch(listo);
  });

  it('PORTUGAL ISLAS', function(listo) {
    this.post('/envio/obtener-tipo', {
      pais1: PA.ESPAÑA,
      pais2: PA.PORTUGAL_ISLAS,
      provincia1: PR.LUGO,
    })
    .then(res => {
      const data = res.body;
      this.assert.typeOf(data.zona, 'object', 'Deberia regresar la zona');
      this.assert.typeOf(data.zona.nombre, 'string', 'Deberia tener el nombre de zona');
      this.assert.include(data.zona.nombre.toUpperCase(), 'PORTUGAL', 'Deberia ser Portugal');
      listo();
    })
    .catch(listo);
  });





}

module.exports = suite;
