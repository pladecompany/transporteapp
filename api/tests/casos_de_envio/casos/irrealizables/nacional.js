const PR = require('../../../../data/id_provincias');
const PA = require('../../../../data/id_paises');


function suite()
{

  it('CEUTA -> BALEARES MALLORCA', function(listo){
    this.post('/envio/obtener-tipo', {
      pais1: PA.ESPAÑA,
      pais2: PA.ESPAÑA,
      provincia1: PR.CEUTA,
      provincia2: PR.BALEARES_MALLORCA
    })
    .then(res => {
      this.assert.isFalse(res.body.zona, 'Zona deberia ser false');
      listo();
    })
    .catch(listo);
  });

  it('CEUTA -> BALEARES RESTO DE ISLAS', function(listo){
    this.post('/envio/obtener-tipo', {
      pais1: PA.ESPAÑA,
      pais2: PA.ESPAÑA,
      provincia1: PR.CEUTA,
      provincia2: PR.BALEARES_RESTO_DE_ISLAS
    })
    .then(res => {
      this.assert.isFalse(res.body.zona, 'Zona deberia ser false');
      listo();
    })
    .catch(listo);
  });

  it('CEUTA -> GRAN CANARIA (SANTA CRUZ DE TENERIFE)', function(listo){
    this.post('/envio/obtener-tipo', {
      pais1: PA.ESPAÑA,
      pais2: PA.ESPAÑA,
      provincia1: PR.CEUTA,
      provincia2: PR.SANTA_CRUZ_DE_TENERIFE
    })
    .then(res => {
      this.assert.isFalse(res.body.zona, 'Zona deberia ser false');
      listo();
    })
    .catch(listo);
  });

  it('CEUTA -> GRAN CANARIA (LAS PALMAS)', function(listo){
    this.post('/envio/obtener-tipo', {
      pais1: PA.ESPAÑA,
      pais2: PA.ESPAÑA,
      provincia1: PR.CEUTA,
      provincia2: PR.LAS_PALMAS
    })
    .then(res => {
      this.assert.isFalse(res.body.zona, 'Zona deberia ser false');
      listo();
    })
    .catch(listo);
  });

  it('CEUTA -> CANARIAS RESTO DE ISLAS', function(listo){
    this.post('/envio/obtener-tipo', {
      pais1: PA.ESPAÑA,
      pais2: PA.ESPAÑA,
      provincia1: PR.CEUTA,
      provincia2: PR.CANARIAS_RESTO_DE_ISLAS
    })
    .then(res => {
      this.assert.isFalse(res.body.zona, 'Zona deberia ser false');
      listo();
    })
    .catch(listo);
  });



  it('MELILLA -> BALEARES MALLORCA', function(listo){
    this.post('/envio/obtener-tipo', {
      pais1: PA.ESPAÑA,
      pais2: PA.ESPAÑA,
      provincia1: PR.MELILLA,
      provincia2: PR.BALEARES_MALLORCA
    })
    .then(res => {
      this.assert.isFalse(res.body.zona, 'Zona deberia ser false');
      listo();
    })
    .catch(listo);
  });

  it('MELILLA -> BALEARES RESTO DE ISLAS', function(listo){
    this.post('/envio/obtener-tipo', {
      pais1: PA.ESPAÑA,
      pais2: PA.ESPAÑA,
      provincia1: PR.MELILLA,
      provincia2: PR.BALEARES_RESTO_DE_ISLAS
    })
    .then(res => {
      this.assert.isFalse(res.body.zona, 'Zona deberia ser false');
      listo();
    })
    .catch(listo);
  });

  it('MELILLA -> GRAN CANARIA (SANTA CRUZ DE TENERIFE)', function(listo){
    this.post('/envio/obtener-tipo', {
      pais1: PA.ESPAÑA,
      pais2: PA.ESPAÑA,
      provincia1: PR.MELILLA,
      provincia2: PR.SANTA_CRUZ_DE_TENERIFE
    })
    .then(res => {
      this.assert.isFalse(res.body.zona, 'Zona deberia ser false');
      listo();
    })
    .catch(listo);
  });

  it('MELILLA -> GRAN CANARIA (LAS PALMAS)', function(listo){
    this.post('/envio/obtener-tipo', {
      pais1: PA.ESPAÑA,
      pais2: PA.ESPAÑA,
      provincia1: PR.MELILLA,
      provincia2: PR.LAS_PALMAS
    })
    .then(res => {
      this.assert.isFalse(res.body.zona, 'Zona deberia ser false');
      listo();
    })
    .catch(listo);
  });

  it('MELILLA -> CANARIAS RESTO DE ISLAS', function(listo){
    this.post('/envio/obtener-tipo', {
      pais1: PA.ESPAÑA,
      pais2: PA.ESPAÑA,
      provincia1: PR.MELILLA,
      provincia2: PR.CANARIAS_RESTO_DE_ISLAS
    })
    .then(res => {
      this.assert.isFalse(res.body.zona, 'Zona deberia ser false');
      listo();
    })
    .catch(listo);
  });



}

module.exports = suite;
