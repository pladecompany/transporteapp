const PR = require('../../../../data/id_provincias');
const PA = require('../../../../data/id_paises');


function suite()
{

  it('INTERNACIONAL -> BALEARES MALLORCA', function(listo) {
    this.post('/envio/obtener-tipo', {
      pais1: PA.ALEMANIA,
      pais2: PA.ESPAÑA,
      provincia2: PR.BALEARES_MALLORCA,
    })
    .then(res => {
      this.assert.isFalse(res.body.zona, 'Zona deberia ser false');
      listo();
    })
    .catch(listo);
  });

  it('INTERNACIONAL -> BALEARES RESTO DE ISLAS', function(listo) {
    this.post('/envio/obtener-tipo', {
      pais1: PA.ALEMANIA,
      pais2: PA.ESPAÑA,
      provincia2: PR.BALEARES_RESTO_DE_ISLAS,
    })
    .then(res => {
      this.assert.isFalse(res.body.zona, 'Zona deberia ser false');
      listo();
    })
    .catch(listo);
  });


  it('INTERNACIONAL -> GRAN CANARIA (SANTA CRUZ DE TENERIFE)', function(listo) {
    this.post('/envio/obtener-tipo', {
      pais1: PA.ALEMANIA,
      pais2: PA.ESPAÑA,
      provincia2: PR.SANTA_CRUZ_DE_TENERIFE,
    })
    .then(res => {
      this.assert.isFalse(res.body.zona, 'Zona deberia ser false');
      listo();
    })
    .catch(listo);
  });

  it('INTERNACIONAL -> GRAN CANARIA (LAS PALMAS)', function(listo) {
    this.post('/envio/obtener-tipo', {
      pais1: PA.ALEMANIA,
      pais2: PA.ESPAÑA,
      provincia2: PR.LAS_PALMAS,
    })
    .then(res => {
      this.assert.isFalse(res.body.zona, 'Zona deberia ser false');
      listo();
    })
    .catch(listo);
  });

  it('INTERNACIONAL -> CANARIAS RESTO DE ISLAS', function(listo) {
    this.post('/envio/obtener-tipo', {
      pais1: PA.ALEMANIA,
      pais2: PA.ESPAÑA,
      provincia2: PR.CANARIAS_RESTO_DE_ISLAS,
    })
    .then(res => {
      this.assert.isFalse(res.body.zona, 'Zona deberia ser false');
      listo();
    })
    .catch(listo);
  });

  it('INTERNACIONAL -> CEUTA', function(listo) {
    this.post('/envio/obtener-tipo', {
      pais1: PA.ALEMANIA,
      pais2: PA.ESPAÑA,
      provincia2: PR.CEUTA,
    })
    .then(res => {
      this.assert.isFalse(res.body.zona, 'Zona deberia ser false');
      listo();
    })
    .catch(listo);
  });

  it('INTERNACIONAL -> MELILLA', function(listo) {
    this.post('/envio/obtener-tipo', {
      pais1: PA.ALEMANIA,
      pais2: PA.ESPAÑA,
      provincia2: PR.MELILLA,
    })
    .then(res => {
      this.assert.isFalse(res.body.zona, 'Zona deberia ser false');
      listo();
    })
    .catch(listo);
  });





}

module.exports = suite;
