const PR = require('../../../../data/id_provincias');
const PA = require('../../../../data/id_paises');


function suite()
{

  it('PROVINCIAL (TERUEL -> TERUEL)', function(listo){
    this.post('/envio/obtener-tipo', {
      pais1: PA.ESPAÑA,
      pais2: PA.ESPAÑA,
      provincia1: PR.TERUEL,
      provincia2: PR.TERUEL
    })
    .then(res => {
      const data = res.body;
      this.assert.typeOf(data.zona, 'object', 'Deberia regresar la zona');
      this.assert.typeOf(data.zona.tipo, 'string', 'Deberia tener el tipo de zona');
      this.assert.include(data.zona.tipo.toUpperCase(), 'PROVINCIAL', 'Deberia ser provincial');
      listo();
    })
    .catch(listo);
  });

  it('REGIONAL (LUGO -> PONTEVEDRA)', function(listo){
    this.post('/envio/obtener-tipo', {
      pais1: PA.ESPAÑA,
      pais2: PA.ESPAÑA,
      provincia1: PR.LUGO,
      provincia2: PR.PONTEVEDRA
    })
    .then(res => {
      const data = res.body;
      this.assert.typeOf(data.zona, 'object', 'Deberia regresar la zona');
      this.assert.typeOf(data.zona.tipo, 'string', 'Deberia tener el tipo de zona');
      this.assert.include(data.zona.tipo.toUpperCase(), 'REGIONAL', 'Deberia ser regional');
    })
    .finally(listo);
  });

  it('PENINSULAR (LUGO -> ZARAGOZA)', function(listo){
    this.post('/envio/obtener-tipo', {
      pais1: PA.ESPAÑA,
      pais2: PA.ESPAÑA,
      provincia1: PR.LUGO,
      provincia2: PR.ZARAGOZA
    })
    .then(res => {
      const data = res.body;
      this.assert.typeOf(data.zona, 'object', 'Deberia regresar la zona');
      this.assert.typeOf(data.zona.nombre, 'string', 'Deberia tener el nombre de zona');
      this.assert.include(data.zona.nombre.toUpperCase(), 'PENINSULAR', 'Deberia ser peninsular');
      listo();
    })
    .catch(listo);
  });


}

module.exports = suite;
