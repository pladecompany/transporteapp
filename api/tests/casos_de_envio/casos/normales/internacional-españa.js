const PR = require('../../../../data/id_provincias');
const PA = require('../../../../data/id_paises');


function suite()
{

  it('ALEMANIA -> ESPAÑA-TERUEL', function(listo){
    this.post('/envio/obtener-tipo', {
      pais1: PA.ALEMANIA,
      pais2: PA.ESPAÑA,
      provincia2: PR.TERUEL,
    })
    .then(res => {
      const data = res.body;
      this.assert.typeOf(data.zona, 'object', 'Deberia regresar la zona');
      this.assert.typeOf(data.zona.tipo, 'string', 'Deberia tener el tipo de zona');
      this.assert.include(data.zona.tipo.toUpperCase(), 'INTERNACIONAL', 'Deberia ser internacional');
      listo();
    })
    .catch(listo);
  });



}

module.exports = suite;
