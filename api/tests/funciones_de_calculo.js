process.env.TEST = true;
const chai = require('chai');
const assert = chai.assert;


// Datos de prueba:

const pesos = [
  { desde:0, hasta:2, costo:6},
  { desde:2.1, hasta:5, costo:7},
  { desde:5.1, hasta:10, costo:8},
];
const pesos2 = [
  { desde:0, hasta:2, costo:6},
  { desde:2.1, hasta:5, costo:7},
  { desde:5.1, hasta:10, costo:8},
  { desde:10.1, hasta:15, costo:9},
];

const bultos = [
  { fondo:4, ancho:15, alto:60, peso:1.5},
  { fondo:40, ancho:65, alto:89, peso:7},
];
const tarifa_adicional = 0.5;
const coeficiente = 5000;



const indicePesoMaximo = require('../functions/calculos/indicePesoMaximo');
const pesoEnRango = require('../functions/calculos/pesoEnRango');
const costoPorPeso = require('../functions/calculos/costoPorPeso');
const costoDeBultos = require('../functions/calculos/costoDeBultos');
const totalEnvio = require('../functions/calculos/totalEnvio');


describe('FUNCIONES DE CÁLCULO', function() {

  describe('indicePesoMaximo', function() {
    it('Deberia ser 2', () => assert.equal(indicePesoMaximo(pesos), 2));
    it('Deberia ser 3', () => assert.equal(indicePesoMaximo(pesos2), 3));
  });


  describe('pesoEnRango', function() {
    it('Deberia ser true', () => assert.isTrue( pesoEnRango(1, pesos) ));
    it('Deberia ser true', () => assert.isTrue( pesoEnRango(10, pesos) ));
    it('Deberia ser false', () => assert.isFalse( pesoEnRango(10, pesos, true) ));
    it('Deberia ser false', () => assert.isFalse( pesoEnRango(11, pesos) ));
  });


  describe('costoPorPeso', function() {
    it('Deberia ser 26.5', () => assert.equal( costoPorPeso(46.28, pesos, tarifa_adicional), 26.5 ));
    it('Deberia ser 27', () => assert.equal( costoPorPeso(46.28, pesos, tarifa_adicional, true), 27 ));
  });


  describe('costoDeBultos', function() {
    it('Deberia ser 32.5', () => assert.equal( costoDeBultos(bultos, pesos, tarifa_adicional, coeficiente), 32.5 ));
  });


  describe('totalEnvio', function() {
    it('Deberia ser 51.43  +/- 0.01', function() {
      const costo_bultos = costoDeBultos(bultos, pesos, tarifa_adicional, coeficiente);
      const total_envio = totalEnvio({ bultos: costo_bultos, modalidad: 10 });
      assert.closeTo(
        total_envio,
        51.43, // Monto esperado
        0.01 // +/- Margen de error
      );
    });
  });

});
