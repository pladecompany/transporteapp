const hashPass = require('../functions/hashPass');

exports.seed = async function(knex, Promise) {
  console.log("Running seed...")

  x = await knex("aduana").del();
  x = await knex("pesos_zonas").del();
  x = await knex("zona").del();
  x = await knex("admin").del();
  x = await knex("pais").del();
  x = await knex("region").del();
  x = await knex("provincia").del();
  x = await knex("modalidad").del();

  //admin por defecto
  await knex("admin").insert({
    id: 1,
    nombre: "admin",
    correo: "admin@admin.com",
    pass: await hashPass('12345'),
  });

  await knex("zona").insert({
    id: 1,
    nombre: "PROVINCIAL",
    tipo: "Nacional/Provincial",
    envio: "Ambos",
    tarifa_adicional: 0.50,
    coeficiente_registrado: 5000,
    coeficiente_no_registrado: 5000,
    coeficiente_registrado_lvl_1: 4000
  });

  await knex("zona").insert({
    id: 2,
    nombre: "REGIONAL",
    tipo: "Nacional/Regional",
    envio: "Ambos",
    tarifa_adicional: 0.60,
    coeficiente_registrado: 5000,
    coeficiente_no_registrado: 5000,
    coeficiente_registrado_lvl_1: 4000
  });

  await knex("zona").insert({
    id: 3,
    nombre: "PENINSULAR",
    tipo: "Nacional",
    envio: "Ambos",
    tarifa_adicional: 0.70,
    coeficiente_registrado: 5000,
    coeficiente_no_registrado: 5000,
    coeficiente_registrado_lvl_1: 4000
  });

  await knex("zona").insert({
    id: 4,
    nombre: "BALEARES MALLORCA",
    tipo: "Nacional",
    envio: "Ambos",
    tarifa_adicional: 0.90,
    coeficiente_registrado: 5000,
    coeficiente_no_registrado: 5000,
    coeficiente_registrado_lvl_1: 4000
  });

  await knex("zona").insert({
    id: 5,
    nombre: "BALEARES RESTO DE ISLAS",
    tipo: "Nacional",
    envio: "Ambos",
    tarifa_adicional: 1.25,
    coeficiente_registrado: 5000,
    coeficiente_no_registrado: 5000,
    coeficiente_registrado_lvl_1: 4000
  });

  await knex("zona").insert({
    id: 6,
    nombre: "CANARIAS GRAN CANARIA",
    tipo: "Nacional",
    envio: "Destino",
    tarifa_adicional: 1.50,
    coeficiente_registrado: 2500,
    coeficiente_no_registrado: 2500,
    coeficiente_registrado_lvl_1: 2500
  });

  await knex("zona").insert({
    id: 7,
    nombre: "CANARIAS RESTO DE ISLAS",
    tipo: "Nacional",
    envio: "Destino",
    tarifa_adicional: 1.50,
    coeficiente_registrado: 2500,
    coeficiente_no_registrado: 2500,
    coeficiente_registrado_lvl_1: 2500
  });

  await knex("zona").insert({
    id: 8,
    nombre: "CEUTA Y MELILLA",
    tipo: "Nacional",
    envio: "Destino",
    tarifa_adicional: 2.50,
    coeficiente_registrado: 5000,
    coeficiente_no_registrado: 5000,
    coeficiente_registrado_lvl_1: 4000
  });

  await knex("zona").insert({
    id: 9,
    nombre: "ZONA 0 PORTUGAL PENÍNSULA",
    tipo: "Internacional",
    envio: "Ambos",
    tarifa_adicional: 1.35,
    coeficiente_registrado: 5000,
    coeficiente_no_registrado: 5000,
    coeficiente_registrado_lvl_1: 4000
  });

  await knex("zona").insert({
    id: 10,
    nombre: "ZONA 0 PORTUGAL ISLAS",
    tipo: "Internacional",
    envio: "Ambos",
    tarifa_adicional: 5.55,
    coeficiente_registrado: 5000,
    coeficiente_no_registrado: 5000,
    coeficiente_registrado_lvl_1: 4000
  });

  await knex("zona").insert({
    id: 11,
    nombre: "INTERNACIONAL ZONA 1",
    tipo: "Internacional",
    envio: "Ambos",
    coeficiente_registrado: 5000,
    coeficiente_no_registrado: 5000,
    coeficiente_registrado_lvl_1: 4000
  });

  await knex("zona").insert({
    id: 12,
    nombre: "INTERNACIONAL ZONA 2",
    tipo: "Internacional",
    envio: "Ambos",
    coeficiente_registrado: 5000,
    coeficiente_no_registrado: 5000,
    coeficiente_registrado_lvl_1: 4000
  });

  await knex("zona").insert({
    id: 13,
    nombre: "INTERNACIONAL ZONA 3",
    tipo: "Internacional",
    envio: "Ambos",
    coeficiente_registrado: 5000,
    coeficiente_no_registrado: 5000,
    coeficiente_registrado_lvl_1: 4000
  });

  await knex("zona").insert({
    id: 14,
    nombre: "INTERNACIONAL ZONA 4",
    tipo: "Internacional",
    envio: "Ambos",
    coeficiente_registrado: 5000,
    coeficiente_no_registrado: 5000,
    coeficiente_registrado_lvl_1: 4000
  });

  await knex("zona").insert({
    id: 15,
    nombre: "INTERNACIONAL ZONA 5",
    tipo: "Internacional",
    envio: "Ambos",
    coeficiente_registrado: 5000,
    coeficiente_no_registrado: 5000,
    coeficiente_registrado_lvl_1: 4000
  });

  await knex("aduana").insert({
    id: 1,
    importacion:0,
    dua:0,
    dua_exportacion:0,
    id_zona:1
  });
  await knex("aduana").insert({
    id: 2,
    importacion:0,
    dua:0,
    dua_exportacion:0,
    id_zona:2
  });
  await knex("aduana").insert({
    id: 3,
    importacion:0,
    dua:0,
    dua_exportacion:0,
    id_zona:3
  });
  await knex("aduana").insert({
    id: 4,
    importacion:0,
    dua:0,
    dua_exportacion:0,
    id_zona:4
  });
  await knex("aduana").insert({
    id: 5,
    importacion:0,
    dua:0,
    dua_exportacion:0,
    id_zona:5
  });
  await knex("aduana").insert({
    id: 6,
    importacion:0,
    dua:19,
    dua_exportacion:19,
    id_zona:6
  });
  await knex("aduana").insert({
    id: 7,
    importacion:0,
    dua:19,
    dua_exportacion:19,
    id_zona:7
  });
  await knex("aduana").insert({
    id: 8,
    importacion:0,
    dua:25,
    dua_exportacion:0,
    id_zona:8
  });
  await knex("aduana").insert({
    id: 9,
    importacion:0,
    dua:0,
    dua_exportacion:0,
    id_zona:9
  });
  await knex("aduana").insert({
    id: 10,
    importacion:0,
    dua:0,
    dua_exportacion:0,
    id_zona:10
  });
  await knex("aduana").insert({
    id: 11,
    importacion:15,
    dua:0,
    dua_exportacion:0,
    id_zona:11
  });
  await knex("aduana").insert({
    id: 12,
    importacion:15,
    dua:0,
    dua_exportacion:0,
    id_zona:12
  });
  await knex("aduana").insert({
    id: 13,
    importacion:15,
    dua:0,
    dua_exportacion:0,
    id_zona:13
  });
  await knex("aduana").insert({
    id: 14,
    importacion:15,
    dua:0,
    dua_exportacion:0,
    id_zona:14
  });
  await knex("aduana").insert({
    id: 15,
    importacion:15,
    dua:0,
    dua_exportacion:0,
    id_zona:15
  });

  await knex("pesos_zonas").insert({
    id: 1,
    desde:0,
    hasta:2,
    costo:6,
    id_zona:1
  });
  await knex("pesos_zonas").insert({
    id: 2,
    desde:2.01,
    hasta:5,
    costo:7,
    id_zona:1
  });
  await knex("pesos_zonas").insert({
    id: 3,
    desde:5.01,
    hasta:10,
    costo:8,
    id_zona:1
  });
  await knex("pesos_zonas").insert({
    id: 4,
    desde:0,
    hasta:2,
    costo:7.25,
    id_zona:2
  });
  await knex("pesos_zonas").insert({
    id: 5,
    desde:2.01,
    hasta:5,
    costo:9,
    id_zona:2
  });
  await knex("pesos_zonas").insert({
    id: 6,
    desde:5.01,
    hasta:10,
    costo:10.5,
    id_zona:2
  });
  await knex("pesos_zonas").insert({
    id: 7,
    desde:0,
    hasta:2,
    costo:8,
    id_zona:3
  });
  await knex("pesos_zonas").insert({
    id: 8,
    desde:2.01,
    hasta:5,
    costo:10.6,
    id_zona:3
  });
  await knex("pesos_zonas").insert({
    id: 9,
    desde:5.01,
    hasta:10,
    costo:12,
    id_zona:3
  });
  await knex("pesos_zonas").insert({
    id: 10,
    desde:0,
    hasta:1,
    costo:9.25,
    id_zona:4
  });
  await knex("pesos_zonas").insert({
    id: 11,
    desde:1.01,
    hasta:5,
    costo:11.85,
    id_zona:4
  });
  await knex("pesos_zonas").insert({
    id: 12,
    desde:5.01,
    hasta:10,
    costo:15.45,
    id_zona:4
  });
  await knex("pesos_zonas").insert({
    id: 13,
    desde:10.01,
    hasta:15,
    costo:18.55,
    id_zona:4
  });
  await knex("pesos_zonas").insert({
    id: 14,
    desde:0,
    hasta:1,
    costo:11.55,
    id_zona:5
  });
  await knex("pesos_zonas").insert({
    id: 15,
    desde:1.01,
    hasta:5,
    costo:15.45,
    id_zona:5
  });
  await knex("pesos_zonas").insert({
    id: 16,
    desde:5.01,
    hasta:10,
    costo:18.55,
    id_zona:5
  });
  await knex("pesos_zonas").insert({
    id: 17,
    desde:10.01,
    hasta:15,
    costo:21.65,
    id_zona:5
  });
  await knex("pesos_zonas").insert({
    id: 18,
    desde:0,
    hasta:1,
    costo:11.35,
    tiempo:'48h a 72h',
    id_zona:6
  });
  await knex("pesos_zonas").insert({
    id: 19,
    desde:1.01,
    hasta:2,
    costo:14.55,
    tiempo:'48h a 72h',
    id_zona:6
  });
  await knex("pesos_zonas").insert({
    id: 20,
    desde:2.01,
    hasta:3,
    costo:18.90,
    tiempo:'48h a 72h',
    id_zona:6
  });
  await knex("pesos_zonas").insert({
    id: 21,
    desde:3.01,
    hasta:4,
    costo:23.75,
    tiempo:'48h a 72h',
    id_zona:6
  });
  await knex("pesos_zonas").insert({
    id: 22,
    desde:4.01,
    hasta:5,
    costo:28.40,
    tiempo:'48h a 72h',
    id_zona:6
  });
  await knex("pesos_zonas").insert({
    id: 23,
    desde:5.01,
    hasta:10,
    costo:32.25,
    tiempo:'8 días',
    id_zona:7
  });
  await knex("pesos_zonas").insert({
    id: 24,
    desde:10.01,
    hasta:15,
    costo:38.95,
    tiempo:'8 días',
    id_zona:7
  });
  await knex("pesos_zonas").insert({
    id: 25,
    desde:0,
    hasta:1,
    costo:19.50,
    id_zona:8
  });
  await knex("pesos_zonas").insert({
    id: 26,
    desde:0,
    hasta:2,
    costo:11.05,
    id_zona:9
  });
  await knex("pesos_zonas").insert({
    id: 27,
    desde:2.01,
    hasta:5,
    costo:15.45,
    id_zona:9
  });
  await knex("pesos_zonas").insert({
    id: 28,
    desde:5.01,
    hasta:10,
    costo:18.55,
    id_zona:9
  });
  await knex("pesos_zonas").insert({
    id: 29,
    desde:10.01,
    hasta:15,
    costo:21.63,
    id_zona:9
  });

  await knex("pesos_zonas").insert({
    id: 30,
    desde:0,
    hasta:2,
    costo:41.95,
    id_zona:10
  });
  await knex("pesos_zonas").insert({
    id: 31,
    desde:2.01,
    hasta:5,
    costo:47.90,
    id_zona:10
  });
  await knex("pesos_zonas").insert({
    id: 32,
    desde:5.01,
    hasta:10,
    costo:56.65,
    id_zona:10
  });
  await knex("pesos_zonas").insert({
    id: 33,
    desde:10.01,
    hasta:15,
    costo:74.65,
    id_zona:10
  });

  await knex("pesos_zonas").insert({
    id: 34,
    desde:0,
    hasta:2,
    costo:20.60,
    id_zona:11
  });
  await knex("pesos_zonas").insert({
    id: 35,
    desde:2.01,
    hasta:5,
    costo:26.10,
    id_zona:11
  });
  await knex("pesos_zonas").insert({
    id: 36,
    desde:5.01,
    hasta:10,
    costo:29.50,
    id_zona:11
  });
  await knex("pesos_zonas").insert({
    id: 37,
    desde:10.01,
    hasta:15,
    costo:32.75,
    id_zona:11
  });
  await knex("pesos_zonas").insert({
    id: 38,
    desde:15.01,
    hasta:20,
    costo:36.50,
    id_zona:11
  });
  await knex("pesos_zonas").insert({
    id: 39,
    desde:20.01,
    hasta:30,
    costo:44.75,
    id_zona:11
  });
  await knex("pesos_zonas").insert({
    id: 40,
    desde:30.01,
    hasta:40,
    costo:55.90,
    id_zona:11
  });

  await knex("pesos_zonas").insert({
    id: 41,
    desde:0,
    hasta:2,
    costo:23.30,
    id_zona:12
  });
  await knex("pesos_zonas").insert({
    id: 42,
    desde:2.01,
    hasta:5,
    costo:29,
    id_zona:12
  });
  await knex("pesos_zonas").insert({
    id: 43,
    desde:5.01,
    hasta:10,
    costo:32.65,
    id_zona:12
  });
  await knex("pesos_zonas").insert({
    id: 44,
    desde:10.01,
    hasta:15,
    costo:37.7,
    id_zona:12
  });
  await knex("pesos_zonas").insert({
    id: 45,
    desde:15.01,
    hasta:20,
    costo:41.5,
    id_zona:12
  });
  await knex("pesos_zonas").insert({
    id: 46,
    desde:20.01,
    hasta:30,
    costo:49.75,
    id_zona:12
  });
  await knex("pesos_zonas").insert({
    id: 47,
    desde:30.01,
    hasta:40,
    costo:61.40,
    id_zona:12
  });

  await knex("pesos_zonas").insert({
    id: 49,
    desde:0,
    hasta:2,
    costo:25.75,
    id_zona:13
  });
  await knex("pesos_zonas").insert({
    id: 50,
    desde:2.01,
    hasta:5,
    costo:32.5,
    id_zona:13
  });
  await knex("pesos_zonas").insert({
    id: 51,
    desde:5.01,
    hasta:10,
    costo:40.5,
    id_zona:13
  });
  await knex("pesos_zonas").insert({
    id: 52,
    desde:10.01,
    hasta:15,
    costo:47.75,
    id_zona:13
  });
  await knex("pesos_zonas").insert({
    id: 53,
    desde:15.01,
    hasta:20,
    costo:52.55,
    id_zona:13
  });
  await knex("pesos_zonas").insert({
    id: 54,
    desde:20.01,
    hasta:30,
    costo:57.85,
    id_zona:13
  });
  await knex("pesos_zonas").insert({
    id: 55,
    desde:30.01,
    hasta:40,
    costo:63.25,
    id_zona:13
  });

  await knex("pesos_zonas").insert({
    id: 56,
    desde:0,
    hasta:2,
    costo:31.75,
    id_zona:14
  });
  await knex("pesos_zonas").insert({
    id: 57,
    desde:2.01,
    hasta:5,
    costo:48.9,
    id_zona:14
  });
  await knex("pesos_zonas").insert({
    id: 58,
    desde:5.01,
    hasta:10,
    costo:54.2,
    id_zona:14
  });
  await knex("pesos_zonas").insert({
    id: 59,
    desde:10.01,
    hasta:15,
    costo:60.4,
    id_zona:14
  });
  await knex("pesos_zonas").insert({
    id: 60,
    desde:15.01,
    hasta:20,
    costo:64.1,
    id_zona:14
  });
  await knex("pesos_zonas").insert({
    id: 61,
    desde:20.01,
    hasta:30,
    costo:69.5,
    id_zona:14
  });
  await knex("pesos_zonas").insert({
    id: 62,
    desde:30.01,
    hasta:40,
    costo:75.75,
    id_zona:14
  });

  await knex("pesos_zonas").insert({
    id: 63,
    desde:0,
    hasta:2,
    costo:51.08,
    id_zona:15
  });
  await knex("pesos_zonas").insert({
    id: 64,
    desde:2.01,
    hasta:5,
    costo:64.37,
    id_zona:15
  });
  await knex("pesos_zonas").insert({
    id: 65,
    desde:5.01,
    hasta:10,
    costo:99.6,
    id_zona:15
  });
  await knex("pesos_zonas").insert({
    id: 66,
    desde:10.01,
    hasta:15,
    costo:144.4,
    id_zona:15
  });
  await knex("pesos_zonas").insert({
    id: 67,
    desde:15.01,
    hasta:20,
    costo:190.96,
    id_zona:15
  });
  await knex("pesos_zonas").insert({
    id: 68,
    desde:20.01,
    hasta:30,
    costo:240.6,
    id_zona:15
  });
  await knex("pesos_zonas").insert({
    id: 69,
    desde:30.01,
    hasta:40,
    costo:311.57,
    id_zona:15
  });
  await knex("pesos_zonas").insert({
    id: 70,
    desde:5.01,
    hasta:10,
    costo:32.25,
    tiempo:'Barco 8 días',
    id_zona:6
  });
  await knex("pesos_zonas").insert({
    id: 71,
    desde:10.01,
    hasta:15,
    costo:38.95,
    tiempo:'Barco 8 días',
    id_zona:6
  });

  await knex("pais").insert({
    id: 1,
    nombre: "España",
    region: true,
    codigo_tlf: '+34',
    importacion: 0
  });

  await knex("pais").insert({
    id: 2,
    nombre: "Portugal península",
    codigo_tlf: '+351',
    id_zona: 9,
    importacion: 0
  });

  await knex("pais").insert({
    id: 3,
    nombre: "Francia",
    codigo_tlf: '+33',
    id_zona: 11,
    importacion: 15
  });

  await knex("pais").insert({
    id: 4,
    nombre: "Alemania",
    codigo_tlf: '+49',
    id_zona: 11,
    importacion: 15
  });

  await knex("pais").insert({
    id: 5,
    nombre: "Monaco",
    extracomunitario: 1,
    codigo_tlf: '+377',
    id_zona: 11,
    importacion: 35
  });

  await knex("pais").insert({
    id: 6,
    nombre: "Austria",
    codigo_tlf: '+43',
    id_zona: 12,
    importacion: 15
  });

  await knex("pais").insert({
    id: 7,
    nombre: "Bélgica",
    codigo_tlf: '+32',
    id_zona: 12,
    importacion: 15
  });

  await knex("pais").insert({
    id: 8,
    nombre: "Italia",
    codigo_tlf: '+39',
    id_zona: 12,
    importacion: 15
  });

  await knex("pais").insert({
    id: 9,
    nombre: "Dinamarca",
    codigo_tlf: '+45',
    id_zona: 12,
    importacion: 15
  });

  await knex("pais").insert({
    id: 10,
    nombre: "Luxemburgo",
    codigo_tlf: '+352',
    id_zona: 12,
    importacion: 15
  });

  await knex("pais").insert({
    id: 11,
    nombre: "Paises Bajos",
    codigo_tlf: '+31',
    id_zona: 12,
    importacion: 15
  });

  await knex("pais").insert({
    id: 12,
    nombre: "Rep. Checa",
    codigo_tlf: '+420',
    id_zona: 12,
    importacion: 15
  });

  await knex("pais").insert({
    id: 13,
    nombre: "San Marino",
    extracomunitario: 1,
    codigo_tlf: '+378',
    id_zona: 12,
    importacion: 35
  });

  await knex("pais").insert({
    id: 14,
    nombre: "C. Vaticano",
    extracomunitario: 1,
    codigo_tlf: '+379',
    id_zona: 12,
    importacion: 35
  });

  await knex("pais").insert({
    id: 15,
    nombre: "Gran Bretaña",
    extracomunitario: 1,
    codigo_tlf: '+44',
    id_zona: 13,
    importacion: 35
  });

  await knex("pais").insert({
    id: 16,
    nombre: "Hungria",
    codigo_tlf: '+36',
    id_zona: 13,
    importacion: 15
  });

  await knex("pais").insert({
    id: 17,
    nombre: "Eslovenia",
    codigo_tlf: '+386',
    id_zona: 13,
    importacion: 15
  });

  await knex("pais").insert({
    id: 18,
    nombre: "Polonia",
    codigo_tlf: '+48',
    id_zona: 13,
    importacion: 15
  });

  await knex("pais").insert({
    id: 19,
    nombre: "Suiza",
    extracomunitario: 1,
    codigo_tlf: '+41',
    id_zona: 13,
    importacion: 35
  });

  await knex("pais").insert({
    id: 20,
    nombre: "Liechtenstein",
    extracomunitario: 1,
    codigo_tlf: '+423',
    id_zona: 13,
    importacion: 35
  });

  await knex("pais").insert({
    id: 21,
    nombre: "Eslovaquia",
    extracomunitario: 1,
    codigo_tlf: '+421',
    id_zona: 13,
    importacion: 15
  });

  await knex("pais").insert({
    id: 22,
    nombre: "Bulgaria",
    codigo_tlf: '+359',
    id_zona: 14,
    importacion: 35
  });

  await knex("pais").insert({
    id: 23,
    nombre: "Estonia",
    codigo_tlf: '+372',
    id_zona: 14,
    importacion: 15
  });

  await knex("pais").insert({
    id: 24,
    nombre: "Finlandia",
    codigo_tlf: '+358',
    id_zona: 14,
    importacion: 15
  });

  await knex("pais").insert({
    id: 25,
    nombre: "Irlanda",
    codigo_tlf: '+353',
    id_zona: 14,
    importacion: 15
  });

  await knex("pais").insert({
    id: 26,
    nombre: "Lituania",
    codigo_tlf: '+370',
    id_zona: 14,
    importacion: 15
  });

  await knex("pais").insert({
    id: 27,
    nombre: "Noruega",
    extracomunitario: 1,
    codigo_tlf: '+47',
    id_zona: 14,
    importacion: 35
  });

  await knex("pais").insert({
    id: 28,
    nombre: "Letonia",
    codigo_tlf: '+371',
    id_zona: 14,
    importacion: 15
  });

  await knex("pais").insert({
    id: 29,
    nombre: "Rumania",
    codigo_tlf: '+40',
    id_zona: 14,
    importacion: 15
  });

  await knex("pais").insert({
    id: 30,
    nombre: "Suecia",
    codigo_tlf: '+46',
    id_zona: 14,
    importacion: 15
  });

  await knex("pais").insert({
    id: 31,
    nombre: "Serbia",
    codigo_tlf: '+381',
    id_zona: 14,
    importacion: 35
  });

  await knex("pais").insert({
    id: 32,
    nombre: "Croacia",
    codigo_tlf: '+385',
    id_zona: 14,
    importacion: 15
  });

  await knex("pais").insert({
    id: 33,
    nombre: "Albania",
    extracomunitario: 1,
    codigo_tlf: '+355',
    id_zona: 15,
    importacion: 35
  });

  await knex("pais").insert({
    id: 34,
    nombre: "Islandia",
    extracomunitario: 1,
    codigo_tlf: '+354',
    id_zona: 15,
    importacion: 35
  });

  await knex("pais").insert({
    id: 35,
    nombre: "Montenegro",
    extracomunitario: 1,
    codigo_tlf: '+382',
    id_zona: 15,
    importacion: 35
  });

  await knex("pais").insert({
    id: 36,
    nombre: "Malta",
    codigo_tlf: '+356',
    id_zona: 15,
    importacion: 15
  });

  await knex("pais").insert({
    id: 37,
    nombre: "Macedonia",
    extracomunitario: 1,
    codigo_tlf: '+389',
    id_zona: 15,
    importacion: 35
  });

  await knex("pais").insert({
    id: 38,
    nombre: "Bosnia Herzegovina",
    extracomunitario: 1,
    codigo_tlf: '+387',
    id_zona: 15,
    importacion: 35
  });

  await knex("pais").insert({
    id: 39,
    nombre: "Chipre",
    codigo_tlf: '+357',
    id_zona: 15,
    importacion: 15
  });

  await knex("pais").insert({
    id: 40,
    nombre: "Grecia",
    codigo_tlf: '+30',
    id_zona: 15,
    importacion: 15
  });

  await knex("pais").insert({
    id: 41,
    nombre: "Turkia",
    extracomunitario: 1,
    codigo_tlf: '+90',
    id_zona: 15,
    importacion: 35
  });

  await knex("pais").insert({
    id: 42,
    nombre: "Islas Feroe",
    extracomunitario: 1,
    codigo_tlf: '+298',
    id_zona: 15,
    importacion: 35
  });

  await knex("pais").insert({
    id: 43,
    nombre: "Kosovo",
    extracomunitario: 1,
    codigo_tlf: '+383',
    id_zona: 15,
    importacion: 35
  });

  await knex("pais").insert({
    id: 44,
    nombre: "Portugal islas",
    codigo_tlf: '+351',
    id_zona: 10,
    importacion: 0
  });

  await knex("region").insert({
    id: 1,
    nombre: "Andalucía",
    id_pais: 1,
    id_zona: 3
  });

  await knex("region").insert({
    id: 2,
    nombre: "Aragon",
    id_pais: 1,
    id_zona: 3
  });

  await knex("region").insert({
    id: 3,
    nombre: "Canarias",
    id_pais: 1,
    id_zona: 6
  });

  await knex("region").insert({
    id: 4,
    nombre: "Cantabria",
    id_pais: 1,
    id_zona: 3
  });

  await knex("region").insert({
    id: 5,
    nombre: "Castilla y León",
    id_pais: 1,
    id_zona: 3
  });

  await knex("region").insert({
    id: 6,
    nombre: "Castilla-La Mancha",
    id_pais: 1,
    id_zona: 3
  });

  await knex("region").insert({
    id: 7,
    nombre: "Cataluña",
    id_pais: 1,
    id_zona: 3
  });

  await knex("region").insert({
    id: 8,
    nombre: "Comunidad de Madrid",
    id_pais: 1,
    id_zona: 3
  });

  await knex("region").insert({
    id: 9,
    nombre: "Comunidad Foral de Navarra",
    id_pais: 1,
    id_zona: 3
  });

  await knex("region").insert({
    id: 10,
    nombre: "Comunidad Valenciana",
    id_pais: 1,
    id_zona: 3
  });

  await knex("region").insert({
    id: 11,
    nombre: "Extremadura",
    id_pais: 1,
    id_zona: 3
  });

  await knex("region").insert({
    id: 12,
    nombre: "Galicia",
    id_pais: 1,
    id_zona: 3
  });

  await knex("region").insert({
    id: 13,
    nombre: "Gipuzkoa",
    id_pais: 1,
    id_zona: 3
  });

  await knex("region").insert({
    id: 14,
    nombre: "Islas Baleares",
    id_pais: 1,
    id_zona: 5
  });

  await knex("region").insert({
    id: 15,
    nombre: "La Rioja",
    id_pais: 1,
    id_zona: 3
  });

  await knex("region").insert({
    id: 16,
    nombre: "País Vascos",
    id_pais: 1,
    id_zona: 3
  });

  await knex("region").insert({
    id: 17,
    nombre: "Principado de Asturias",
    id_pais: 1,
    id_zona: 3
  });

  await knex("region").insert({
    id: 18,
    nombre: "Región de Murcia",
    id_pais: 1,
    id_zona: 3
  });

  /*await knex("region").insert({
    id: 19,
    nombre: "Portugal Islas",
    id_pais: 2,
    id_zona: 10
  });*/

  await knex("region").insert({
    id: 20,
    nombre: "Baleares",
    id_pais: 1,
    id_zona: 4
  });

  await knex("region").insert({
    id: 21,
    nombre: "Islas Canarias",
    id_pais: 1,
    id_zona: 7
  });

  await knex("region").insert({
    id: 22,
    nombre: "Ceuta y Melilla",
    id_pais: 1,
    id_zona: 8
  });

  await knex("provincia").insert({
    id: 1,
    nombre: "Almería",
    id_region: 1,
  });

  await knex("provincia").insert({
    id: 2,
    nombre: "Cádiz",
    id_region: 1,
  });

  await knex("provincia").insert({
    id: 3,
    nombre: "Córdoba",
    id_region: 1,
  });

  await knex("provincia").insert({
    id: 4,
    nombre: "Granada",
    id_region: 1,
  });

  await knex("provincia").insert({
    id: 5,
    nombre: "Huelva",
    id_region: 1,
  });

  await knex("provincia").insert({
    id: 6,
    nombre: "Jaén",
    id_region: 1,
  });

  await knex("provincia").insert({
    id: 7,
    nombre: "Málaga",
    id_region: 1,
  });

  await knex("provincia").insert({
    id: 8,
    nombre: "Sevilla",
    id_region: 1,
  });

  await knex("provincia").insert({
    id: 9,
    nombre: "Huesca",
    id_region: 2,
  });

  await knex("provincia").insert({
    id: 10,
    nombre: "Teruel",
    id_region: 2,
  });

  await knex("provincia").insert({
    id: 11,
    nombre: "Zaragoza",
    id_region: 2,
  });

  await knex("provincia").insert({
    id: 12,
    nombre: "Las Palmas",
    id_region: 3,
  });

  await knex("provincia").insert({
    id: 13,
    nombre: "Santa Cruz de Tenerife",
    id_region: 3,
  });

  await knex("provincia").insert({
    id: 14,
    nombre: "Cantabria",
    id_region: 4,
  });

  await knex("provincia").insert({
    id: 15,
    nombre: "Avila",
    id_region: 5,
  });

  await knex("provincia").insert({
    id: 16,
    nombre: "Burgos",
    id_region: 5,
  });

  await knex("provincia").insert({
    id: 17,
    nombre: "León",
    id_region: 5,
  });

  await knex("provincia").insert({
    id: 18,
    nombre: "Palencia",
    id_region: 5,
  });

  await knex("provincia").insert({
    id: 19,
    nombre: "Salamanca",
    id_region: 5,
  });

  await knex("provincia").insert({
    id: 20,
    nombre: "Segovia",
    id_region: 5,
  });

  await knex("provincia").insert({
    id: 21,
    nombre: "Soria",
    id_region: 5,
  });

  await knex("provincia").insert({
    id: 22,
    nombre: "Valladolid",
    id_region: 5,
  });

  await knex("provincia").insert({
    id: 23,
    nombre: "Zamora",
    id_region: 5,
  });

  await knex("provincia").insert({
    id: 24,
    nombre: "Albacete",
    id_region: 6,
  });

  await knex("provincia").insert({
    id: 25,
    nombre: "Ciudad Real",
    id_region: 6,
  });

  await knex("provincia").insert({
    id: 26,
    nombre: "Cuenca",
    id_region: 6,
  });

  await knex("provincia").insert({
    id: 27,
    nombre: "Guadalajara",
    id_region: 6,
  });

  await knex("provincia").insert({
    id: 28,
    nombre: "Toledo",
    id_region: 6,
  });

  await knex("provincia").insert({
    id: 29,
    nombre: "Barcelona",
    id_region: 7,
  });

  await knex("provincia").insert({
    id: 30,
    nombre: "Gerona (Girona)",
    id_region: 7,
  });

  await knex("provincia").insert({
    id: 31,
    nombre: "Lérida (Lleida)",
    id_region: 7,
  });

  await knex("provincia").insert({
    id: 32,
    nombre: "Tarragona",
    id_region: 7,
  });

  await knex("provincia").insert({
    id: 33,
    nombre: "Madrid",
    id_region: 8,
  });

  await knex("provincia").insert({
    id: 34,
    nombre: "Navarra",
    id_region: 9,
  });

  await knex("provincia").insert({
    id: 35,
    nombre: "Alicante",
    id_region: 10,
  });

  await knex("provincia").insert({
    id: 36,
    nombre: "Castellón",
    id_region: 10,
  });

  await knex("provincia").insert({
    id: 37,
    nombre: "Valencia",
    id_region: 10,
  });

  await knex("provincia").insert({
    id: 38,
    nombre: "Badajoz",
    id_region: 11,
  });

  await knex("provincia").insert({
    id: 39,
    nombre: "Cáceres",
    id_region: 11,
  });

  await knex("provincia").insert({
    id: 40,
    nombre: "La Coruña (A Coruña)",
    id_region: 12,
  });

  await knex("provincia").insert({
    id: 41,
    nombre: "Lugo",
    id_region: 12,
  });

  await knex("provincia").insert({
    id: 42,
    nombre: "Orense (Ourense)",
    id_region: 12,
  });

  await knex("provincia").insert({
    id: 43,
    nombre: "Pontevedra",
    id_region: 12,
  });

  await knex("provincia").insert({
    id: 44,
    nombre: "Guipúzcoa",
    id_region: 13,
  });

  await knex("provincia").insert({
    id: 45,
    nombre: "Baleares resto de islas",
    id_region: 14,
  });

  await knex("provincia").insert({
    id: 46,
    nombre: "La Rioja",
    id_region: 15,
  });

  await knex("provincia").insert({
    id: 47,
    nombre: "Alava (Araba)",
    id_region: 16,
  });

  await knex("provincia").insert({
    id: 48,
    nombre: "Vizcaya (Bizkaia)",
    id_region: 16,
  });

  await knex("provincia").insert({
    id: 49,
    nombre: "Asturias",
    id_region: 17,
  });

  await knex("provincia").insert({
    id: 50,
    nombre: "Murcia",
    id_region: 18,
  });

  await knex("provincia").insert({
    id: 51,
    nombre: "Baleares Mallorca",
    id_region: 20,
  });

  await knex("provincia").insert({
    id: 52,
    nombre: "Canarias resto de islas",
    id_region: 21,
  });

  await knex("provincia").insert({
    id: 53,
    nombre: "Ceuta",
    id_region: 22,
  });

  await knex("provincia").insert({
    id: 54,
    nombre: "Melilla",
    id_region: 22,
  });



  /* MODALIDADES - PROVINCIAL */
  /*
  await knex("modalidad").insert({
    id: 1,
    nombre: "Paquete 10 Provincial",
    descripcion: "Paquete 10 Provincial",
    horas: null,
    id_zona: 1,
    id_pais: null,
  });
  await knex("modalidad").insert({
    id: 2,
    nombre: "Paquete 14 Provincial",
    descripcion: "Paquete 14 Provincial",
    horas: null,
    id_zona: 1,
    id_pais: null,
  });*/
  await knex("modalidad").insert({
    id: 3,
    nombre: "Paquete 24 Provincial",
    descripcion: "Paquete 24 Provincial",
    horas: null,
    id_zona: 1,
    id_pais: null,
  });

  /* MODALIDADES - REGIONAL */
  /*
  await knex("modalidad").insert({
    id: 4,
    nombre: "Paquete 10 Regional",
    descripcion: "Paquete 10 Regional",
    horas: null,
    id_zona: 2,
    id_pais: null,
  });
  await knex("modalidad").insert({
    id: 5,
    nombre: "Paquete 14 Regional",
    descripcion: "Paquete 14 Regional",
    horas: null,
    id_zona: 2,
    id_pais: null,
  });*/
  await knex("modalidad").insert({
    id: 6,
    nombre: "Paquete 24 Regional",
    descripcion: "Paquete 24 Regional",
    horas: null,
    id_zona: 2,
    id_pais: null,
  });

  /* MODALIDADES - PENINSULAR */
  /*
  await knex("modalidad").insert({
    id: 7,
    nombre: "Paquete 10 Peninsular",
    descripcion: "Paquete 10 Peninsular",
    horas: null,
    id_zona: 3,
    id_pais: null,
  });
  await knex("modalidad").insert({
    id: 8,
    nombre: "Paquete 14 Peninsular",
    descripcion: "Paquete 14 Peninsular",
    horas: null,
    id_zona: 3,
    id_pais: null,
  });*/
  await knex("modalidad").insert({
    id: 9,
    nombre: "Paquete 24 Peninsular",
    descripcion: "Paquete 24 Peninsular",
    horas: null,
    id_zona: 3,
    id_pais: null,
  });

  /* MODALIDADES - OTROS */
  await knex("modalidad").insert({
    id: 10,
    nombre: "Paquete Baleares Mallorca",
    descripcion: "Paquete Baleares Mallorca",
    horas: null,
    id_zona: 4,
    id_pais: null,
  });
  await knex("modalidad").insert({
    id: 11,
    nombre: "Paquete Baleares Resto de Islas",
    descripcion: "Paquete Baleares Resto de Islas",
    horas: null,
    id_zona: 5,
    id_pais: null,
  });
  await knex("modalidad").insert({
    id: 12,
    nombre: "Paquete Canarias Gran Canaria",
    descripcion: "Paquete Canarias Gran Canaria",
    horas: null,
    id_zona: 6,
    id_pais: null,
  });
  await knex("modalidad").insert({
    id: 13,
    nombre: "Paquete Ceuta / Melilla",
    descripcion: "Paquete Ceuta / Melilla",
    horas: null,
    id_zona: 8,
    id_pais: null,
  });
  await knex("modalidad").insert({
    id: 14,
    nombre: "Paquete Portugal Península",
    descripcion: "Paquete Portugal Península",
    horas: null,
    id_zona: 9,
    id_pais: null,
  });
  await knex("modalidad").insert({
    id: 15,
    nombre: "Paquete Portugal Islas",
    descripcion: "Paquete Portugal Islas",
    horas: null,
    id_zona: 10,
    id_pais: null,
  });

  /* MODALIDADES - ZONAS */
  await knex("modalidad").insert({
    id: 16,
    nombre: "Paquete Internacional ZONA 1",
    descripcion: "Paquete Internacional ZONA 1",
    horas: null,
    id_zona: 11,
    id_pais: null,
  });
  await knex("modalidad").insert({
    id: 17,
    nombre: "Paquete Internacional ZONA 2",
    descripcion: "Paquete Internacional ZONA 2",
    horas: null,
    id_zona: 12,
    id_pais: null,
  });
  await knex("modalidad").insert({
    id: 18,
    nombre: "Paquete Internacional ZONA 3",
    descripcion: "Paquete Internacional ZONA 3",
    horas: null,
    id_zona: 13,
    id_pais: null,
  });
  await knex("modalidad").insert({
    id: 19,
    nombre: "Paquete Internacional ZONA 4",
    descripcion: "Paquete Internacional ZONA 4",
    horas: null,
    id_zona: 14,
    id_pais: null,
  });
  await knex("modalidad").insert({
    id: 20,
    nombre: "Paquete Internacional ZONA 5",
    descripcion: "Paquete Internacional ZONA 5",
    horas: null,
    id_zona: 15,
    id_pais: null,
  });
  await knex("modalidad").insert({
    id: 21,
    nombre: "Paquete Canarias Resto de Islas",
    descripcion: "Paquete Canarias Resto de Islas",
    horas: null,
    id_zona: 7,
    id_pais: null,
  });

}
