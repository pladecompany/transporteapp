"use strict";

const { Modelo } = require("./Modelo");
const EnvioBulto = require('./EnvioBulto');


const Zona = require('../models/Zona');
const Pais = require('../models/Pais');
const Aduana = require('../models/Aduana');
const Modalidad = require('../models/Modalidad');
const Provincia = require('../models/Provincia');

const PA = require('../data/id_paises');
const PR = require('../data/id_provincias');

const {
  ID_PROVINCIAS_ESPECIALES,
  ID_PROVINCIAS_MAS_FRANJA_PRECIO,
  ENVIOS_NO_REALIZABLES,
} = require('../data/casos_envio');

const isNum = n => +n === +n;




class Envio extends Modelo {
  static get tableName() {
    return "envio";
  }

  static get relationMappings() {
    return {
      'bultos': {
        relation: Modelo.HasManyRelation,
        modelClass: EnvioBulto,
        join: {
          from: 'envio.id',
          to: 'envio_bulto.id_envio'
        }
      }
    };
  }


  static async obtenerTipoDeEnvio(id_pais1, id_pais2, id_provincia1, id_provincia2) {

    const { pais1, pais2, provincia1, provincia2 } = await Pais.recuperarPaisesYProvincias(id_pais1, id_pais2, id_provincia1, id_provincia2);


    let id_zona_envio = null;
    const params = {};

    // DENTRO DE ESPAÑA
    if (id_pais1 == PA.ESPAÑA && id_pais1 == id_pais2)
    {
      if (provincia1.id == provincia2.id) {
        id_zona_envio = 1; //PROVINCIAL
      } else if (provincia1.id_region == provincia2.id_region) {
        id_zona_envio = 2; //REGIONAL
      } else {
        id_zona_envio = 3; //PENINSULAR
      }

      // Si es provincia especial, tomar la zona con la que esta asociada.
      if (ID_PROVINCIAS_ESPECIALES.includes(+provincia2.id)) {
        id_zona_envio = null;
      }

    }

    // ESPAÑA -> INTERNACIONAL (tomar zona internacional)
    else if (id_pais2 != PA.ESPAÑA) {
      id_zona_envio = pais2.id_zona;
    }

    // INTERNACIONAL -> ESPAÑA (tomar zona internacional)
    else if (id_pais1 != PA.ESPAÑA) {
      id_zona_envio = pais1.id_zona;
    }



    /* BUSCAR ZONA A UTILIZAR */
    let zona_destino;
    if (id_zona_envio) {
      zona_destino = await Zona.query().findById(id_zona_envio).catch(console.error);
    }
    else if (provincia2 && provincia2.id) {
      zona_destino = await Zona.query().findById(provincia2.id_zona).catch(console.error);
    }
    else {
      zona_destino = await Zona.query().findById(pais2.id_zona).catch(console.error);
    }

    let modalidades_envio, aduana;
    if (zona_destino) {
      modalidades_envio = await Modalidad.query().where('id_zona', zona_destino.id).catch(console.error);
      aduana = await Aduana.query().findOne({ id_zona: zona_destino.id }).catch(console.error);
    }


    /* OTROS CASOS */
    if (isNum(id_provincia1) && isNum(id_provincia2) && ID_PROVINCIAS_MAS_FRANJA_PRECIO.includes(+id_provincia1)) {
      params.franja_adicional = 1;
    }



    /* ENVIOS QUE NO SE PUEDEN REALIZAR */

    // INTERNACIONAL -> ESPAÑA
    if (id_pais1 != PA.ESPAÑA && isNum(id_provincia2) && ENVIOS_NO_REALIZABLES[PR.INTERNACIONAL]) {
      if (ENVIOS_NO_REALIZABLES[PR.INTERNACIONAL].includes(+id_provincia2)) {
        zona_destino = false;
      }
    }

    // ESPAÑA -> ESPAÑA
    if (isNum(id_provincia1) && isNum(id_provincia2) && ENVIOS_NO_REALIZABLES[id_provincia1]) {
      if (ENVIOS_NO_REALIZABLES[id_provincia1].includes(+id_provincia2)) {
        zona_destino = false;
      }
    }


    return {
      zona: zona_destino,
      modalidades: zona_destino ? modalidades_envio : [],
      aduana: zona_destino ? aduana : {},
      params: zona_destino ? params : {},
    };
  }


}

module.exports = Envio;