"use strict";
const { Modelo } = require("./Modelo");

class Cliente extends Modelo {
  static get tableName() {
    return "cliente";
  }
}

module.exports = Cliente;