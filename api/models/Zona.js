"use strict";
const { Modelo } = require("./Modelo");
const PesosZona = require('../models/PesosZona');
const Region = require('../models/Region');
const Pais = require('../models/Pais');

class Zona extends Modelo {
	static get tableName() {
	    return "zona";
	}

  	$afterGet(queryContext) {
    	return this.ag();
  	}

  	async pais() {
        if(this.id){
            var res = await Pais.query()
                .where("id_zona", this.id);
            if (res[0]) var valor = res;
            else var valor = [];
            this.pais = valor;
        }
    }

    async region() {
        if(this.id){
            var res = await Region.query()
                .where("id_zona", this.id);
            if (res[0]) var valor = res;
            else var valor = [];
            this.region = valor;
        }
    }

    async pesos() {
        if(this.id){
            var res = await PesosZona.query()
                .where("id_zona", this.id);
            if (res[0]) var valor = res;
            else var valor = [];
            this.pesos = valor;
        }
    }

  	async ag() {
        await this.pais();
        await this.region();
        await this.pesos();
    }
}

module.exports = Zona;