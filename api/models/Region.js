"use strict";
const { Modelo } = require("./Modelo");

class Region extends Modelo {
  static get tableName() {
    return "region";
  }
}

module.exports = Region;