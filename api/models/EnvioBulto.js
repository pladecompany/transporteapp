"use strict";
const { Modelo } = require("./Modelo");

class EnvioBulto extends Modelo {
  static get tableName() {
    return "envio_bulto";
  }


  static get relationMappings() {
  	const Envio = require('./Envio');

    return {
      'rel_envio': {
        relation: Modelo.BelongsToOneRelation,
        modelClass: Envio,
        join: {
          from: 'envio_bulto.id_envio',
          to: 'envio.id'
        }
      }
    };
  }


}

module.exports = EnvioBulto;