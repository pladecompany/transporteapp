"use strict";
const { Modelo } = require("./Modelo");
const { raw } = require("objection");

class TokenRecuperacion extends Modelo {
  static get tableName() {
    return "token_recuperacion";
  }
}

module.exports = TokenRecuperacion;