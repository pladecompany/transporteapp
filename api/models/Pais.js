"use strict";
const { Modelo } = require("./Modelo");
const Provincia = require('./Provincia');

class Pais extends Modelo {
  static get tableName() {
    return "pais";
  }



  static async recuperarPaisesYProvincias(id_pais1, id_pais2, id_provincia1, id_provincia2) {

    let pais1, provincia1, pais2, provincia2;

    pais1 = await this.query()
      .findById(id_pais1)
      .catch(console.error);

    if (+id_provincia1 === +id_provincia1) {
      provincia1 = await Provincia.query()
        .select('provincia.*', 'zona.nombre AS zona', 'region.id_zona AS id_zona')
        .findById(id_provincia1)
        .leftJoin('region', 'region.id', 'provincia.id_region')
        .leftJoin('zona', 'zona.id', 'region.id_zona')
        .catch(console.error);
    }
    if (!provincia1)
      provincia1 = {};

    if (+id_provincia2 === +id_provincia2) {
      provincia2 = await Provincia.query()
        .select('provincia.*', 'zona.nombre AS zona', 'region.id_zona AS id_zona')
        .findById(id_provincia2)
        .leftJoin('region', 'region.id', 'provincia.id_region')
        .leftJoin('zona', 'zona.id', 'region.id_zona')
        .catch(console.error);
    }
    if (!provincia2)
      provincia2 = {};


    if (+id_pais2 === +id_pais1) {
      pais2 = pais1;
    }
    else {
      pais2 = await this.query().findById(id_pais2).catch(console.error);
    }

    return { pais1, pais2, provincia1, provincia2 };
  }


}

module.exports = Pais;