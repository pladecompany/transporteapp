"use strict";
const { Modelo } = require("./Modelo");

class PagoEnvio extends Modelo {
  static get tableName() {
    return "pago_envio";
  }
}

module.exports = PagoEnvio;