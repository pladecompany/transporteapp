"use strict";
const { Modelo } = require("./Modelo");

class Aduana extends Modelo {
  static get tableName() {
    return "aduana";
  }
}

module.exports = Aduana;