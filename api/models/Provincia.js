"use strict";
const { Modelo } = require("./Modelo");

class Provincia extends Modelo {
  static get tableName() {
    return "provincia";
  }
}

module.exports = Provincia;