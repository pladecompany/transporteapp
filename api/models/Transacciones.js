"use strict";

const { Modelo } = require("./Modelo");

class Transacciones extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "transacciones";
    }

}

module.exports = Transacciones;