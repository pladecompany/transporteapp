"use strict";
const { Modelo } = require("./Modelo");

class Modalidad extends Modelo {
  static get tableName() {
    return "modalidad";
  }
}

module.exports = Modalidad;