"use strict";
const { Modelo } = require("./Modelo");

class PesosZona extends Modelo {
  static get tableName() {
    return "pesos_zonas";
  }
}

module.exports = PesosZona;