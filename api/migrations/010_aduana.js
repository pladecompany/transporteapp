
exports.up = function(knex) {
  return knex.schema.createTable('aduana', table => {
    table.increments('id').primary();
    table.decimal('importacion');
    table.decimal('dua');
    table.decimal('dua_exportacion');
    table.integer('id_zona').unsigned().references('zona.id').onDelete('CASCADE');
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('aduana');
};
