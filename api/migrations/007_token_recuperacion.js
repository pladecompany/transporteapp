
exports.up = function(knex) {
  return knex.schema.createTable('token_recuperacion', table => {
    table.increments('id').primary();
    table.integer('id_admin').unsigned().references('admin.id');
    table.string('token');
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('token_recuperacion');
};
