
exports.up = function(knex) {
  return knex.schema.createTable('envio', table => {
    table.increments('id').primary();

    table.integer('id_pais_origen').unsigned().references('pais.id').notNullable();
    table.integer('id_pais_destino').unsigned().references('pais.id').notNullable();
    table.integer('id_provincia_origen').unsigned().references('provincia.id').nullable();
    table.integer('id_provincia_destino').unsigned().references('provincia.id').nullable();

    table.string('direccion_origen', 100);
    table.string('codigo_postal_origen', 20);

    table.string('direccion_destino', 100);
    table.string('codigo_postal_destino', 20);

    table.string('localidad_origen', 100);
    table.string('localidad_destino', 100);

    table.integer('id_modalidad').unsigned().references('modalidad.id');
    table.integer('id_cliente').unsigned().nullable().references('cliente.id');
    table.integer('id_zona').unsigned();

    table.decimal('seguro', 6,4).unsigned();
    table.decimal('reembolso', 6,4).unsigned();

    table.date('fecha_recogida');
    table.time('hora_recogida_desde');
    table.time('hora_recogida_hasta');

    table.string('nombre_destinatario', 50);
    table.string('correo_destinatario', 50);
    table.string('telefono_destinatario', 20);
    table.string('cod_telefono_destinatario', 10);

    table.boolean('es_documentacion');
    table.boolean('es_cliente_empresa');
    table.boolean('es_destinatario_empresa');

    table.decimal('monto');

    table.timestamp('fecha_envio').defaultTo(knex.fn.now());
    table.string('codigo_generado', 50);
    table.enu('estatus', ['1', '2']); //1:completo, 2:borrador

    table.string('url_seguimiento');
    table.string('observacion_seguimiento');
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('envio');
};
