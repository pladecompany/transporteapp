
exports.up = function(knex) {
  return knex.schema.createTable('envio_bulto', table => {
    table.increments('id').primary();
    table.integer('id_envio').references('envio.id').unsigned().notNullable().onDelete('CASCADE');

    table.decimal('fondo');
    table.decimal('ancho');
    table.decimal('alto');
    table.decimal('peso');
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('envio_bulto');
};
