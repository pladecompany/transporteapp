exports.up = knex => {
    return knex.schema.createTable("transacciones", table => {
        table.increments("id").primary();
        table.integer("envio_id");
        table.string("code_pago", 50);
        table.string("email", 100);
        table.string("proceso", 50); //1 activo // o inactivo
        table.string("metodo_pago", 50);
        table.string("metodo_pago_text", 50);
        table.string("estatus", 50);
        table.string("response_payment", 50);
        table.decimal("importe");
        table.datetime('fecha_registro');
        table.datetime('fecha_actualizacion');
    });
  };
exports.down = knex => {
    return knex.schema.dropTableIfExists("transacciones");

};
