exports.up = function(knex, Promise) {
    return knex.schema.alterTable("envio", table => {
        table.string('nombre_contacto_destinatario',50);
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.alterTable("envio", table => {
        table.dropColumn('nombre_contacto_destinatario');
    });
};
