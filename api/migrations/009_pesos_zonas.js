
exports.up = function(knex) {
  return knex.schema.createTable('pesos_zonas', table => {
    table.increments('id').primary();
    table.decimal('desde');
    table.decimal('hasta');
    table.decimal('costo');
    table.string('tiempo', 255);
    table.integer('id_zona').unsigned().references('zona.id').onDelete('CASCADE');
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('pesos_zonas');
};
