
exports.up = function(knex) {
  return knex.schema.createTable('provincia', table => {
    table.increments('id').primary();
    table.string('nombre', 100);
    table.integer('id_region').unsigned().references('region.id').onDelete('CASCADE');
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('provincia');
};
