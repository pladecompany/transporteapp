
exports.up = function(knex) {
  return knex.schema.createTable('pago_envio', table => {
    table.increments('id').primary();
    table.integer('id_envio').references('envio.id').unsigned().notNullable().onDelete('CASCADE');
    table.string('tipo_pago', 50);
    table.date('fecha_pago');
    table.string('referencia_pago', 100);
    table.string('estatus_pago', 100);
    table.text('img_pago');
    table.timestamp('fecha_registro_pago').defaultTo(knex.fn.now());
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('pago_envio');
};
