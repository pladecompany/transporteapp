exports.up = knex => {
    return knex.schema.createTable("notificacion", table => {
        table.increments("id").primary();
        table.text("titulo");
        table.text("contenido");
        table.datetime("fecha");
        table.integer("estatus").defaultTo(0); //1 activo // o inactivo
        table.integer("tipo_noti"); // 0 sinredireccionar // 1 redireccionar // 2 redireccionar con identificador
        table.string("url");
        table.integer("id_usable");
        table.integer('id_receptor');
        table.integer('id_emisor');
        table.string("tabla_usuario_r");
        table.string("tabla_usuario_e");
    });
  };

exports.down = knex => {
    return knex.schema.dropTableIfExists("notificacion");

};
