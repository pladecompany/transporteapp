
exports.up = function(knex) {
  return knex.schema.createTable('tarifa_zona', table => {
    table.increments('id').primary();
    table.string('nombre', 100);
    table.double('desde');
    table.double('hasta');
    table.double('costo');
    table.integer('id_zona').unsigned().references('zona.id').onDelete('CASCADE');
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('tarifa_zona');
};
