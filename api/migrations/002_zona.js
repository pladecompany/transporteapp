
exports.up = function(knex) {
  return knex.schema.createTable('zona', table => {
    table.increments('id').primary();
    table.string('nombre', 100);
    table.string('tipo');
    table.string('envio');
    table.decimal('tarifa_adicional');
    table.decimal('coeficiente_registrado');
    table.decimal('coeficiente_registrado_lvl_1');
    table.decimal('coeficiente_no_registrado');
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('zona');
};
