
exports.up = function(knex) {
  return knex.schema.createTable('modalidad', table => {
    table.increments('id').primary();
    table.string('nombre', 50);
    table.string('descripcion', 100);
    table.decimal('monto');
    table.integer('horas').unsigned().nullable();
    table.integer('id_zona').unsigned().references('zona.id').onDelete('CASCADE');
    table.integer('id_pais').unsigned().references('pais.id').onDelete('CASCADE');
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('modalidad');
};

