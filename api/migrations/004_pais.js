
exports.up = function(knex) {
  return knex.schema.createTable('pais', table => {
    table.increments('id').primary();
    table.string('nombre', 100);
    table.boolean('region');
    table.string('codigo_tlf', 20);
    table.decimal('importacion');
    table.integer('id_zona').unsigned().references('zona.id').onDelete(null);
    table.boolean('extracomunitario').defaultTo(false);
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('pais');
};
