
exports.up = function(knex) {
  return knex.schema.createTable('cliente', table => {
    table.increments('id').primary();
    table.string('dni', 20);
    table.string('correo', 50);
    table.string('nombre', 100);
    table.string('cod_telefono', 20);
    table.string('telefono', 20);
    table.string('direccion', 100);
    table.string('localidad', 100);
    table.string('codigo_postal', 20);
    table.string('empresa', 50);
    table.integer('coeficiente_envio');
    table.decimal('porcentaje_des');
    table.boolean('domiciliacion_bancaria');
    table.integer('id_pais').unsigned().references('pais.id');
    table.integer('id_region').unsigned().references('region.id');
    table.integer('id_provincia').unsigned().references('provincia.id');
    table.string('motivo', 255);
    table.boolean('es_empresa');
    table.enu('estatus', ['1', '2', '3']); // 1:Temporal, 2:Registrado, 3:Cliente uno.
    table.string('pass');
    table.string('token', 100).nullable();
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('cliente');
};