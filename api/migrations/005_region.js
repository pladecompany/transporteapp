
exports.up = function(knex) {
  return knex.schema.createTable('region', table => {
    table.increments('id').primary();
    table.string('nombre', 100);
    table.integer('id_zona').unsigned().references('zona.id').onDelete(null);
    table.integer('id_pais').unsigned().references('pais.id').onDelete('CASCADE');
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('region');
};
